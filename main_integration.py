"""
Created on 1/11/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Description:
    In this file the use of the core package is checked by emulating the surrounding calculation
    
"""
import copy

import bfch
import time
import numpy as np

# -------------------------------------------------------------------------------------------------------------------- #
# Model Control
# -------------------------------------------------------------------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #
# CHECKLIST NEW MODEL / NEW CALCULATION
#   - check control parameters, spelling and values
#   - check circuit_param
#   - component_info_all filled out correctly at keyword circuit_type?
#   - data2plot ok? check the names of the variables, are they defined in the corresponding components?
#   - is the correct list entry chosen in plot_results_dyn (almost at the end of this file)

custom_mission_duration_toggle = 0      # 0: take duration from load data| 1: set custom duration
custom_mission_duration = 200           # [s]

circuit_type = "CircuitRegulatedUncontrolledPowerSharing"
circuit_type = "CircuitUnregulatedDiodeChargeResistor"
# circuit_type = "fc_dc"

# Info: character '°' is not allowed in here, degrees Celsius is only 'C'
# mission: either example mission or filepath
# mission = "AREA_FlyHy_mission_profile_extended_Compressor"
mission = "AREA_FlyHy_mission_profile_extended_Basic"
# mission = "mission_profile_100s_test_state3"

conditioning_toggle = False
max_step_size = 50   # [s] maximum length of a calculation step
min_num_steps_per_segment = 10  # minimum number of segments in a given segment.
max_mean_error_component = 0.01  # [-]
max_solver_steps = 5000  # [-]
convergence_crit = 8e-4  # [-]
solver_step_factor = 0.5      # [-] scaling of all the component functions, when calculating the step. Smaller factor
                            # leads to smaller steps
step_factor_reduction_speed = 200  # reduction every 100 iterations

components_list = {
    'fuelCellStack': bfch.FcModel,
    'liIonBattery': bfch.LiIon,
    'powerSink': bfch.PowerSink,
    'dcdc': bfch.DcDc,
    'resistor': bfch.Resistor,
    'bldc': bfch.BLDC,
    'compressor': bfch.Compressor1,
    'diode': bfch.Diode
}

circuit_param = {
    "bat_charge_below_soc": 0.9,    # [-]
    "bat_max_soc": 0.99,    # [-]
    "bat_min_soc": 0.05,    # [-]
    "conditioning_toggle": conditioning_toggle, # [-]
    "conditioning_period": 10,  # [s]
    "conditioning_duration": 0.1,   # [s]
    "load_voltage": 13.5,   # [V]
    "bat_charging_i": 1,    # [A]
    "bat_discharging_i": 2,  # [A]
    "limit_fc_power_percent_max_power": 0.7,  # [-]
    "fuel_cell_current": 3.5,  # [A]
    "maximum_fuel_cell_power": 50  # [W]
}
conditions_defaults = {
                            "time": [0, "s"],
                            "power": [0, "W"],
                            "rot_speed": [0, "Hz"],
                            "rot_torque": [0, "Nm"],
                            "ambient_temp": [290, "K"],
                            "fc_temp": [350, "K"],
                            "anode_p": [101325, "Pa"],
                            "anode_h2_molfrac": [1, "1"],
                            "cath_p": [101325, "Pa"],
                            "cath_o2_molfrac": [0.21, "1"],
                            "spec_heat_cap": [1004.5, "J/(kg*K)"],
                            "isentropic_exp": [1.4, "1"],
                            "ambient_pressure": [101325, "Pa"],
                            "cath_massflow": [0.01, "kg/s"],
                            "ambient_air_density": [1.13, "kg/m^3"]
    }
conversions = {
        "kW": ["mult", 1000],
        "MW": ["mult", 1e6],
        "rpm": ["mult", 0.016666667],
        "1/min": ["mult", 0.016666667],
        "rad/s": ["mult", 0.15915494309191],
        "C": ["add", 273.15],
        "bar": ["mult", 100000],
        "atm": ["mult", 9.86923267e-6],
        "min": ["mult", 60],
        "h": ["mult", 3600],
        "g/cm^3": ["mult", 1000]
    }

component_info_all = {
    "regulated1": {
        "fc_stack": "Fuel_Cell_ng2019.yaml",
        "Battery": "Battery_Shepherd_unregulated.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "DCDC_converter.yaml",
        "DCDC_bat_ch": "DCDC_converter.yaml",
        "DCDC_bat_di": "DCDC_converter.yaml"
    },
    "regulated2": {
        "fc_stack": "Fuel_Cell_ng2019.yaml",
        "Battery": "Battery_Shepherd_conditioning.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "DCDC_converter.yaml",
        "DCDC_bat_ch": "DCDC_converter.yaml",
        "DCDC_bat_di": "DCDC_converter.yaml",
        "Resistor": "Resistor.yaml"
    },
    "regulated3": {
        "fc_stack": "Fuel_Cell_ng2019.yaml",
        "Battery": "Battery_Shepherd_conditioning.yaml",
        "BLDC": "BLDC_Prattes.yaml",
        "DCDC_fc": "DCDC_converter.yaml",
        "DCDC_bat_ch": "DCDC_converter.yaml",
        "DCDC_bat_di": "DCDC_converter.yaml"
    },
    "regulated4": {
        "fc_stack": "NG19_Fuel_Cell.yaml",
        "Battery": "Battery_Shepherd_conditioning.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "DCDC_converter.yaml",
        "DCDC_bat_ch": "DCDC_converter.yaml",
        "DCDC_bat_di": "DCDC_converter.yaml"
    },
    "unregulated1": {
        "fc_stack": "NG19_Fuel_Cell.yaml",
        "Battery": "NG19_Battery_Shepherd_0_5C.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "DCDC_converter_unreg_ng_2018.yaml"
    },
    "unregulated2": {
        "Battery1": "Battery_Shepherd_unregulated.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "Battery2": "Battery_Shepherd_unregulated.yaml"
    },
    "unregulated3": {
        "fc_stack": "Fuel_Cell_ng2019.yaml",
        "Battery": "Battery_Shepherd_unregulated.yaml",
        "DCDC_fc": "DCDC_converter.yaml",
        "BLDC1": "BLDC_Val_ng.yaml",
        "BLDC2": "BLDC_Val_ng.yaml",
        "BLDC3": "BLDC_Val_ng.yaml",
        "BLDC4": "BLDC_Val_ng.yaml"
    },
    "fc": {
        "fc_stack": "Fuel_Cell_ng2019.yaml",
        "Power_Sink": "Power_Sink.yaml"
    },
    "fc_dc": {
        "fc_stack": "z_NG19_FC.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "z_DCDC.yaml"
    },
    "regulated5": {
        "fc_stack": "Fuel_Cell_ng2019.yaml",
        "Battery": "Battery_Shepherd_unregulated.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "DCDC_converter.yaml",
        "DCDC_bat_ch": "DCDC_converter.yaml",
        "DCDC_bat_di": "DCDC_converter.yaml",
        "compressor": "Compressor.yaml"
    },
    "area": {
        "fc_stack": "AREA_Fuel_Cell.yaml",
        "Battery": "AREA_Battery.yaml",
        "Power_Sink": "AREA_Power_Sink.yaml",
        "DCDC_fc": "AREA_DCDC_converter.yaml",
        "DCDC_bat_ch": "AREA_DCDC_converter.yaml",
        "DCDC_bat_di": "AREA_DCDC_converter.yaml",
        "compressor": "AREA_Compressor.yaml"
    },
    "regulated8": {
        "fc_stack": "Fuel_Cell_ng2019.yaml",
        "Battery": "REG8_Battery_Shepherd.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "DCDC_converter.yaml",
        "DCDC_bat_ch": "DCDC_converter.yaml",
        "DCDC_bat_di": "DCDC_converter.yaml"
    },
    "regulated9": {
        "fc_stack": "NG19_Fuel_Cell.yaml",
        "Battery": "NG19_Battery_Shepherd_3_6C.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "REG9_NG19_DCDC.yaml",
        "DCDC_bat_di": "REG9_NG19_DCDC.yaml",
        "DCDC_bat_ch": "REG9_NG19_DCDC.yaml"
    },
    "regulated_test": {
        "fc_stack": "NG19_Fuel_Cell.yaml",
        "Battery": "NG19_Battery_Shepherd_0_5C.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "DCDC_fc": "REG9_NG19_DCDC.yaml",
        "DCDC_bat_di": "REG9_NG19_DCDC.yaml",
        "DCDC_bat_ch": "REG9_NG19_DCDC.yaml"
    }
}

data2plot = {
    "regulated1": {   # 0 DICT regulated1
                    "Fuel Cell Power": {
                            "fc_stack": ["power"],
                            "Power_Sink": ["power"]
                        },
                    "DcDc Power": {
                            "DCDC_fc": ["power_in", "power_out"]
                        },
                    "conditioning cycle": {
                                        circuit_type: ["conditioning_toggle"]
                                        }
                        },
    "regulated3": {   # 2 DICT regulated3
        "Fuel Cell Power": {
                "fc_stack": ["power"],
                "BLDC": ["power"]
            },
        "DcDc Power": {
                "DCDC_fc": ["power_in", "power_out"]
            },
        "conditioning cycle": {
                circuit_type: ["conditioning_toggle"]
            }
    },
    "regulated4": {   # 3 DICT regulated4
        "Power Distribution": {
                "dcdc_fc": ["power_out"],
                "power_sink": ["power_in"],
                "diode_bat_di": ["power_out"],
                "dcdc_bat_di": ["power_in"]
            },
        "Voltages": {
                "fc_stack": ["u_out"],
                "battery": ["u_out", "u_in"]
            },
        "SOC": {
                "battery": ["soc"]
            },
        "conditioning cycle": {
                circuit_type: ["conditioning_toggle"]
            }
    },
    "regulated4_compressor": {   # 3 DICT regulated4
        "Power Distribution": {
                "dcdc_fc": ["power_out"],
                "power_sink": ["power_in"],
                "diode_bat_di": ["power_out"],
                "dcdc_bat_di": ["power_in"],
                "compressor": ["power_in"]
            },
        "Voltages": {
                "fc_stack": ["u_out"],
                "battery": ["u_out", "u_in"]
            },
        "SOC": {
                "battery": ["soc"]
            },
        "conditioning cycle": {
                circuit_type: ["conditioning_toggle"]
            }
    },
    "regulated5": {   # 4 DICT regulated5
        "Fuel Cell Power": {
                "fc_stack": ["power"],
                "Power_Sink": ["power"]
            },
        "DcDc Power": {
                "DCDC_fc": ["power_in", "power_out"]
            },
        "Efficiency": {
                circuit_type: ["total_efficiency"]
        }
    },
    "unregulated1": {   # 5 DICT unregulated1 validation ng
        "FS Power": {
                "fc_stack": ["power"]
            },
        "FS Power After Step Down": {
                "DCDC_fc": ["power_out"]
            },
        "FS and Battery Power": {
                "DCDC_fc": ["power_out"],
                "Battery": ["power_out"]
            },
        "Current Density": {
                "fc_stack": ["current_density"]
            },
        "Cell Voltage": {
                "fc_stack": ["cell_voltage"]
            },
        "Battery Current": {
                "Battery": ["i_out"]
            },
        "Battery Voltage": {
                "Battery": ["u_out"]
        }
    },
    "regulated8": {   # 6 DICT Regulated8 controlled Regulation
        "Power Split": {
                "DCDC_fc": ["power_out"],
                "Battery": ["power_out"],
                "Power_Sink": ["p_it"]
            },
        "Voltage": {
                "Battery": ["voltage"],
                "DCDC_fc": ["u_out"]
            },
        "Current": {
                "Battery": ["current"],
                "DCDC_fc": ["i_out"]
            },
        "Current Density": {
                "fc_stack": ["current_density"]
            },
        "Cell Voltage": {
                "fc_stack": ["cell_voltage"]
            },
        "Battery Current": {
                "Battery": ["i_out"]
            },
        "Battery Voltage": {
                "Battery": ["u_out"]
        }
    },
    "unregulated_diode": {   # Unregulated Fuel Cell + Battery
        "Power Split": {
                "fc_stack": ["power_out"],
                "Battery": ["power_out"],
                "Power_Sink": ["power_in"]
            },
        "Voltages": {
                "fc_stack": ["u_out"],
                "Battery": ["voltage"],
                "Power_Sink": ["u_in"]
            },
        "Currents": {
                "fc_stack": ["i_out"],
                "Battery": ["current"],
                "Power_Sink": ["i_in"]
            },
        "SOC Battery": {
                "Battery": ["soc"]
            },
        "Current Density": {
                "fc_stack": ["current_density"]
            },
        "Fuel Cell Voltage": {
                "fc_stack": ["cell_voltage"]
            },
        "H2 Consumption": {
                "fc_stack": ["h2_cumulated_consumption"]
            },
        "H2 Flow": {
                "fc_stack": ["h2_fuel_flow"]
            }
    },
    "unregulated_NG": {   # Unregulated Fuel Cell + Battery
        "Power Split": {
                "dcdc_fc": ["power_out"],
                "battery": ["power_out"],
                "power_sink": ["power_in"]
            },
        "Voltages": {
                "dcdc_fc": ["u_out"],
                "battery": ["voltage"],
                "power_sink": ["u_in"]
            },
        "Currents": {
                "dcdc_fc": ["i_out"],
                "Battery": ["current"],
                "Power_Sink": ["i_in"]
            },
        "SOC Battery": {
                "battery": ["soc"]
            },
        "Current Density": {
                "fc_stack": ["current_density"]
            },
        "Fuel Cell Voltage": {
                "fc_stack": ["cell_voltage"]
            },
        "H2 Consumption": {
                "fc_stack": ["h2_cumulated_consumption"]
            },
        "H2 Flow": {
                "fc_stack": ["h2_fuel_flow"]
            }
    },
    "regulated9": {   # 3 DICT regulated4
            "Power Distribution": {
                    "dcdc_fc": ["power_out"],
                    "power_sink": ["power_in"],
                    "battery": ["power_total"]
                },
            "Voltages": {
                    "dcdc_fc": ["u_out"],
                    "battery": ["voltage"]
                },
            "Currents": {
                    "dcdc_fc": ["i_out"],
                    "battery": ["current"]
                },
            "SOC": {
                    "battery": ["soc"]
                },
            "conditioning cycle": {
                    circuit_type: ["conditioning_toggle"]
                }
        },
    "fc_dc": {   # 3 DICT regulated4
            "Power Distribution": {
                    "dcdc_fc": ["power_out"],
                    "fc_stack": ["power_out"]
                },
            "Voltages": {
                    "dcdc_fc": ["u_out"],
                    "fc_stack": ["u_out"]
                }
        },
    "CircuitRegulatedUncontrolledPowerSharing": {   # 3 DICT regulated4
            "Power Distribution": {
                    "dcdc_fc": ["power_out"],
                    "power_sink": ["power_in"],
                    "diode_bat_di": ["power_out"],
                    "dcdc_bat_di": ["power_in"]
                },
            "Voltages": {
                    "fc_stack": ["u_out"],
                    "battery": ["u_out", "u_in"]
                },
            "SOC": {
                    "battery": ["soc"]
                },
            "conditioning cycle": {
                    circuit_type: ["conditioning_toggle"]
                }
    },
    "CircuitUnregulatedDiodeChargeResistor": {   # Unregulated Fuel Cell + Battery
            "Power Split": {
                    "fc_stack": ["power_out"],
                    "Battery": ["power_out"],
                    "Power_Sink": ["power_in"]
                },
            "Voltages": {
                    "fc_stack": ["u_out"],
                    "Battery": ["voltage"],
                    "Power_Sink": ["u_in"]
                },
            "Currents": {
                    "fc_stack": ["i_out"],
                    "Battery": ["current"],
                    "Power_Sink": ["i_in"]
                },
            "SOC Battery": {
                    "Battery": ["soc"]
                },
            "Current Density": {
                    "fc_stack": ["current_density"]
                },
            "Fuel Cell Voltage": {
                    "fc_stack": ["cell_voltage"]
                },
            "H2 Consumption": {
                    "fc_stack": ["h2_cumulated_consumption"]
                },
            "H2 Flow": {
                    "fc_stack": ["h2_fuel_flow"]
                }
    },
}
# -------------------------------------------------------------------------------------------------------------------- #
# Data Loading - the component data loading will be needed for the integrated form too
# -------------------------------------------------------------------------------------------------------------------- #
# load component data


# create the full mission profile
mission_profile = bfch.data.get_mission_data(mission, conditions_defaults, conversions, custom_mission_duration_toggle,
                                             custom_mission_duration)

# -------------------------------------------------------------------------------------------------------------------- #
# Initialization - this part will be needed in the integrated form of the code
# -------------------------------------------------------------------------------------------------------------------- #
# instantiate circuit
circuit = bfch.create_circuit(circuit_type=circuit_type)
# , circuit_param=circuit_param, component_info=component_info_all[circuit_type])

# instantiate the input and the output
model_input = bfch.Input()
output_data = bfch.Output()
list_of_outputs = []

# instantiate the solver
circuit_solver = bfch.CircuitSolver(max_step_size, max_mean_error_component, max_solver_steps, convergence_crit,
                                    solver_step_factor, step_factor_reduction_speed, min_num_steps_per_segment)

# -------------------------------------------------------------------------------------------------------------------- #
# Calculation - this part is again only an emulation except for the circuit_solver
# -------------------------------------------------------------------------------------------------------------------- #
start = time.perf_counter()
current_mission_step = 0  # current time step in the mission profile
t_elap = 0  # elapsed time since the start of the calculation
for i in range(0, len(mission_profile.index)-1, 2):
    # get the information about the current segment into the input class
    model_input.update_from_mission_profile(i, mission_profile)

    # run the calculation for the current segment and store it in the list of outputs
    list_of_outputs.append(copy.deepcopy((circuit_solver.solve(circuit, model_input, output_data))))

    # show calculation progress
    state0 = list_of_outputs[-1].output_circuit[f'{circuit_type} state 1'].iloc[0]
    state1 = list_of_outputs[-1].output_circuit[f'{circuit_type} state 1'].iloc[-1]
    print(f'\nSegment: {i/2+1} GO!| t = {current_mission_step}| at: t={t_elap}| State: {state0}')
    current_mission_step = np.around(mission_profile["time s"][i + 1], 3)
    t_elap = np.around(time.perf_counter() - start, 6)
    print(f'Segment: {i/2+1} FIN| t = {current_mission_step}| at: t={t_elap}| State: {state1}\n')

bfch.data.save_output_to_csv(list_of_outputs, circuit_type, mission, circuit_param, circuit)
fig = bfch.data.plot_results_dyn(list_of_outputs, data2plot=data2plot[circuit_type])
# plt.show()

bfch.data.save_plots(circuit_type, fig, mission)
# -------------------------------------------------------------------------------------------------------------------- #
