"""
Created on 1/11/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Description:
    In this file the use of the core package is checked by emulating the surrounding calculation
    
"""
import copy

import bfch
import time
import numpy as np
from bfch.Circuits.circuit_IE import CircuitIntelligentEnergy

# -------------------------------------------------------------------------------------------------------------------- #
# Model Control
# -------------------------------------------------------------------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #
# CHECKLIST NEW MODEL / NEW CALCULATION
#   - check control parameters, spelling and values
#   - check circuit_param
#   - component_info_all filled out correctly at keyword circuit_type?
#   - data2plot ok? check the names of the variables, are they defined in the corresponding components?
#   - is the correct list entry chosen in plot_results_dyn (almost at the end of this file)

set_duration = 0    # 0: take duration from load data| 1: set custom duration
duration = 200     # in s
circuit_type = "regulated"
soc = 0.1             # soc at the beginning of the calculation
mission = "mission_test2"   # Info: character '°' is not allowed in here so degrees Celsius is only 'C_bat_As'
# mission = "mission_step_pid"   # Info: character '°' is not allowed in here so degrees Celsius is only 'C_bat_As'
purging = False
max_step_size = 1
allowed_dev = 0.01
max_solver_steps = 500
convergence_crit = 1e-4
solver_step_factor = 0.5

circuit_param = {
        "bat_charge_below_soc": 0.97,
        "bat_max_soc": 0.99,
        "bat_start_soc": 0.99,
        "bat_min_soc": 0.2,
        "bat_charging_i_max": 12,
        # "conditioning_toggle": False,
        # "conditioning_period": 30,
        # "conditioning_duration": 0.1
        "dcdc_fc_mult":0.72,
        "u_target":51.4,
}

conditions_defaults = {
    "time": [0, "s"],
    "power": [0, "W"],
    "omega": [0, "Hz"],
    "ambient_temp": [290, "K"],
    "fc_temp": [350, "K"],
    "anode_p": [101325, "Pa"],
    "anode_h2_molfrac": [1, "1"],
    "cath_p": [101325, "Pa"],
    "cath_o2_molfrac": [0.21, "1"],
    "cp": [1004.5, "J/(kg*K)"],
    "kappa": [1.4, "1"],
    "ambient_pressure": [101325, "Pa"],
    "massflow_cath": [0.001, "kg/s"],
    "air_density": [1.13, "kg/m^3"]
    }

component_info_all = {
    "regulated": {
        # "fc_stack": "FC_generic_singlecell.yaml",  # 0
        # "dcdc_fc": "DCDC_converter.yaml",  # 1
        # "diode_fc": "Diode_regulated.yaml",  # 2
        # "battery": "battery_generic_singlecell.yaml",  # 3
        # "dcdc_bat_di": "DCDC_converter.yaml",  # 4
        # "diode_bat_di": "Diode_regulated.yaml",  # 5
        # "dcdc_bat_ch": "DCDC_converter.yaml",  # 6
        # "diode_bat_ch": "Diode_regulated.yaml",  # 7
        # "power_sink": "power_sink.yaml",  # 8
    }}

# instantiate circuit
circuit = CircuitIntelligentEnergy(circuit_param=circuit_param, circuit_type=circuit_type, component_info=component_info_all[circuit_type])

voltage=51.4
circuit("fc_stack").power_rated=4800.
circuit("fc_stack").u_prated=70.
circuit("fc_stack").series_cells=97
circuit("fc_stack").cell_area=310.

circuit("battery").series_cells=12

circuit("battery").parallel_cells=4
circuit("battery").C_cell_Ah=2.54
circuit("battery").C_bat_As= circuit("battery").C_cell_Ah * circuit("battery").parallel_cells * 3600
circuit("battery").max_charging_current = 5 * circuit("battery").parallel_cells * circuit("battery").C_cell_Ah  # maximum charging current, A
circuit("battery").max_discharging_current = 30 * circuit("battery").parallel_cells * circuit("battery").C_cell_Ah
circuit("battery").i_in.val_max=circuit("battery").max_charging_current
circuit("battery").i_out.val_max=circuit("battery").max_discharging_current
# circuit("battery").u_in.val_min=2.5*circuit("battery").series_cells
# circuit("battery").u_out.val_min=2.5*circuit("battery").series_cells
circuit("battery").u_in.val_min=0.
circuit("battery").u_out.val_min=0.


# Plotting
data2plot = [
    {   # Unregulated Fuel Cell + battery
        "Power Split": {
                "fc_stack": ["power_out"],
                "diode_fc": ["power_out"],
                "battery": ["power_out"],
                "power_sink": ["power"]
            },
        "Power FC_Sink": {
                "fc_stack": ["power_out"],
                "power_sink": ["power"]
            },
        "Voltage": {
                "fc_stack": ["u_out"],
                "battery": ["u_out","u_in"],
                "power_sink": ["u_in"]
            },
        "Current": {
                "fc_stack": ["i_out"],
                "battery": ["i_out","i_in"],
                "power_sink": ["i_in"]
            },
        "Current Density": {
                "fc_stack": ["current_density"]
            },
        "Cell Voltage": {
                "fc_stack": ["cell_voltage"],
            },
        "battery SoC": {
                "battery": ["soc"]
        },
        "battery charging power": {
                "dcdc_bat_ch": ["power_out"]
        },
        "H2 Consumption": {
                "fc_stack": ["h2_cumulated_consumption"]
        },
        "H2 Flow": {
                "fc_stack": ["h2_fuel_flow"]
        }
    }
    ]


# -------------------------------------------------------------------------------------------------------------------- #
# Data Loading - the component data loading will be needed for the integrated form too
# -------------------------------------------------------------------------------------------------------------------- #
# load component data


# create the full mission profile
mission_profile = bfch.data.get_mission_data(mission=mission, conditions_defaults=conditions_defaults, set_duration=set_duration, duration=duration)

# -------------------------------------------------------------------------------------------------------------------- #
# Initialization - this part will be needed in the integrated form of the code
# -------------------------------------------------------------------------------------------------------------------- #

# instantiate the input and the output
model_input = bfch.Input()
output_data = bfch.Output()
list_of_outputs = []

# instantiate the solver
circuit_solver = bfch.CircuitSolver(max_step_size, allowed_dev, max_solver_steps, convergence_crit, solver_step_factor)

# -------------------------------------------------------------------------------------------------------------------- #
# Calculation - this part is again only an emulation except for the circuit_solver
# -------------------------------------------------------------------------------------------------------------------- #
start = time.perf_counter()
t_instant_p = 0
t_elap = 0
for i in range(0, len(mission_profile.index)-1, 2):
    # get the information about the current segment into the input class
    model_input.update_from_mission_profile(i, mission_profile)

    # run the calculation for the current segment and store it in the list of outputs
    list_of_outputs.append(copy.deepcopy((circuit_solver.solve(circuit, model_input, output_data))))

    # show calculation progress
    state0 = list_of_outputs[-1].output_circuit[f'{circuit_type} state 1'].iloc[0]
    state1 = list_of_outputs[-1].output_circuit[f'{circuit_type} state 1'].iloc[-1]
    print(f'Segment: {i/2+1} GO!| t = {t_instant_p}| at: t={t_elap}| State: {state0}')
    t_instant_p = np.around(mission_profile["time s"][i+1], 3)
    t_elap = np.around(time.perf_counter() - start, 6)
    print(f'Segment: {i/2+1} FIN| t = {t_instant_p}| at: t={t_elap}| State: {state1}')

fig = bfch.data.plot_results_dyn(list_of_outputs, data2plot=data2plot[0])
# plt.show()
bfch.data.save_output_to_csv(list_of_outputs, circuit_type, mission, circuit_param, circuit)
bfch.data.save_plots(circuit_type, fig, mission)
# -------------------------------------------------------------------------------------------------------------------- #