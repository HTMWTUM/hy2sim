"""
Created on 01.03.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
import numpy as np
from .circuit import Circuit


class CircuitFuelCellDcDc(Circuit):
    """
    Model of a circuit with a fuel cell, a dcdc and a load
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # define default values
        if component_info is None:
            component_info = {
                "fc_stack": "UNICADO_FuelCell_oneDFlux.yaml",
                "power_sink": "Power_Sink.yaml",
                "dcdc_fc": "DCDC_converter.yaml"
            }
        if circuit_param is None:
            self.circuit_param = {
                "load_voltage": 500
            }
        else:
            self.circuit_param = circuit_param

        self.max_power_fc = 0
        self.load_voltage = self.circuit_param["load_voltage"]
        super().__init__(component_info, circuit_type)
        self("fc_stack").core = True
        self("dcdc_fc").core = True
        self("power_sink").core = True

    def create_circuit_equations(self):
        self.state.val = 0
        self.max_power_fc = self("fc_stack").maximum_power() * self("dcdc_fc").beta
        self.connectivity = np.array([[0, 0, 0],
                                      [0, 0, 1],
                                      [1, 0, 0]])
        self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage)]


class CircuitFuelCellDcDcBleedAirHydraulic(Circuit):
    """
    Model of a circuit with a fuel cell, a dcdc and a load
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # define default values
        if component_info is None:
            component_info = {
                # "fc_stack": "NG19_Fuel_Cell_onedflux.yaml",
                "fc_stack": "UNICADO_FuelCell_oneDFlux.yaml",
                "dcdc_fc": "DCDC_converter.yaml",
                "power_sink": "Power_Sink.yaml",
                "bleed_air": "Bleed_Air.yaml",
                #"hydraulic": "Hydraulic.yaml"
            }
        if circuit_param is None:
            self.circuit_param = {
                "load_voltage": 500
            }
        else:
            self.circuit_param = circuit_param

        self.max_power_fc = 0
        self.core = False # if true, only the core fuel cell components are connected. e.g. to calculate their power rho
        self.load_voltage = self.circuit_param["load_voltage"]
        super().__init__(component_info, circuit_type)
        # define core components:
        self("fc_stack").core = True
        self("dcdc_fc").core = True
        self("power_sink").core = True


    def create_circuit_equations(self):
        self.state.val = 0
        self.max_power_fc = self("fc_stack").maximum_power() * self("dcdc_fc").beta
        if self.core:
            self.connectivity = np.array([[0, 0, 0, 0],
                                          [1, 0, 0, 0],
                                          [0, 1, 0, 0],
                                          [0, 0, 0, 0]])
        else:
            self.connectivity = np.array([[0, 0, 0, 0],
                                          [1, 0, 0, 0],
                                          [0, 1, 0, 0],
                                          [0, 1, 0, 0]])
        self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage)]

