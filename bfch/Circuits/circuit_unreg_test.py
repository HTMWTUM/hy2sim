"""
Created on 17.12.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:
    
"""
import numpy as np
from .circuit import Circuit
from ..Utilities.variable import Variable
import logging


class CircuitUnregulated1(Circuit):
    """
    one fuel cell connected to a dcdc converter, assisted in parallel by a battery connected to a power sink
    FC-DCDC-POWERSINK-BATTERY, Circuit as in Ng, Datta 2018
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "DCDC_fc": "DCDC_converter.yaml"}

        if circuit_param is None:
            circuit_param = {
                "bat_min_soc": 0.01,
                "load_voltage": 11
            }

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]

        # maximum fuel cell stack power
        self.max_power_fc = 0

        # define circuit state parameters
        self.power_share_fuel_cell = Variable("power_share", "%")

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # make sure the battery is not totally depleted
        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta
        system_power_max = self.max_power_fc + \
                           self.load_voltage * self.components[1].return_discharging_voltage(self.load_voltage)
        if system_power_max < self.components[2].current_power():
            logging.error(f'Maximum power of System ({system_power_max}) not sufficient at step: {self.t_place}')
            raise ValueError()
        if self.components[2].current_power() == 0:
            pass
        elif self.components[1].soc() > self.bat_min_soc:
            self.state.val = 0
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc
            self.connectivity = np.array([[0, 0, 0, 0],
                                          [0, 0, 0, 0],
                                          [0, 1, 0, 1],
                                          [1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[1].u_in, 0),
                                        (self.components[1].i_in, 0)]
        else:
            logging.error(f'SOC of the battery critically low at step: {self.t_place}')
            raise ValueError()

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []

    def define_circuit_states(self):
        self.circuit_states = [self.power_share_fuel_cell, self.state]

    def calculate_post_values(self):
        dcdc_out = self.components[3].power_out()
        bat_out = self.components[1].power_out()
        self.power_share_fuel_cell.val = dcdc_out / (bat_out + dcdc_out) * 100


class CircuitUnregulated2(Circuit):
    """
    two batteries in parallel connected to a power sink
    """

    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        Testing of the compressor

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "Battery1": "Battery_Shepherd_unregulated.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "Battery2": "Battery_Shepherd_unregulated.yaml"
            }

        if circuit_param is None:
            circuit_param = {
                "bat_min_soc": 0.01,
                "load_voltage": 10
            }

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]

        # maximum fuel cell stack power
        self.max_power_fc = 0

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # make sure the battery is not totally depleted
        if self.components[0].soc() > self.bat_min_soc:
            self.state.val = 0
            self.components[1].charging = False
            # note for the connectivity matrix: 0. Bat, 1. PowerSink, 2. Bat
            self.connectivity = np.array([[0, 0, 0],
                                          [1, 0, 1],
                                          [0, 0, 0]])
            self.boundary_conditions = [(self.components[0].u_in, 0),
                                        (self.components[0].i_in, 0),
                                        (self.components[2].u_in, 0),
                                        (self.components[2].i_in, 0)]
            # self.initial_conditions = [(self.components[0].u_out, self.components[1].return_discharging_voltage(2))]

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []

    def define_circuit_states(self):
        self.circuit_states = [self.state]


class CircuitUnregulated3(Circuit):
    """
    one fuel cell connected to a dcdc converter, assisted in parallel by a battery connected to a 4 BLDC
    FC-DCDC-POWERSINK-BATTERY, Circuit as in Ng, Datta 2018
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "DCDC_fc": "DCDC_converter.yaml",
                "BLDC1": "BLDC_Val_ng.yaml",
                "BLDC2": "BLDC_Val_ng.yaml",
                "BLDC3": "BLDC_Val_ng.yaml",
                "BLDC4": "BLDC_Val_ng.yaml"
            }

        if circuit_param is None:
            circuit_param = {
                "bat_min_soc": 0.01,
                "load_voltage": 11
            }

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]

        # maximum fuel cell stack power
        self.max_power_fc = 0

        # define circuit state parameters
        self.power_share_fuel_cell = Variable("power_share", "%")

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # make sure the battery is not totally depleted
        self.max_power_fc = self.components[0].find_limit_values()
        system_power_max = self.max_power_fc + \
                           self.load_voltage * self.components[1].return_discharging_voltage(self.load_voltage)
        # if system_power_max < self.components[2].current_power():
        #     print(f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n'
        #           f'Maximum power of System ({system_power_max}) not sufficient at step: {self.t_place}\n'
        #           f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n')
        #     raise ValueError()
        if self.components[3].rot_torque() == 0:
            pass
        elif self.components[1].soc() > self.bat_min_soc:
            self.state.val = 0
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. dcdc_fc, 3. BLDC1, 4. BLDC2, 5. BLDC3, 6. BLDC4
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 1, 0, 0, 0, 0],
                                          [0, 1, 1, 0, 0, 0, 0],
                                          [0, 1, 1, 0, 0, 0, 0],
                                          [0, 1, 1, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[1].u_in, 0),
                                        (self.components[1].i_in, 0),
                                        (self.components[2].u_out,
                                         self.components[3].calc_voltage(self.components[3].calc_current()))]
            # self.initial_conditions = [(self.components[2].u_out,
            #                                      self.components[3].calc_voltage(self.components[3].calc_current()))]
        else:
            logging.error(f'SOC of the battery critically low at step: {self.t_place}')
            raise ValueError()

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []

    def define_circuit_states(self):
        self.circuit_states = [self.power_share_fuel_cell, self.state]

    def calculate_post_values(self):
        dcdc_out = self.components[2].power_out()
        bat_out = self.components[1].power_out()
        self.power_share_fuel_cell.val = dcdc_out / (bat_out + dcdc_out) * 100
        
    
class CircuitUnregulated_H3(Circuit):
    """
    Unregulated Circuit for H3 Aerostak Fuel Cells
    """

    def __init__(self, circuit_type="unreg_H3", component_info=None, circuit_param=None):
        """
        Testing of the compressor

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
            "fc_stack": "H3_Fuel_Cell.yaml",
            "Battery": "Battery_Shepherd_unregulated.yaml",
            "BLDC": "Power_Sink.yaml",
            }

        if circuit_param is None:
            circuit_param = {
            "bat_charge_below_soc": 0.9,
            "bat_max_soc": 0.99,
            "bat_min_soc": 0.05,
            "conditioning_toggle": False,
            "conditioning_period": 10,
            "conditioning_duration": 0.1,
            "maximum_fuel_cell_power": 1000,
            "bat_min_soc": 0.,
            }

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]
        
        self.conditioning_setting = circuit_param["conditioning_toggle"]
        
        # define circuit state parameters
        self.power_share_fuel_cell = Variable("power_share", "%")

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # make sure the battery is not totally depleted
        self.max_power_fc = self.components[0].find_limit_values()
        
        if self.components[2].p_mission() == 0:
            self.state.val = 0
            pass
        
        elif self.conditioning():
            self.state.val = 1
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Power_Sink
            self.connectivity = np.array([[0,0,0],
                                          [0,0,0],
                                          [0,1,0]])
            self.boundary_conditions = [(self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
            
        # elif self.components[1].soc() > self.bat_min_soc:
        #     self.state.val = 2
        #     self.components[1].charging = False
        #     # note for the connectivity matrix: 0. FC, 1. Bat, 2. Power_Sink
        #     self.connectivity = np.array([[0,0,0],
        #                                   [0,0,0],
        #                                   [1,1,0]])
        #     self.boundary_conditions = [(self.components[1].u_in, 0),
        #                                 (self.components[1].i_in, 0)]
            
        elif self.components[2].p_mission() <= 0.95*self.components[0].power_rated \
            and self.components[1].return_discharging_voltage(i_out=0.) < 0.95*self.components[0].u_prated \
            and self.components[1].soc() > self.bat_min_soc:
            self.state.val = 3
            self.components[1].charging = True
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Power_Sink
            self.connectivity = np.array([[0,0,0],
                                          [1,0,0],
                                          [1,0,0]])
            self.boundary_conditions = [(self.components[1].u_out, 0),
                                        (self.components[1].i_out, 0)]
            
        elif self.components[1].soc() > self.bat_min_soc:
            self.state.val = 4
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Power_Sink
            self.connectivity = np.array([[0,0,0],
                                          [0,0,0],
                                          [1,1,0]])
            self.boundary_conditions = [(self.components[1].u_in, 0),
                                        (self.components[1].i_in, 0)]
            
        else:
            raise ValueError(f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n'
                  f'SOC of the battery critically low at step: {self.t_place}\n'
                  f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n')


    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = [self.conditioning]

    def define_circuit_states(self):
        self.circuit_states = [self.power_share_fuel_cell, self.state]

    def calculate_post_values(self):
        fc_out = self.components[0].i_out()*self.components[0].i_out()
        bat_out = self.components[1].power_out()
        self.power_share_fuel_cell.val = fc_out / (bat_out + fc_out) * 100
   
           
class CircuitUnregulated_H3_Diode(Circuit):
    """
    Unregulated Circuit for H3 Aerostak Fuel Cells, Battery can not be charged
    """

    def __init__(self, circuit_type="unreg_H3_Diode", component_info=None, circuit_param=None):
        """
        Testing of the compressor

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell_onedflux.yaml",  # 0
                "Diode": "Diode_unregulated.yaml",  # 1
                "Battery": "Battery_HyDDEn.yaml",  # 2
                "Power_Sink": "Power_Sink.yaml",  # 3
                "Diode_Battery": "Diode_unregulated.yaml",  # 4
                "Diode_Battery_discharging": "Diode_unregulated.yaml",  # 5
                #"Resistor_Bat_charging": "Potentiometer.yaml"
                "Resistor_Bat_charging": "Resistor_charge.yaml"# 6
            }

        if circuit_param is None:
            circuit_param = {
            "bat_charge_below_soc": 0.9,
            "bat_max_soc": 0.99,
            "bat_min_soc": 0.05,
            "conditioning_toggle": False,
            "conditioning_period": 10,
            "conditioning_duration": 0.1,
            "maximum_fuel_cell_power": 1000,
            "bat_min_soc": 0.
            }

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]
        
        self.conditioning_setting = circuit_param["conditioning_toggle"]
        
        # define circuit state parameters
        self.power_share_fuel_cell = Variable("power_share", "%")

        super().__init__(component_info, circuit_type, logging_variables=[self.power_share_fuel_cell],
                         func_post_values=[self.calculate_add_post_values], func_pre_values=[self.stop_charging])
        self.components[2].soc_start = 1

    def create_circuit_equations(self):
        # make sure the battery is not totally depleted
        self.max_power_fc = self.components[0].find_limit_values()
        
        if self.components[3].p_mission() == 0:
            self.state.val = 0
            pass
        
        elif self.conditioning():
            self.state.val = 1
            self.components[2].charging = False
            # note for the connectivity matrix: 0. FC, 1. Diode_fc, 2. Bat, 3. Power_Sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].i_in, 0),
                                        (self.components[2].u_in, 0)]

            
        # elif self.components[2].soc() > self.bat_min_soc:
        #     self.state.val = 2
        #     self.components[2].charging = False
        #     # note for the connectivity matrix: 0. FC, 1. Diode, 2. Bat, 3. Power_Sink
        #     self.connectivity = np.array([[0,0,0,0],
        #                                   [1,0,0,0],
        #                                   [0,0,0,0],
        #                                   [0,1,1,0]])
        #     self.boundary_conditions = [(self.components[2].u_in, 0),
        #                                 (self.components[2].i_in, 0)]
            
        # elif self.components[3].p_curve() <= 0.95*self.components[0].power_rated \
        #     and self.components[2].return_discharging_voltage(i_out=0.) < 0.95*self.components[0].u_prated \
        #     and self.components[2].soc() > self.bat_min_soc:
        #     self.state.val = 2
        #     self.components[2].charging = True
        #     # note for the connectivity matrix: 0. FC, 1. Diode, 2. Bat, 3. Power_Sink
        #     self.connectivity = np.array([[0, 0, 0, 0, 0],
        #                                   [1, 0, 0, 0, 0],
        #                                   [0, 1, 0, 0, 0],
        #                                   [0, 0, 0, 0, 0],
        #                                   [0, 0, 0, 0, 0]])
        #     self.boundary_conditions = [(self.components[2].u_out, 0),
        #                                 (self.components[2].i_out, 0)]

        else:  # self.components[2].soc() > self.bat_min_soc:
            self.state.val = 3
            self.components[2].charging = False
            # note for the connectivity matrix: 0. FC, 1. Diode_fc, 2. Bat, 3. Power_Sink, 4. Diode battery, 5. Diode battery discharging
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1],
                                          [0, 1, 0, 0, 1, 0],
                                          [0, 0, 1, 0, 0, 0],
                                          [0, 1, 0, 0, 1, 0]])
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1],
                                          [0, 1, 0, 0, 1, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 1, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0]])
            # self.boundary_conditions = [(self.components[2].i_out, 0)]
            # self.initial_conditions = [(self.components[2].u_in, 43)]
            
        # else:
        #     raise ValueError(f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n'
        #           f'SOC of the battery critically low at step: {self.t_place}\n'
        #           f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n')

    def calculate_add_post_values(self):
        fc_out = self.components[0].i_out()*self.components[0].i_out()
        bat_out = self.components[2].power_out()
        self.power_share_fuel_cell.val = fc_out / (bat_out + fc_out) * 100

    def stop_charging(self):
        pass
        if self.components[2].soc() > 0.999999:
            self.components[6].r = 100
        else:
            self.components[6].r = self.components[6].r_store


class CircuitUnregulated_H3_Diode_Compressor(Circuit):
    """
    Unregulated Circuit for H3 Aerostak Fuel Cells, Battery can not be charged
    """

    def __init__(self, circuit_type="unreg_H3_Diode", component_info=None, circuit_param=None):
        """
        Testing of the compressor

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell_onedflux.yaml",  # 0
                "Diode": "Diode_unregulated.yaml",  # 1
                "Battery": "Battery_HyDDEn.yaml",  # 2
                "Power_Sink": "Power_Sink.yaml",  # 3
                "Diode_Battery": "Diode_unregulated.yaml",  # 4
                "Diode_Battery_discharging": "Diode_unregulated.yaml",  # 5
                # "Resistor_Bat_charging": "Potentiometer.yaml"
                "Resistor_Bat_charging": "Resistor_charge.yaml",  # 6
                "compressor": "Compressor.yaml"
            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "conditioning_toggle": False,
                "conditioning_period": 10,
                "conditioning_duration": 0.1,
                "maximum_fuel_cell_power": 1000,
                "bat_min_soc": 0.
            }

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]

        self.conditioning_setting = circuit_param["conditioning_toggle"]

        # define circuit state parameters
        self.power_share_fuel_cell = Variable("power_share", "%")

        super().__init__(component_info, circuit_type, logging_variables=[self.power_share_fuel_cell],
                         func_post_values=[self.calculate_add_post_values], func_pre_values=[self.stop_charging])
        self.components[2].soc_start = 1

    def create_circuit_equations(self):
        # make sure the battery is not totally depleted
        self.max_power_fc = self.components[0].find_limit_values()

        if self.components[3].p_mission() == 0:
            self.state.val = 0
            pass

        elif self.conditioning():
            self.state.val = 1
            self.components[2].charging = False
            # note for the connectivity matrix: 0. FC, 1. Diode_fc, 2. Bat, 3. Power_Sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].i_in, 0),
                                        (self.components[2].u_in, 0)]

        else:  # self.components[2].soc() > self.bat_min_soc:
            self.state.val = 3
            self.components[2].charging = False
            # note for the connectivity matrix: 0. FC, 1. Diode_fc, 2. Bat, 3. Power_Sink, 4. Diode battery, 5. Diode battery discharging
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1, 0],
                                          [0, 1, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0, 0],
                                          [0, 1, 0, 0, 1, 0, 0, 0]])

    def calculate_add_post_values(self):
        fc_out = self.components[0].i_out() * self.components[0].i_out()
        bat_out = self.components[2].power_out()
        self.power_share_fuel_cell.val = fc_out / (bat_out + fc_out) * 100

    def stop_charging(self):
        pass
        if self.components[2].soc() > 0.999999:
            self.components[6].r = 100
        else:
            self.components[6].r = self.components[6].r_store


class CircuitUnregulated_Diode_test(Circuit):
    """
    one fuel cell connected to a dcdc converter, assisted in parallel by a battery connected to a power sink
    FC-DCDC-Diode-POWERSINK-BATTERY
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "NG19_Battery_Shepherd_3_6C.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "DCDC_fc": "DCDC_converter_better.yaml",
                "DCDC_bat": "DCDC_converter.yaml",
                "Diode": "Diode_unregulated.yaml"}

        if circuit_param is None:
            circuit_param = {
                "bat_min_soc": 0.01,
                "dcdc_fc": 11.7,
                "dcdc_bat": 11
            }

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.u_dcdc_fc = circuit_param["dcdc_fc"]
        self.u_dcdc_bat = circuit_param["dcdc_bat"]

        # maximum fuel cell stack power
        self.max_power_fc = 0

        # define circuit state parameters
        # self.power_share_fuel_cell = Variable("power_share", "%")

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):

        if self.components[2].current_power() == 0:
            pass
        elif self.components[1].soc() > self.bat_min_soc:
            self.state.val = 0
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_bat, 5. diode
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 1],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0]])
            self.boundary_conditions = [(self.components[3].u_out, self.u_dcdc_fc),
                                        (self.components[4].u_out, self.u_dcdc_bat),
                                        (self.components[1].u_in, 0),
                                        (self.components[1].i_in, 0)]
        else:
            logging.error(f'SOC of the battery critically low at step: {self.t_place}')
            raise ValueError()

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []

    def define_circuit_states(self):
        self.circuit_states = [self.state]


class CircuitUnregulated_NG(Circuit):
    """
    Unregulated Circuit for H3 Aerostak Fuel Cells, Battery can not be charged
    """

    def __init__(self, circuit_type="unreg_H3_Diode", component_info=None, circuit_param=None):
        """
        Testing of the compressor

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",  # 0
                "dcdc_fc": "DCDC_converter_unreg_ng_2018.yaml",  # 1
                "diode_fc": "Diode_unregulated.yaml",  # 2
                "battery": "NG19_Battery_Shepherd_3_6C.yaml",  # 3
                "diode_battery": "Diode_unregulated.yaml",  # 4
                "power_sink": "Power_Sink.yaml"  # 5
            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "conditioning_toggle": False,
                "conditioning_period": 10,
                "conditioning_duration": 0.1,
                "maximum_fuel_cell_power": 1000,
                "bat_min_soc": 0.
            }

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]

        self.conditioning_setting = circuit_param["conditioning_toggle"]

        # define circuit state parameters
        self.power_share_fuel_cell = Variable("power_share", "%")

        super().__init__(component_info, circuit_type, logging_variables=[self.power_share_fuel_cell],
                         func_post_values=[self.calculate_add_post_values])
        self("battery").soc_start = 1
        self("battery").regulated = True
        self("battery").charging = False
        self("battery").num_eq = 1

    def create_circuit_equations(self):
        # make sure the battery is not totally depleted
        self.max_power_fc = self("fc_stack").find_limit_values()

        if self("power_sink").p_mission() == 0:
            self.state.val = 0
            pass

        elif self.conditioning():
            self.state.val = 1
            # note for the connectivity matrix: 0. FC, 1. Diode_fc, 2. Bat, 3. Power_Sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("battery").i_in, 0),
                                        (self("battery").u_in, 0)]

        else:
            self.state.val = 3
            # note for the connectivity matrix: 0. FC, 1. dcdc_fc, 2. Bat, 3. Power
            self.connectivity = np.array([[0, 0, 0, 0],
                                          [1, 0, 0, 0],
                                          [0, 0, 0, 0],
                                          [0, 1, 1, 0]])
            # note for the connectivity matrix: 0. FC, 1. dcdc_fc, 2. diode_fc, 3. bat, 4. diode_bat, 5. Power
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0],
                                          [0, 0, 1, 0, 1, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, 12), (self("battery").i_in, 0),
                                        (self("battery").u_in, 0)]

    def calculate_add_post_values(self):
        fc_out = self("fc_stack").i_out() * self("fc_stack").i_out()
        bat_out = self("battery").power_out()
        self.power_share_fuel_cell.val = fc_out / (bat_out + fc_out) * 100
