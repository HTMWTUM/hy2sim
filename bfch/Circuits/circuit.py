"""
Created on 17.12.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 1.00

File Description:
    
"""
from abc import abstractmethod

import numpy as np
import copy
from ..Components.connection import Connection
from ..Utilities.input import Input
from ..Components.fuel_cell_stack import FcModel
from ..Components.battery import LiIon
from ..Components.load import PowerSink, BLDC, BleedAirSystem, HydraulicSystem
from ..Components.dcdc_converter import *
from ..Components.diode import Diode
from ..Components.resistor import *
from ..Components.component import Component
from ..Components.compressor import Compressor1

from ..Utilities.variable import Variable
from ..Components.boundary_condition import BoundaryCondition
import bfch.Utilities.data_handling as data
from matplotlib import pyplot as plt
import pandas as pd
from typing import final
import logging


class Circuit:
    """
    Super Class for all circuits
    contains all the necessary functions to solve a given circuit in steady state fashion using newtons method
    to solve the circuit at a certain timestep, the actions needed are (example in circuit_solver.py):

    * at the start of every segment: "circuit_obj_name".new_segment(Input)
    * inside loop over every timestep of the segment: "circuit_obj_name".solve_circuit()

    A Child class only needs (ex: circuit_reg_test.py):

    * __init__(self), call super().__init__() at the end of the constructor.
    * create_circuit_equations(self):
    * try_other_state(self):
    * define_circuit_states(self):
    * define_circuit_operating_conditions(self):

    """
    circuit_type = []
    components = []
    active_components = []
    active_connected_components = []
    conditioning_setting = False
    conditioning_period = 10  # length of the cycle, s
    conditioning_duration = 0.1  # length of the conditioning activity, s
    circuit_logging_variables = []
    circuit_op_conditions = []
    circuit_param = []
    core = False
    number_of_states = 1

    def __init__(self, component_info, circuit_type: str, components_list=None, passes_init_condition=2,
                 logging_variables=None, op_conditions=None, func_post_values=None, func_pre_values=None,
                 num_of_states=1):
        """
        Initializes the circuit.
        Creates all components, defines the solving parameters for newtons method
        Initializes logging variables (default: state and efficiency) and operating conditions (default: conditioning)
        creates lists of functions to call before and after each time step

        :param component_info: dictionary containing an entry for every object needed for the specific circuit
        :param circuit_type: name of the circuit
        :param components_list: list of currently available components
        :param passes_init_condition: number of components inside the circuit the set initial conditions are passed on
        :param logging_variables: LIST of object of class :obj:`~bfch.Variable` containing the variables that have to be saved,
        circuit state and efficiency are already set.
        :param op_conditions: LIST of object of class :obj:`~bfch.Variable` containing the variables defining operating conditions,
        conditioning is already set
        """
        self.circuit_type = circuit_type

        # Initialization of the components
        if components_list is None:
            components_list = {
                                        'fuelCellStack': FcModel,
                                        'liIonBattery': LiIon,
                                        'powerSink': PowerSink,
                                        'dcdc': DcDc,
                                        'dcdc_mult': DcDc_Mult,
                                        'resistor': Resistor,
                                        'bldc': BLDC,
                                        'compressor': Compressor1,
                                        'diode': Diode,
                                        'potentiometer': Potentiometer,
                                        'bleedAirSystem': BleedAirSystem,
                                        'hydraulicSystem': HydraulicSystem
            }
        self.components_masterlist = components_list
        component_data, component_list = data.get_component_data(component_info)
        self.components = self.create_components(component_data, component_list)
        Component.t = []
        logging.info(f"The initialization of the following components in {self.circuit_type} was successful: "
                     f"{component_info}")
        #print(f"The initialization of the following components in {self.circuit_type} was successful!\n"
         #     f"{component_info}")
        self.component_info = component_info

        # Initialization of the circuit solving parameters
        self.passes_init_condition = passes_init_condition
        self.t_place = 0
        self.segment = 0
        self.order = 0
        self.num_eq = 0
        self.connectivity = np.array([])
        self.boundary_conditions = None
        self.initial_conditions = None
        self.solved = Variable("solved", unit="1")

        # Initialize which variables of the circuit will be logged
        self.state = Variable("state", val_min=0, unit="1")
        self.efficiency = Variable("efficiency", unit="1")
        self.circuit_logging_variables = [self.state, self.efficiency, self.solved]
        if logging_variables is not None:
            self.circuit_logging_variables.extend(logging_variables)

        # Initialize the operating conditions of the circuit
        self.conditioning = Variable("conditioning", unit="1")
        self.segment_id = Variable("segment_id", unit="1")
        self.circuit_op_conditions = [self.conditioning, self.segment_id]
        if op_conditions is not None:
            self.circuit_op_conditions.extend(op_conditions)

        # Initialize user functions for post calculations
        self.post_values_funcs = None
        if func_post_values is not None:
            self.post_values_funcs = func_post_values

        # Initialize user functions for post calculations
        self.pre_values_funcs = None
        if func_pre_values is not None:
            self.pre_values_funcs = func_pre_values

        self.number_of_states = num_of_states

    def __call__(self, name: str):
        """
        callable returning the <Component> object corresponding to the input string.
        """

        try:
            index = list(self.component_info).index(name)
            component = self.components[index]
        except ValueError:
            component = None
        return component

# -------------------------------------------------------------------------------------------------------------------- #
    # ABSTRACT METHODS
# -------------------------------------------------------------------------------------------------------------------- #
    @abstractmethod
    def create_circuit_equations(self):
        """
        define self.connectivity and optionally self.boundary_conditions and self.initial_conditions in a child class
        essentially defining the circuit architecture and the circuit logic
        """
        logging.error(f'Please create an object of a child class, defining what circuit you want to investigate.'
                      f'Please implement the function create_circuit_equations().'
                      f'Define self.connectivity and optionally self.boundary_conditions and '
                      f'self.initial_conditions in a child class.')
        raise ValueError()

    @abstractmethod
    def state_caller(self, state: int):
        """
        define self.connectivity and optionally self.boundary_conditions and self.initial_conditions in a child class
        essentially defining the circuit architecture and the circuit logic
        """
        logging.error(f'Please create an object of a child class, defining what circuit you want to investigate.'
                      f'Please implement the function state_caller().'
                      f'Define self.connectivity and optionally self.boundary_conditions and '
                      f'self.initial_conditions in a child class')
        raise ValueError()

# -------------------------------------------------------------------------------------------------------------------- #
    # METHODS THAT MAY BE OVERRIDDEN
# -------------------------------------------------------------------------------------------------------------------- #
    def try_other_state(self):
        """
        possibility to switch the circuit if the current option does not converge
        """
        pass

    def calculate_segment_pre_values(self):
        """
        function that is called at the end of the definition of a new segment and before any calculations are done,
        to allow pre calculations, that do not require solving the circuit and that reduce the time need to perform
        the calculation
        """
        pass

# -------------------------------------------------------------------------------------------------------------------- #
    # SETUP OF CIRCUIT
# -------------------------------------------------------------------------------------------------------------------- #
    @final
    def connect_circuit(self, connectivity):
        """
        creates objects of the class :obj:`~bfch.data.Connection` modelling the connections between the existing component following the
        connectivity matrix. They are added to the list "active connected components"

        :param connectivity: Matrix with ones, where the column output flows into the row input
        """
        self.create_active_components(connectivity)
        self.active_connected_components = self.active_components.copy()
        # extract the directional component pairs and create a vector to monitor, which pairs have already been
        # visited by the following algorithm
        result = np.where(connectivity == 1)
        result_comb = list(zip(result[1], result[0]))
        visited = np.zeros(len(result[0]))

        # transform the pairs into Connections
        while len(np.where(visited == 0)[0]) != 0:
            # first pair of a connection is the first not yet visited pair
            in_out = []
            index = np.where(visited == 0)[0][0]
            in_out.append(result_comb[index])
            visited[index] = 1
            if np.where(visited == 0)[0].size == 0:
                pass
            else:
                index = np.where(visited == 0)[0][0]

                # if this is not the last pair of the list loop through all the remaining pairs, and append
                # the pairs that have either the same input or the same output since this means they must be
                # electrically connected
                j = 0
                while j < len(in_out):
                    last_non_visited = np.where(visited == 0)[0][-1]
                    for i in range(index, last_non_visited + 1):
                        if in_out[j][0] == result_comb[i][0] or in_out[j][1] == result_comb[i][1]:
                            if result_comb[i] in in_out:
                                pass
                            else:
                                in_out.append(result_comb[i])
                            visited[i] = 1
                    if np.where(visited == 0)[0].size == 0:
                        break
                    index = np.where(visited == 0)[0][0]
                    j += 1

            in_nums = []
            out_nums = []
            # gather all the inputs and outputs of a connection
            for j in range(len(in_out)):
                in_nums.append(in_out[j][0])
                out_nums.append(in_out[j][1])

            # make sure that there are no duplicates
            in_nums = np.unique(in_nums)
            out_nums = np.unique(out_nums)
            input_objs = []
            output_objs = []

            # create lists of the input objects and output objects from the component list
            for i in in_nums:
                input_objs.append(self.components[i])
            for i in out_nums:
                output_objs.append(self.components[i])

            # create a new connection and append it to the component list
            new_connection = Connection(input_objs, output_objs)
            self.active_connected_components.append(new_connection)

    @final
    def create_active_components(self, connectivity):
        """
        uses the connectivity matrix to retrieve the active components of the circuit in the given time step

        :param connectivity: matrix containing the information about the connections between the components
        """
        tmp_connectivity = connectivity + connectivity.transpose()
        self.active_components = []
        for i in range(len(tmp_connectivity[0][:])):
            if np.any(tmp_connectivity[:][i]):
                self.active_components.append(self.components[i])

    @final
    def set_system_of_equations(self):
        """
        fills the active_connected_components list for further calculations
        sets initial conditions, boundary conditions and passes initial conditions to enhance solving speed
        """
        self.check_connectivity_matrix()

        self.connect_circuit(self.connectivity)

        for comp in self.active_connected_components:
            comp.set_initial_condition()

        if self.boundary_conditions:
            for bc in self.boundary_conditions:
                if len(bc) == 2:
                    self.active_connected_components.append(BoundaryCondition(bc[0], bc[1]))
                elif len(bc) == 3:
                    self.active_connected_components.append(BoundaryCondition(bc[0], bc[1], second_variable=bc[2]))
                elif len(bc) == 5:
                    self.active_connected_components.append(BoundaryCondition(bc[0], bc[1], second_variable=bc[2], f_lam=bc[3], df_lam=bc[4]))

        #for comp in self.active_connected_components:
         #   comp.pass_initial_condition()

        for i in range(self.passes_init_condition):
            pass
            #for comp in self.active_components:
             #   comp.pass_initial_condition()

            #for comp in self.active_connected_components:
             #   comp.pass_initial_condition()

        if self.initial_conditions:
            for ic in self.initial_conditions:
                var = ic[0]
                var.val = ic[1]

# -------------------------------------------------------------------------------------------------------------------- #
    # CALCULATION OF CIRCUIT
# -------------------------------------------------------------------------------------------------------------------- #
    @final
    def solve_circuit(self, t_place, allowed_dev, max_solver_steps, convergence_crit, solver_step_factor,
                      step_factor_reduction_speed, verbose=True):
        """
        calculates the values of all the currents and voltages inside the circuit as well as all the additional values
        defined in components or circuits using calculate_post_values

        :param solver_step_factor: factor used to reduce the step size of the newton algorithm
        :param convergence_crit: highest allowed value of a component function that leads to stopping the calculation
        :param max_solver_steps: maximum number of steps the solver is allowed to make
        :param t_place: current position of the time cursor
        :param allowed_dev: tolerable error in the calculation - checked after the convergence criterion
        """
        self.update_tplace(t_place)
        self.solved.val = False
        self.connectivity = None
        self.calculate_pre_values()
        self.create_circuit_equations()
        if self.connectivity is not None:
            self.set_system_of_equations()
            self.solve_nlse_newton(max_solver_steps, convergence_crit, solver_step_factor, step_factor_reduction_speed, verbose)
            if self.solved() == 1:
                self.check_computation(allowed_dev, verbose)
            self.calculate_post_values()
            self.boundary_conditions = None
            self.initial_conditions = None
        else:
            logging.error(f'No connectivity matrix at {self.t_place}. No state is selected.'
                          f'Most probable cause: the power limit of the system is reached/exceeded'
                          f' or the power is 0. '
                          f'Current State = {self.state()}')

    @final
    def solve_nlse_newton(self, max_solver_steps, convergence_crit, solver_step_factor, step_factor_reduction_speed, verbose=True):
        """
        uses a version of newtons algorithm to solve the nonlinear set of equations
        stops either if the convergence criterion is met, the number of iterations is exceeded or nan values appear
        """
        self.check_solvability()
        self.segment_sto=copy.copy(self.segment)
        if not self.t_place==0 and self.state.val==self.state.prev_val and self.segment_sto==self.segment: 
            self.set_all_x_to_prev()
        
        f = self.circuit_function()
        f_sto = f.copy()
        delta_f = np.linalg.norm(f)
        delta_f_all = [delta_f]
        i = 1
        steps = []
        x_sto = self.current_var_values()
        while delta_f > convergence_crit:
            df = self.circuit_function_derivative()
            try:
                z_k_plus_1 = solver_step_factor*np.linalg.solve(df, np.array(f)*(-1.))#[i_f * (-solver_step_factor) for i_f in f])
            except np.linalg.LinAlgError:
                for row in range(df.shape[0]):
                    is_all_zero = not np.any(df[row, :])
                    if is_all_zero:
                        logging.info(f'Row: {row} contains only 0')
                for column in range(df.shape[1]):
                    is_all_zero = not np.any(df[:, column])
                    if is_all_zero:
                        logging.info(f'Column: {column} contains only 0')
                logging.error(f'{i}th step has a singular df at {self.t_place} and state {self.state()}.'
                              f'Current f = {f} '
                              f' {self.current_var_values()}. No Convergence!')
                raise np.linalg.LinAlgError
            if len(steps) != 0:
                steps = np.column_stack((steps, z_k_plus_1))
            else:
                steps = z_k_plus_1.copy()

            self.update_x(z_k_plus_1)
            x_sto = np.column_stack((x_sto, self.current_var_values()))
            f = self.circuit_function()
            f_sto = np.column_stack((f_sto, f))
            f_abs = [abs(ele) for ele in self.circuit_function_result()]
            delta_f = max(f_abs)
            delta_f_all.extend([delta_f])
            i += 1
            
            if i % step_factor_reduction_speed == 0 and i > 10 and delta_f < 3000:
                solver_step_factor = 0.05
                if i > 600:
                    solver_step_factor = 0.001
                if i > 2000:
                    solver_step_factor = 0.001
                if verbose:
                    logging.info(f'{i} - solver factor: {solver_step_factor}')
                    logging.info(f'{i} - deltaf: {delta_f}')

            if i == max_solver_steps:
                if verbose:
                    logging.info(f'Reached limit of {i} steps at {self.t_place}. No Convergence!')
                    logging.info(f'Most probable cause: the power limit of the system is reached/exceeded.')
                    logging.info(f'Current f = {f}')
                    logging.info(f'Current x = {self.current_var_values()}')
                    logging.info(f'Current State = {self.state()}')
                plotting = False
                if plotting:
                    plt.figure(0)
                    plt.plot(delta_f_all)
                    for j in range(x_sto.shape[0]):
                        plt.figure(j+1)
                        plt.plot(x_sto[:][j])
                    for j in range(x_sto.shape[0]):
                        plt.figure(j*2+1)
                        plt.plot(f_sto[:][j])
                    plt.show()
                # self.set_all_x_to_zero()
                self.solved.val = False
                break

            self.check_nan(f, self.current_var_values(), i)
        self.solved.val = True
        if verbose: logging.info(f'number of steps taken: {i}| solver factor: {solver_step_factor}')

    @final
    def circuit_function(self):
        """
        loops through all active components and calls :func:`~bfch.Component.f` and multiplying the result with the component
        specific newton function factor

        :return: result of all the functions of the components as a list
        """
        result = []
        for comp in self.active_connected_components:
            result.extend(np.multiply(comp.f(), comp.newton_function_factor))
        return result

    def circuit_function_result(self):
        """
        loops through all active components and calls :func:`~bfch.Component.f`

        :return: result of all the functions of the components as a list
        """
        result = []
        for comp in self.active_connected_components:
            result.extend(comp.f())
        return result

    @final
    def circuit_function_derivative(self):
        """
        loops through all active components and their unknown to construct the matrix of derivatives needed for newton

        :return: the matrix of derivative of f wrt to x
        """
        # the organisation of the derivatives in the matrix needs to fit the order in circuit_function
        result = []
        for active_comp in self.active_components:
            for d_unknown in active_comp.unknowns:
                column = []
                for active_conn_comp in self.active_connected_components:
                    df_tmp = active_conn_comp.df(d_unknown)
                    if np.isnan(df_tmp).any() or np.isinf(df_tmp).any():
                        logging.error(f'{active_conn_comp.name} has nan of inf in df')
                    column.extend(active_conn_comp.df(d_unknown))
                if len(result) == 0:
                    result = column
                else:
                    result = np.column_stack((result, column))
        return result

    @final
    def update_x(self, z_k_plus_1):
        """
        perform the update step on the unknowns of the active components

        :param z_k_plus_1: step size, needs to match the dimension of x
        """
        z_cursor = 0
        for comp in self.active_components:
            z_cursor = comp.update_var(z_k_plus_1, z_cursor)

    @final
    def current_var_values(self):
        """
        :return: current value of x as a list
        """
        x = []
        for i in self.active_components:
            for j in i.unknowns:
                x.extend([j.val])
        return x
    
    @final
    def prev_var_values(self):
        """
        :return: current value of x as a list
        """
        x = []
        for i in self.active_components:
            for j in i.unknowns:
                x.extend([j.prev_val])
        return x

    @final
    def set_all_x_to_zero(self):
        """
        sets all the unknowns to zero
        """
        for i in self.active_components:
            for j in i.unknowns:
                j.val = 0

    @final
    def set_all_x_to_prev(self):
        """
        sets all the unknowns to zero
        """
        for i in self.active_components:
            for j in i.unknowns:
                j.val = j.prev_val

# -------------------------------------------------------------------------------------------------------------------- #
    # CHECK FUNCTIONS
# -------------------------------------------------------------------------------------------------------------------- #
    @final
    def check_computation(self, allowed_dev, verbose):
        """
        checks whether the resulting mean of f is below the allowed deviation

        :param allowed_dev: maximum allowed mean value of f at the component level
        """
        i = 0
        stop_calculation = False
        for component in self.active_connected_components:
            if np.mean(component.f()) > allowed_dev:
                if verbose:
                    logging.error(f'Calculation did not converge at step: {self.t_place} and for component {i}. '
                                  f'f is: {component.f()}')
                stop_calculation = True
            i += 1
        if stop_calculation:
            # raise ValueError
            self.solved.val = False

    @final
    def check_nan(self, f, var_values, i):
        """
        looks for nan values and terminates if one is found

        :param f: result vector of functions
        :param var_values: current values of the variables
        :param i: step of newtons method
        """
        if np.isnan(f).any():
            logging.error(f'{i}th step has a nan in f at {self.t_place}. {f}. No Convergence!')
            raise ValueError
        if np.isnan(var_values).any():
            logging.error(f'{i}th step has a nan in x at {self.t_place}. {var_values}. No Convergence')
            raise ValueError

    @final
    def check_solvability(self):
        """
        checks whether the number of equations fits the number of unknowns, terminates if not

        """
        self.order = 0
        self.num_eq = 0
        for comp in self.active_connected_components:
            self.order += comp.order
            self.num_eq += comp.num_eq

        if self.num_eq == self.order:
            pass
        else:
            logging.error(f'Order of problem {self.order} does not match the number of equations {self.num_eq} at: '
                          f'{self.t_place} and State {self.state()}')
            raise ValueError()

    @final
    def check_connectivity_matrix(self):
        """
        checks whether the connectivity matrix is square and not empty, terminates if one of those is true
        """
        if self.connectivity is None or len(self.connectivity) == 0:
            logging.error(f'Connectivity Matrix must be set for a calculation to be done. Error at: {self.t_place}')
            raise ValueError()
        if self.connectivity.shape[0] != self.connectivity.shape[1]:
            logging.error(f'Connectivity Matrix must be square, please check. Error at: {self.t_place}')
            raise ValueError()
        connectivity_t = self.connectivity.T
        for i in range(len(connectivity_t[0])):
            for j in range(len(connectivity_t[0])):
                if connectivity_t[i][j] == self.connectivity[i][j] == 1:
                    logging.error(f'Connectivity Matrix must not contain symmetric parts, please check. Error at: {self.t_place}')
                    raise ValueError()

# -------------------------------------------------------------------------------------------------------------------- #
    # Initialization of class and segment
# -------------------------------------------------------------------------------------------------------------------- #
    @final
    def new_segment(self, input_data: Input):
        """
        data of new segment is propagated into the circuit and its components

        :param input_data: contains data of segment
        """
        add_values = np.zeros((1, len(input_data.segment[0])))

        for circuit_state in self.circuit_logging_variables:
            circuit_state.new_segment(add_values)
        for circuit_op_condition in self.circuit_op_conditions:
            try:
                index = input_data.conditions.index(f'{circuit_op_condition.name} {circuit_op_condition.unit}')
            except ValueError:
                logging.error(f'Circuit Operating Condition: {circuit_op_condition.name} is not in the input file.')
                raise ValueError()
            circuit_op_condition.new_segment(input_data.segment[index])

        Component.t = np.append(Component.t, input_data.segment[0])
        for comp in self.components:
            comp.new_segment(input_data)
        self.calculate_segment_pre_values()
        self.segment+=1

    @final
    def pass_output(self, index=0):
        """
        collects all the calculated data of the segment from all the circuit. concerns operating conditions and logging
        variables

        :param index: position of the time cursor at the start of the segment

        :return: a Dataframe with states and operating conditions
        """
        data_tmp = []
        columns = []
        data_tmp.append(Component.t[index:])
        columns.append("time")
        for circuit_state in self.circuit_logging_variables:
            columns.append(f'{self.circuit_type} {circuit_state.name} {circuit_state.unit}')
            data_tmp.append(circuit_state.return_val(index))
        for circuit_op_condition in self.circuit_op_conditions:
            columns.append(f'{self.circuit_type} {circuit_op_condition.name} {circuit_op_condition.unit}')
            data_tmp.append(circuit_op_condition.return_val(index))
        output = pd.DataFrame(data_tmp, columns)
        return output.transpose()

    @final
    def create_components(self, component_data, component_list):
        """
        creates the components needed for a specific circuit

        :param component_data: list of dictionaries containing the data of every single component
        :param component_list: list of the component names

        :return: list of Component Objects
        """
        components = []
        for i in range(len(component_list)):
            if component_data[i]['component'] not in self.components_masterlist.keys():
                logging.error(f'Either {component_data[i]["component"]} is not yet added to the list of available '
                              f'components or this component does not exist, or the keyword component is missing in '
                              f'the yaml file containing the data.')
                raise ValueError

            components.append(self.components_masterlist[component_data[i]['component']](component_list[i],
                                                                                         component_data[i]))

        if len(components) < 2:
            logging.error(f'A circuit must have 2 components or more, please check your component list.')
            raise ValueError()

        for i in range(len(components)-1):
            for j in range(i+1, len(components)):
                if components[i] == components[j]:
                    logging.error(f'Two of the components are identical. '
                          f'Every component of the circuit must be a separate Object.')
                    raise ValueError()
                if components[i].name == components[j].name:
                    logging.info(f'Two of the components have identical names. '
                                 f'It is useful two have separate names for the output')
        return components

    @final
    def update_tplace(self, t_place):
        """
        updates the value of the current position of the time cursor in all components and variables
        """
        self.t_place = t_place
        Component.t_place = t_place
        Variable.t_place = t_place

# -------------------------------------------------------------------------------------------------------------------- #
    # PRE AND POST CALCULATIONS
# -------------------------------------------------------------------------------------------------------------------- #
    @final
    def calculate_pre_values(self):
        """
        function called after every time step to calculate or update certain values. Does it for the circuit and the
        components
        e.g.: soc, temperatures, fuel consumptions, etc.
        """
        # Calculate the pre values of all the components
        for comp in self.components:
            comp.calculate_pre_values()

        # Calculate the default pre values of the circuit

        # Calculate the pre values defined by the user
        if self.pre_values_funcs is not None:
            [f() for f in self.pre_values_funcs]

    @final
    def calculate_post_values(self):
        """
        function called after every time step to calculate or update certain values
        e.g.: efficiency, soc, temperatures, fuel consumptions, etc.
        """
        efficiency = 1
        p_loss = 0
        p_out = 0
        # Calculate the post values of all the components plus efficiency of circuit avoiding multiple loops
        for comp in self.active_components:
            comp.calculate_post_values()
            eff = comp.efficiency()
            if eff == 0:
                logging.error(f'{comp.name} has eff = 0')
            else:
                p_loss += comp.power_out() * ((1 / comp.efficiency()) - 1)
            if comp.system_output:
                p_out += comp.power_out()

        # Calculate the default post values of the circuit
        self.efficiency.val = p_out / (p_out + p_loss)
        if self.efficiency.prev_val >= 0.9999:
            self.efficiency.prev_val = self.efficiency.val

        # Calculate the post values defined by the user
        if self.post_values_funcs is not None:
            [f() for f in self.post_values_funcs]

    @final
    def calculate_circuit_mass_properties(self):
        """
        :return: Mass of all the circuit components in [kg]
        """
        core_mass = 0.
        core_volume = 0.
        bleedAirSystem_mass = 0.
        bleedAirSystem_volume = 0.
        hydraulicSystem_mass = 0.
        hydraulicSystem_volume = 0.
        for comp in self.components:
            if comp.core:
                core_mass += comp.calculate_mass()
                core_volume += comp.calculate_volume()
            elif comp.comp_type == "bleedAirSystem":
                bleedAirSystem_mass += comp.calculate_mass()
                bleedAirSystem_volume += comp.calculate_volume()
            elif comp.comp_type == "hydraulicSystem":
                hydraulicSystem_mass += comp.calculate_mass()
                hydraulicSystem_volume += comp.calculate_volume()        

        return [core_mass, bleedAirSystem_mass, hydraulicSystem_mass],  \
               [core_volume, bleedAirSystem_volume, hydraulicSystem_volume]

    @final
    def calculate_circuit_rated_power(self):
        """
        :return: Mass of all the circuit components in [kg]
        """
        circuit_rated_power = 0.
        for comp in self.components:
            circuit_rated_power += comp.calculate_rated_power()
        return circuit_rated_power

    @final
    def scale_circuit_rated_power(self, scaling_factor, scaling_factor_bleed):
        """
        scale the circuit according to the scaling factor
        """
        circuit_rated_power = 0.
        for comp in self.components:
            if comp.core:
                comp.scale_rated_power(scaling_factor)
            # elif comp.comp_type == "bleedAirSystem":
            #     comp.scale_rated_power(scaling_factor_bleed)

        # print(self.calculate_circuit_rated_power())
