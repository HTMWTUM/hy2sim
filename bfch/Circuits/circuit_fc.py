"""
Created on 01.03.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
import numpy as np
from .circuit import Circuit


class CircuitFuelCell(Circuit):
    """
    Model of a Circuit only containing a load and the fuel cell
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        contains the same parameters as the other circuits to enable looping

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        if component_info is None:
            component_info = {
                            "fc_stack": "NG19_Fuel_Cell.yaml",
                            "power_sink": "Power_Sink.yaml"
                        }
        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        self.state.val = 0
        self.connectivity = np.array([[0, 0],
                                      [1, 0]])

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []

    def define_circuit_states(self):
        self.circuit_states = [self.state]
