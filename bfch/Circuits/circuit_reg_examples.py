"""
Created on 7/8/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:
    
"""
import logging

import numpy as np
from .circuit import Circuit


class CircuitRegulatedUncontrolledPowerSharing(Circuit):
    """
    The Fuel Cell stack is connected to the load via a dcdc converter and a diode. The battery has a discharging and
    a charging circuit connected in parallel to the fuel cell stack. They can be turned on and off through switches.
    This is modelled using if else in create_circuit_equations
    """

    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        Regulated circuit with constant dcdc settings and conditioning option

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing [name: filename.yaml] pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """

        # default parameters
        if component_info is None:
            # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. dcdc_bat_di, 5. diode_bat_di, 6. dcdc_bat_ch,
            # 7. diode_bat_ch, 8. power_sink
            component_info = {
                "fc_stack": "NG19_Fuel_Cell_onedflux.yaml",  # 0
                "dcdc_fc": "DCDC_converter.yaml",  # 1
                "diode_fc": "Diode_regulated.yaml",  # 2
                "battery": "Battery_HyDDEn.yaml",  # 3
                "dcdc_bat_di": "DCDC_converter.yaml",  # 4
                "diode_bat_di": "Diode_regulated.yaml",  # 5
                "dcdc_bat_ch": "DCDC_converter.yaml",  # 6
                "diode_bat_ch": "Diode_regulated.yaml",  # 7
                "power_sink": "Power_Sink.yaml",  # 8

            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.97,
                "bat_max_soc": 0.9999,
                "bat_min_soc": 0.05,
                "load_voltage": 40,
                "bat_charging_i": 1,
                "bat_discharging_i": 30,
                "conditioning_toggle": False,
                "conditioning_period": 30,
                "conditioning_duration": 0.1
            }

        # Initialization of specific thresholds
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]
        self.conditioning_setting = circuit_param["conditioning_toggle"]
        self.conditioning_period = circuit_param["conditioning_period"]
        self.conditioning_duration = circuit_param["conditioning_duration"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type, num_of_states=4)
        self("battery").regulated = True
        self("battery").num_eq = 1
        self("battery").soc_start = 1

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self("battery").soc():
            self("battery").charging = False

        self.max_power_fc = self("fc_stack").maximum_power() * self("dcdc_fc").beta
        self.max_charging_power = self("battery").return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self("dcdc_bat_di").beta

        if self.conditioning():
            self.state_caller(0)

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc*0.95 < self("power_sink").p_mission() and self("battery").soc() > self.bat_min_soc:
            self.state_caller(2)

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc*0.95 > self("power_sink").p_mission()+self.max_charging_power and \
                self.bat_min_soc < self("battery").soc() < self.bat_charge_below_soc:
            self.state_caller(3)

        # State 1: SOC very high, load < max power of the fuel cell
        elif self.max_power_fc > self("power_sink").p_mission():
            self.state_caller(1)

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}. '
                          f'Power Fuel Cell: {self.max_power_fc}.'
                          f'SOC: {self("battery").soc()}.'
                          f'Power needed: {self("power_sink").p_mission()}.')
            raise ValueError()

    def state_caller(self, state: int):
        if state == 0:
            self.state.val = state
            self("battery").charging = False
            # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. dcdc_bat_di, 5. diode_bat_di, 6. dcdc_bat_ch,
            # 7. diode_bat_ch, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_bat_di").u_out, self.load_voltage),
                                        (self("battery").i_in, 0),
                                        (self("battery").u_in, 0)]
        elif state == 1:
            self.state.val = state
            self("battery").charging = False
            # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. dcdc_bat_di, 5. diode_bat_di, 6. dcdc_bat_ch,
            # 7. diode_bat_ch, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage)]
        elif state == 2:
            self.state.val = state
            self("battery").charging = False
            # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. dcdc_bat_di, 5. diode_bat_di, 6. dcdc_bat_ch,
            # 7. diode_bat_ch, 8. power_sink
            # Mit Dioden
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 1, 0, 0, 0]])
            # ohne Dioden
            # self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
            #                               [1, 0, 0, 0, 0, 0, 0, 0, 0],
            #                               [0, 0, 0, 0, 0, 0, 0, 0, 0],
            #                               [0, 0, 0, 0, 0, 0, 0, 0, 0],
            #                               [0, 0, 0, 1, 0, 0, 0, 0, 0],
            #                               [0, 0, 0, 0, 0, 0, 0, 0, 0],
            #                               [0, 0, 0, 0, 0, 0, 0, 0, 0],
            #                               [0, 0, 0, 0, 0, 0, 0, 0, 0],
            #                               [0, 1, 0, 0, 1, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage),
                                        (self("dcdc_bat_di").i_in, self.bat_discharging_i),
                                        (self("battery").u_in, 0),
                                        (self("battery").i_in, 0)]
        elif state == 3:
            self.state.val = state
            self("battery").charging = True
            # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. dcdc_bat_di, 5. diode_bat_di, 6. dcdc_bat_ch,
            # 7. diode_bat_ch, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 1, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage),
                                        (self("dcdc_bat_ch").i_out, self.bat_charging_i),
                                        (self("battery").i_out, 0),
                                        (self("battery").u_out, 0)]
        else:
            logging.error(f"State {state} has not been defined. No connectivity matrix was set.")
