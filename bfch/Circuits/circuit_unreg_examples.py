"""
Created on 7/8/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:
    
"""
import numpy as np
from .circuit import Circuit
from ..Utilities.variable import Variable


class CircuitUnregulatedDiodeChargeResistor(Circuit):
    """
    Unregulated Circuit for H3 Aerostak Fuel Cells. The fuel cell stack is connected to the load and the battery via a
    diode. The Battery discharging poles are connected to the load via a diode and the charging is done via a diode and
    a resistor to limit the current
    """

    def __init__(self, circuit_type="unreg_H3_Diode", component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing [name: filename.yaml] pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell_onedflux.yaml",  # 0
                "diode": "Diode_unregulated.yaml",  # 1
                "battery": "Battery_HyDDEn.yaml",  # 2
                "power_sink": "Power_Sink.yaml",  # 3
                "diode_battery_discharging": "Diode_unregulated.yaml",  # 4
                "diode_battery_charging": "Diode_unregulated.yaml",  # 5
                "resistor_bat_charging": "Resistor_Charge.yaml"  # 6
            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "conditioning_toggle": False,
                "conditioning_period": 10,
                "conditioning_duration": 0.1,
                "maximum_fuel_cell_power": 1000,
            }

        self.max_power_fc = 0

        # Initialization of specific thresholds and parameters
        self.bat_min_soc = circuit_param["bat_min_soc"]

        self.conditioning_setting = circuit_param["conditioning_toggle"]

        # define circuit state parameters
        self.power_share_fuel_cell = Variable("power_share", "%")

        super().__init__(component_info, circuit_type, logging_variables=[self.power_share_fuel_cell],
                         func_post_values=[self.calculate_add_post_values], func_pre_values=[self.stop_charging])
        self("battery").soc_start = 1
        Variable.reset = False

    def create_circuit_equations(self):
        # make sure the battery is not totally depleted
        self.max_power_fc = self("fc_stack").maximum_power()

        if self("power_sink").p_mission() == 0:
            self.state.val = 0
            pass

        elif self.conditioning():
            self.state_caller(0)
        else:
            self.state_caller(1)

    def state_caller(self, state: int):
        if state == 0:  # conditioning
            self.state.val = state
            self("battery").charging = False
            # connectivity: 0. FC, 1. Diode FC, 2. Bat, 3. Load, 4. Diode Bat Dis, 5. Diode Bat Char, 6. Resistor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0],
                                          [0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("battery").i_in, 0),
                                        (self("battery").u_in, 0)]
        if state == 1:  # normal operations
            self.state.val = state
            self("battery").charging = False
            # connectivity: 0. FC, 1. Diode FC, 2. Bat, 3. Load, 4. Diode Bat Dis, 5. Diode Bat Char, 6. Resistor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1],
                                          [0, 1, 0, 0, 1, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 1, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0]])

    def calculate_add_post_values(self):
        fc_out = self("fc_stack").i_out() * self("fc_stack").u_out()
        bat_out = self("battery").power_out()
        self.power_share_fuel_cell.val = fc_out / (bat_out + fc_out) * 100

    def stop_charging(self):
        pass
        if self("battery").soc() > 0.999999:
            self("resistor_bat_charging").r = 100
        else:
            self("resistor_bat_charging").r = self("resistor_bat_charging").r_store
