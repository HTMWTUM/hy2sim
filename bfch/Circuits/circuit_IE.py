"""
Created on 7/8/2022
Author: Victor Zappek, Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:
    
"""
import copy
import numpy as np
from .circuit import Circuit
from ..Utilities.variable import Variable


class CircuitIntelligentEnergy(Circuit):
    """
    The Fuel Cell stack is connected to the load via a dcdc converter and a diode. The battery has a discharging and
    a charging circuit connected in parallel to the fuel cell stack. They can be turned on and off through switches.
    This is modelled using if else in create_circuit_equations
    """

    def __init__(self, circuit_type: str, component_info={}, circuit_param={}):
        """
        Regulated circuit with constant dcdc settings and conditioning option

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing [name: filename.yaml] pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        
        # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. dcdc_bat_di, 5. diode_bat_di, 6. dcdc_bat_ch,
        # 7. diode_bat_ch, 8. power_sink
        component_info_default = {
            "fc_stack": "FC_generic_singlecell.yaml",  # 0
            # "dcdc_fc": "DCDC_converter_Mult.yaml",  # 1
            "dcdc_fc": "DCDC_converter.yaml",  # 1
            "diode_fc": "Diode_regulated.yaml",  # 2
            "battery": "Battery_generic_singlecell.yaml",  # 3
            "diode_bat_di": "Diode_unregulated.yaml",  # 5
            "dcdc_bat_ch": "DCDC_converter.yaml",  # 6
            "diode_bat_ch": "Diode_regulated.yaml",  # 7
            "power_sink": "Power_Sink.yaml",  # 8
        }
        component_info_default.update(component_info)
        component_info=component_info_default
        
        circuit_param_default = {
            "bat_charge_below_soc": 0.97,
            "bat_max_soc": 0.9999,
            "bat_start_soc": 0.9999,
            "bat_min_soc": 0.05,
            "bat_charging_i_max": 6,
            "conditioning_toggle": False,
            "conditioning_period": 30,
            "conditioning_duration": 0.1,
            # "dcdc_fc_mult":0.72,
            "u_target":51.4,
        }
        
        circuit_param_default.update(circuit_param)
        circuit_param=circuit_param_default
        # Set circuit_params as attributes
        for key in circuit_param:
            setattr(self, key, circuit_param[key])
            
        self.u_target=circuit_param['u_target']/0.98
        
        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type, func_post_values=[self.calculate_add_post_values])
        self("battery").regulated = True
        self("battery").num_eq = 1
        self("battery").soc_start = self.bat_start_soc

    def create_circuit_equations(self):
        charge_threshold = self.bat_charge_below_soc
        if self("battery").charging: charge_threshold = self.bat_max_soc
        self("battery").charging = False
            
        self.power_rated_dcdc_fc = self("fc_stack").power_rated * self("dcdc_fc").beta *0.99# self("diode_fc").u_out.prev_val/self("diode_fc").u_in.prev_val
        self.max_charging_power = self("battery").return_charging_voltage(self.bat_charging_i_max) \
                                  * self.bat_charging_i_max / self("dcdc_bat_ch").beta
                          
        # print("power:", round(self("power_sink").p_mission.prev_val))
        # print("power share:", round(self("fc_stack").i_out.prev_val*self("fc_stack").u_out.prev_val), round(self("diode_fc").i_out.prev_val*self("diode_fc").u_out.prev_val), round(self("battery").i_out.prev_val*self("battery").u_out.prev_val))
        # print("voltage:", round(self("fc_stack").u_out.prev_val,1),  round(self("diode_fc").u_out.prev_val,1),  round(self("battery").u_out.prev_val,1))
        # print("current:", round(self("fc_stack").i_out.prev_val,1),  round(self("diode_fc").i_out.prev_val,1),  round(self("battery").i_out.prev_val,1), round(self("battery").i_in.prev_val,3))
        # print("SoC:", round(self("battery").soc()*100,2),round(self("battery").soc.prev_val*100,2))
        
        # State 0: Conditioning
        if self("battery").soc() < self.bat_min_soc:
            raise ValueError(f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n'
                  f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}\n'
                  f'Power Fuel Cell: {self.max_power_fc}\n'
                  f'SOC: {self("battery").soc()}\n'
                  f'Power needed: {self("power_sink").p_mission()}\n'
                  f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n')
        
        elif self.conditioning():
            self.state.val = 0
            self("battery").charging = False
            # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. diode_bat_di, 5. dcdc_bat_ch,
            # 6. diode_bat_ch, 7. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0, 0, 0]])
            
            self.boundary_conditions = [(self("battery").i_in, 0),
                                        (self("battery").u_in, self("battery").u_in.val_min)]
        # State 1: Battery charging
        elif self.power_rated_dcdc_fc > self("power_sink").p_mission()*1.05 and \
                self("battery").soc() < charge_threshold:
            

            self.state.val = 1
            self("battery").charging = True
            
            i_charge = min(self.bat_charging_i_max, (self.power_rated_dcdc_fc-self("power_sink").p_mission())/self("battery").voltage_reversible_charging())#*self("dcdc_bat_ch").beta)
            
            # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. diode_bat_di, 5. dcdc_bat_ch,
            # 6. diode_bat_ch, 7. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.u_target),
                                        (self("dcdc_bat_ch").i_out, i_charge),
                                        (self("battery").i_out, 0),
                                        (self("battery").u_out, self("battery").u_out.val_min)]


        # State 2: Power sharing operation
        elif self("battery").soc() > self.bat_min_soc:
            Variable.reset = False
            # self("diode_fc").reset = False
            self("battery").charging = False                
            
            # 0. fc_stack, 1. dcdc_fc, 2. diode_fc, 3. battery, 4. diode_bat_di, 5. dcdc_bat_ch,
            # 6. diode_bat_ch, 7. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 1, 0, 0, 0]])
            
            if self.power_rated_dcdc_fc < self("power_sink").p_mission():
                self.state.val = 2
                self.boundary_conditions = [(self("fc_stack").i_out, self("fc_stack").power_rated, self("fc_stack").u_out, lambda i,u:i*u, (lambda i,u:u,lambda i,u:i)),
                                            (self("battery").u_in, self("battery").u_in.val_min),
                                            (self("battery").i_in, 0)]
            else:
                self.state.val = 3
                self.boundary_conditions = [(self("dcdc_fc").u_out, self.u_target),
                                            (self("battery").u_in, self("battery").u_in.val_min),
                                            (self("battery").i_in, 0)]
            
            

    def calculate_add_post_values(self):
        fc_out = self("fc_stack").i_out() * self("fc_stack").u_out()
        bat_out = self("battery").power_out()
            
        # print("t=",round(self("fc_stack").t[self.t_place],2),"   State:",int(self.state.val),"\n")
        print(self.t_place, self.segment, self("battery").t[self("battery").t_place], int(self.state()), int(self("power_sink").p_mission()),int(self("fc_stack").power_out()), int(self("dcdc_fc").power_out()), int(self("diode_fc").power_out()), int(self("battery").power_out()), int(self("diode_bat_di").power_out()), round(self("battery").soc(),3))
        print(round(self("battery").u_out(),2), round(self("battery").i_out(),2))
                    
