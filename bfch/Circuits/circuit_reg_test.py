"""
Created on 17.12.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:
    
"""
import logging

import numpy as np
from .circuit import Circuit
from ..Utilities.variable import Variable


class CircuitRegulated1(Circuit):
    """
    basic regulated circuit
    no conditioning possible FC-DC-Load-DC-Bat-DC-Load
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "DCDC_fc": "DCDC_converter.yaml",
                "DCDC_bat_ch": "DCDC_converter.yaml",
                "DCDC_bat_di": "DCDC_converter.yaml"
            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "load_voltage": 12,
                "bat_charging_i": 1,
                "bat_discharging_i": 2
            }

        # Initialization of specific thresholds and parameters
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self.components[1].soc():
            self.components[1].charging = False

        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta
        self.max_charging_power = self.components[1].return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self.components[5].beta

        # State 1: SOC very high, load < max power of the fuel cell
        if (self.max_power_fc > self.components[2].current_power()) \
                and ((self.components[1].soc() > self.bat_charge_below_soc and self.components[1].charging is False)
                     or (self.max_power_fc < self.components[2].current_power()+self.max_charging_power)):

            self.state.val= 1
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc < self.components[2].p_mission() and self.components[1].soc() > self.bat_min_soc:

            self.state.val= 2
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[4].i_in, self.bat_discharging_i),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
            self.initial_conditions = [(self.components[4].u_in,
                                                 self.components[1].return_discharging_voltage(2))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc > self.components[2].p_mission()+self.max_charging_power and \
                self.components[1].soc > self.bat_min_soc:

            self.state.val= 3
            self.components[1].charging = True
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[5].i_out, self.bat_charging_i),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(1))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}')
            raise ValueError()

    def try_other_state(self):
        pass

    def define_circuit_states(self):
        self.circuit_states = [self.state]

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []


class CircuitRegulated2(Circuit):
    """
    Test Resistor, same circuit as regulated1, but with a resistor in front of the load
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        CircuitRegulated2 with resistance between fc_dcdc and load

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """

        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "DCDC_fc": "DCDC_converter.yaml",
                "DCDC_bat_ch": "DCDC_converter.yaml",
                "DCDC_bat_di": "DCDC_converter.yaml",
                "Resistor": "Resistor.yaml"
            }
        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "load_voltage": 12,
                "bat_charging_i": 1,
                "bat_discharging_i": 2,
                "limit_fc_power_percent_max_power": 0.9
            }

        # Initialization of specific thresholds and parameters
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]
        self.limit_power_pc = circuit_param["limit_fc_power_percent_max_power"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self.components[1].soc():
            self.components[1].charging = False

        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta * self.limit_power_pc
        self.max_charging_power = self.components[1].return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self.components[5].beta

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack -> fuel cell and battery
        if self.max_power_fc < self.components[2].p_mission() and self.components[1].soc() > self.bat_min_soc:

            self.state.val = 2
            self.components[1].charging = False
            # note for the connectivity matrix:
            # 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge, 6. Resistor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[4].i_in, self.bat_discharging_i),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
            self.initial_conditions = [(self.components[4].u_in,
                                        self.components[1].return_discharging_voltage(2))]

        # State 1: SOC very high, load < max power of the fuel cell -> only fuel cell
        elif (self.max_power_fc > self.components[2].current_power()) \
                and (self.components[1].soc() > self.bat_charge_below_soc and self.components[1].charging is False):

            self.state.val = 1
            self.components[1].charging = False
            # connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge, 6. Resistor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage)]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack -> charging battery
        elif self.max_power_fc > self.components[2].p_mission()+self.max_charging_power and \
                self.components[1].soc > self.bat_min_soc:

            self.state.val= 3
            self.components[1].charging = True
            # note for the connectivity matrix:
            # 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge, 6. Resistor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0],
                                          [0, 0, 0, 0, 0, 0, 1],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[5].i_out, self.bat_charging_i),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(1))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place};'
                          f'Required Power: {self.components[2].p_mission()};'
                          f'Max Fuel Cell Power: {self.max_power_fc};'
                          f'SOC: {self.components[1].soc()}.')

    def try_other_state(self):
        pass

    def define_circuit_states(self):
        self.circuit_states = [self.state]

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []


class CircuitRegulated3(Circuit):
    """
    Test BLDC Load and conditioning
    """

    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        CircuitRegulated1 plus BLDC and conditioning option

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """

        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "BLDC": "BLDC_Prattes.yaml",
                "DCDC_fc": "DCDC_converter.yaml",
                "DCDC_bat_ch": "DCDC_converter.yaml",
                "DCDC_bat_di": "DCDC_converter.yaml"
            }
        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "load_voltage": 12,
                "bat_charging_i": 1,
                "bat_discharging_i": 2,
                "conditioning_toggle": False
            }

        # Initialization of specific thresholds
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]
        self.conditioning_setting = circuit_param["conditioning_toggle"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self.components[1].soc():
            self.components[1].charging = False

        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta
        self.max_charging_power = self.components[1].return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self.components[5].beta

        if self.conditioning():
            self.state.val= 0
            self.components[1].charging = False
            # connectivity: 0. FC, 1. Bat, 2. BLDC, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
        # State 1: SOC very high, load < max power of the fuel cell
        elif (self.max_power_fc > self.components[2].current_power()) \
                and ((self.components[1].soc() > self.bat_charge_below_soc and self.components[1].charging is False)
                     or (self.max_power_fc < self.components[2].current_power()+self.max_charging_power)):

            self.state.val= 1
            self.components[1].charging = False
            # connectivity: 0. FC, 1. Bat, 2. BLDC, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc < self.components[2].p_mission() and self.components[1].soc > self.bat_min_soc:

            self.state.val= 2
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. BLDC, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[4].i_in, self.bat_discharging_i),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
            self.initial_conditions = [(self.components[4].u_in,
                                        self.components[1].return_discharging_voltage(2))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc > self.components[2].p_mission()+self.max_charging_power and \
                self.components[1].soc() > self.bat_min_soc:

            self.state.val= 3
            self.components[1].charging = True
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. BLDC, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[5].i_out, self.bat_charging_i),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(1))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}')
            raise ValueError()

    def try_other_state(self):
        pass

    def define_circuit_states(self):
        self.circuit_states = [self.state]

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = [self.conditioning]


class CircuitRegulated4(Circuit):
    """
    Test power sink Load and conditioning - Circuit for investigation of simple unregulated circuit
    """

    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        CircuitRegulated1 with conditioning option

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """

        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell_onedflux.yaml",
                "dcdc_fc": "DCDC_converter.yaml",
                "diode_fc": "Diode_regulated.yaml",
                "battery": "Battery_HyDDEn.yaml",
                "dcdc_bat_di": "DCDC_converter.yaml",
                "diode_bat_di": "Diode_regulated.yaml",
                "dcdc_bat_ch": "DCDC_converter.yaml",
                "diode_bat_ch": "Diode_regulated.yaml",
                "power_sink": "Power_Sink.yaml",

            }
            # component_info = {
            #     "fc_stack": "NG19_Fuel_Cell.yaml",
            #     "dcdc_fc": "DCDC_converter_unreg_ng_2018.yaml",
            #     "diode_fc": "Diode_regulated.yaml",
            #     "battery": "NG19_Battery_Shepherd.yaml",
            #     "dcdc_bat_di": "DCDC_converter_unreg_ng_2018.yaml",
            #     "diode_bat_di": "Diode_regulated.yaml",
            #     "dcdc_bat_ch": "DCDC_converter_unreg_ng_2018.yaml",
            #     "diode_bat_ch": "Diode_regulated.yaml",
            #     "power_sink": "Power_Sink.yaml",
            #
            # }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.97,
                "bat_max_soc": 0.9999,
                "bat_min_soc": 0.05,
                "load_voltage": 40,
                "bat_charging_i": 1,
                "bat_discharging_i": 30,
                "conditioning_toggle": False,
                "conditioning_period": 30,
                "conditioning_duration": 0.1
            }

        # Initialization of specific thresholds
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]
        self.conditioning_setting = circuit_param["conditioning_toggle"]
        self.conditioning_period = circuit_param["conditioning_period"]
        self.conditioning_duration = circuit_param["conditioning_duration"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)
        self("battery").regulated = True; self("battery").num_eq = 1
        self("battery").soc_start = 0.9

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self("battery").soc():
            self("battery").charging = False

        self.max_power_fc = self("fc_stack").maximum_power() * self("dcdc_fc").beta
        self.max_charging_power = self("battery").return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self("dcdc_bat_di").beta

        if self.conditioning():
            self.state.val = 0
            self("battery").charging = False
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. diode_bat_dis, 5. dcdc_dis, 6. diode_bat_char,
            #               7. dcdc_charge, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[5].u_out, self.load_voltage),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc*0.95 < self("power_sink").p_mission() and self("battery").soc() > self.bat_min_soc:

            self.state.val = 2
            self("battery").charging = False
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. dcdc_dis,
            #               5. dcdc_charge, 6. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 1, 0, 0]])
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. diode_bat_dis, 5. dcdc_dis, 6. diode_bat_char,
            #               7. dcdc_charge, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 1, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage),
                                        (self("dcdc_bat_di").i_in, self.bat_discharging_i),
                                        (self("battery").u_in, 0),
                                        (self("battery").i_in, 0)]
            #self.initial_conditions = [(self("dcdc_bat_di").u_in, self("battery").return_discharging_voltage(10))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc*0.95 > self("power_sink").p_mission()+self.max_charging_power and \
                self("battery").soc() > self.bat_min_soc:

            self.state.val = 3
            self("battery").charging = True
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. diode_bat_dis, 5. dcdc_dis, 6. diode_bat_char,
            #               7. dcdc_charge, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 1, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage),
                                        (self("dcdc_bat_ch").i_out, self.bat_charging_i),
                                        (self("battery").i_out, 0),
                                        (self("battery").u_out, 0)]
            # self.initial_conditions = [(self.components[5].u_out,
            #                            self.components[1].return_charging_voltage(1))]

            # State 1: SOC very high, load < max power of the fuel cell
        elif (self.max_power_fc > self("power_sink").p_mission()):
                 #and ((self("battery").soc() > self.bat_charge_below_soc and self("battery").charging is False)
                  #    or (self.max_power_fc < self("power_sink").p_mission() + self.max_charging_power)):

            self.state.val = 1
            self("battery").charging = False
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. diode_bat_dis, 5. dcdc_dis, 6. diode_bat_char,
            #               7. dcdc_charge, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage)]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}; '
                          f'Power Fuel Cell: {self.max_power_fc}; '
                          f'SOC: {self("battery").soc()}; '
                          f'Power needed: {self("power_sink").p_mission()}.')
            raise ValueError()


class CircuitRegulated4Compressor(Circuit):
    """
    Test power sink Load and conditioning - Circuit for investigation of simple unregulated circuit
    """

    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        CircuitRegulated1 with conditioning option

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """

        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell_onedflux.yaml",
                "diode_fc": "Diode_regulated.yaml",
                "dcdc_fc": "DCDC_converter.yaml",
                "battery": "Battery_HyDDEn.yaml",
                "dcdc_bat_di": "DCDC_converter.yaml",
                "diode_bat_di": "Diode_regulated.yaml",
                "dcdc_bat_ch": "DCDC_converter.yaml",
                "diode_bat_ch": "Diode_regulated.yaml",
                "power_sink": "Power_Sink.yaml",
                "compressor": "Compressor.yaml"

            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.97,
                "bat_max_soc": 0.9999,
                "bat_min_soc": 0.05,
                "load_voltage": 40,
                "bat_charging_i": 1,
                "bat_discharging_i": 30,
                "conditioning_toggle": False,
                "conditioning_period": 30,
                "conditioning_duration": 0.1
            }

        # Initialization of specific thresholds
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]
        self.conditioning_setting = circuit_param["conditioning_toggle"]
        self.conditioning_period = circuit_param["conditioning_period"]
        self.conditioning_duration = circuit_param["conditioning_duration"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)
        self("battery").regulated = True; self("battery").num_eq = 1
        self("battery").soc_start = 1

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self("battery").soc():
            self("battery").charging = False

        self.max_power_fc = self("fc_stack").maximum_power() * self("dcdc_fc").beta
        self.max_charging_power = self("battery").return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self("dcdc_bat_di").beta

        if self.conditioning():
            self.state.val = 0
            self("battery").charging = False
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. diode_bat_dis, 5. dcdc_dis, 6. diode_bat_char,
            #               7. dcdc_charge, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage),
                                        (self("battery").i_in, 0),
                                        (self("battery").u_in, 0)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc*0.95 < self("power_sink").p_mission()+self("compressor").p_electric_motor()\
                and self("battery").soc() > self.bat_min_soc:

            self.state.val = 2
            self("battery").charging = False
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. dcdc_dis,
            #               5. dcdc_charge, 6. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 1, 0, 0]])
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. diode_bat_dis, 5. dcdc_dis, 6. diode_bat_char,
            #               7. dcdc_charge, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 1, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage),
                                        (self("dcdc_bat_di").i_in, self.bat_discharging_i),
                                        (self("battery").u_in, 0),
                                        (self("battery").i_in, 0)]
            #self.initial_conditions = [(self("dcdc_bat_di").u_in, self("battery").return_discharging_voltage(10))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc*0.95 > self("power_sink").p_mission()+self.max_charging_power+\
                self("compressor").p_electric_motor() and \
                self.bat_min_soc < self("battery").soc() < self.bat_max_soc:

            self.state.val = 3
            self("battery").charging = True
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. diode_bat_dis, 5. dcdc_dis, 6. diode_bat_char,
            #               7. dcdc_charge, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage),
                                        (self("dcdc_bat_ch").i_out, self.bat_charging_i),
                                        (self("battery").i_out, 0),
                                        (self("battery").u_out, 0)]
            # self.initial_conditions = [(self.components[5].u_out,
            #                            self.components[1].return_charging_voltage(1))]

            # State 1: SOC very high, load < max power of the fuel cell
        elif (self.max_power_fc > self("power_sink").p_mission()):
                 #and ((self("battery").soc() > self.bat_charge_below_soc and self("battery").charging is False)
                  #    or (self.max_power_fc < self("power_sink").p_mission() + self.max_charging_power)):

            self.state.val = 1
            self("battery").charging = False
            # connectivity: 0. fc, 1. diode_FC, 2. dcdc_fc, 3. bat, 4. diode_bat_dis, 5. dcdc_dis, 6. diode_bat_char,
            #               7. dcdc_charge, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage)]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}; '
                          f'Power Fuel Cell: {self.max_power_fc}; '
                          f'SOC: {self("battery").soc()}; '
                          f'Power needed: {self("power_sink").p_mission()}.')
            raise ValueError()


class CircuitRegulated5(Circuit):
    """
    no conditioning possible FC-DC-Load-DC-Bat-DC-Load Test compressor, contains a calculation of the efficiency
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        Testing of the compressor

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "DCDC_fc": "DCDC_converter.yaml",
                "DCDC_bat_ch": "DCDC_converter.yaml",
                "DCDC_bat_di": "DCDC_converter.yaml",
                "compressor": "Compressor.yaml"
            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "load_voltage": 12,
                "bat_charging_i": 1,
                "bat_discharging_i": 2
            }

        # Initialization of specific thresholds and parameters
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        # circuit states to calculate
        self.total_efficiency = Variable("total_efficiency", "%")  # total efficiency of the system

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self.components[1].soc():
            self.components[1].charging = False

        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta
        self.max_charging_power = self.components[1].return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self.components[5].beta

        # State 1: SOC very high, load < max power of the fuel cell
        if (self.max_power_fc > self.components[2].current_power()) \
                and ((self.components[1].soc() > self.bat_charge_below_soc and self.components[1].charging is False)
                     or (self.max_power_fc < self.components[2].current_power()+self.max_charging_power)):

            self.state.val= 1
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge,
            # 6. compressor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc < self.components[2].p_mission() and self.components[1].soc() > self.bat_min_soc:

            self.state.val= 2
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            # 6. compressor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[4].i_in, self.bat_discharging_i),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
            self.initial_conditions = [(self.components[4].u_in,
                                        self.components[1].return_discharging_voltage(2))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc > self.components[2].p_mission()+self.max_charging_power and \
                self.components[1].soc > self.bat_min_soc:

            self.state.val= 3
            self.components[1].charging = True
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            # 6. compressor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[5].i_out, self.bat_charging_i),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(1))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}')
            raise ValueError()

    def try_other_state(self):
        pass

    def define_circuit_states(self):
        self.circuit_states = [self.state, self.total_efficiency]

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []

    def calculate_post_values(self):
        # EFFICIENCY
        power_out = 0
        power_in = 1
        if self.t_place == 0:
            pass
        else:
            if self.state() == 1:  # only FC
                power_out = self.components[2].p_it()
                power_in = self.components[0].power_h2_equivalent()
            elif self.state() == 2:  # FC plus Battery
                power_out = self.components[2].p_it()
                power_in = self.components[1].power_h2_equivalent() + self.components[0].power_h2_equivalent()
            elif self.state() == 3:
                power_out = self.components[2].p_it() + self.components[1].power_h2_equivalent()
                power_in = self.components[0].power_h2_equivalent()

        self.total_efficiency.val = (power_out / power_in) * 100


class CircuitRegulated6(Circuit):
    """
    circuit regulated with FC-DC-Load-Bat, no dcdc converter between bat and load
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        Testing of the compressor

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "DCDC_fc": "DCDC_converter.yaml"
            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "load_voltage": 12,
                "bat_charging_i": 1,
                "bat_discharging_i": 2
            }

        # Initialization of specific thresholds and parameters
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        # circuit states to calculate
        self.total_efficiency = Variable("total_efficiency", "%")  # total efficiency of the system

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self.components[1].soc():
            self.components[1].charging = False

        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta
        self.max_charging_power = self.components[1].return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self.components[5].beta

        # State 1: SOC very high, load < max power of the fuel cell
        if (self.max_power_fc > self.components[2].current_power()) \
                and ((self.components[1].soc() > self.bat_charge_below_soc and self.components[1].charging is False)
                     or (self.max_power_fc < self.components[2].current_power()+self.max_charging_power)):

            self.state.val = 1
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc
            self.connectivity = np.array([[0, 0, 0, 0],
                                          [0, 0, 0, 0],
                                          [0, 0, 0, 1],
                                          [1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc < self.components[2].p_mission() and self.components[1].soc() > self.bat_min_soc:

            self.state.val = 2
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc
            self.connectivity = np.array([[0, 0, 0, 0],
                                          [0, 0, 0, 0],
                                          [0, 1, 0, 1],
                                          [1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc > self.components[2].p_mission()+self.max_charging_power and \
                self.components[1].soc > self.bat_min_soc:

            self.state.val = 3
            self.components[1].charging = True
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc
            self.connectivity = np.array([[0, 0, 0, 0],
                                          [0, 0, 0, 1],
                                          [0, 0, 0, 1],
                                          [1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(1))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}')
            raise ValueError()

    def try_other_state(self):
        pass

    def define_circuit_states(self):
        self.circuit_states = [self.state, self.total_efficiency]

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []

    def calculate_post_values(self):
        # EFFICIENCY
        power_out = 0
        power_in = 1
        if self.t_place == 0:
            pass
        else:
            if self.state() == 1:  # only FC
                power_out = self.components[2].p_it()
                power_in = self.components[0].power_h2_equivalent()
            elif self.state() == 2:  # FC plus Battery
                power_out = self.components[2].p_it()
                power_in = self.components[1].power_h2_equivalent() + self.components[0].power_h2_equivalent()
            elif self.state() == 3:
                power_out = self.components[2].p_it() + self.components[1].power_h2_equivalent()
                power_in = self.components[0].power_h2_equivalent()

        self.total_efficiency.val = (power_out / power_in) * 100


class CircuitRegulated7Area(Circuit):
    """
    Regulated circuit for the area drone, FC and Battery with conditioning and compressor
    """

    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        CircuitRegulated1 with conditioning option

        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """

        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "AREA_Fuel_Cell.yaml",
                "Battery": "AREA_Battery.yaml",
                "Power_Sink": "AREA_Power_Sink.yaml",
                "DCDC_fc": "AREA_DCDC_converter.yaml",
                "DCDC_bat_ch": "AREA_DCDC_converter.yaml",
                "DCDC_bat_di": "AREA_DCDC_converter.yaml",
                "compressor": "AREA_Compressor.yaml"
            }
        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "load_voltage": 12,
                "bat_charging_i": 1,
                "bat_discharging_i": 2,
                "conditioning_toggle": False
            }

        # Initialization of specific thresholds
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]
        self.conditioning_setting = circuit_param["conditioning_toggle"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self.components[1].soc():
            self.components[1].charging = False

        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta
        self.max_charging_power = self.components[1].return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self.components[5].beta

        if self.conditioning():
            self.state.val = 0
            self.components[1].charging = False
            # connectivity: 0. FC, 1. Bat, 2. Power_Sink, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge, 6. compressor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0, 0]])
            self.boundary_conditions = [(self.components[4].u_out, self.load_voltage),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
        # State 1: SOC very high, load < max power of the fuel cell
        elif (self.max_power_fc > self.components[2].current_power()) \
                and ((self.components[1].soc() > self.bat_charge_below_soc and self.components[1].charging is False)
                     or (self.max_power_fc < self.components[2].current_power()+self.max_charging_power)):

            self.state.val = 1
            self.components[1].charging = False
            # connectivity: 0. FC, 1. Bat, 2. Power_Sink, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge, 6. compressor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[3].u_out, self.load_voltage)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc < self.components[2].p_mission() and self.components[1].soc > self.bat_min_soc:

            self.state.val = 2
            self.components[1].charging = False
            # connectivity: 0. FC, 1. Bat, 2. Power_Sink, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge, 6. compressor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[4].i_in, self.bat_discharging_i),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
            self.initial_conditions = [(self.components[4].u_in,
                                        self.components[1].return_discharging_voltage(2))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc > self.components[2].p_mission()+self.max_charging_power and \
                self.components[1].soc() > self.bat_min_soc:

            self.state.val = 3
            self.components[1].charging = True
            # connectivity: 0. FC, 1. Bat, 2. Power_Sink, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge, 6. compressor
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[5].i_out, self.bat_charging_i),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(1))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}.')
            raise ValueError()

    def try_other_state(self):
        pass

    def define_circuit_states(self):
        self.circuit_states = [self.state]

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = [self.conditioning]


class CircuitRegulated8(Circuit):
    """
    same as regulated circuit one but with controlled power sharing
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "DCDC_fc": "DCDC_converter.yaml",
                "DCDC_bat_ch": "DCDC_converter.yaml",
                "DCDC_bat_di": "DCDC_converter.yaml"
            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "load_voltage": 12,
                "bat_charging_i": 1,
                "bat_discharging_i": 2
            }

        # Initialization of specific thresholds and parameters
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= self.components[1].soc():
            self.components[1].charging = False

        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta
        self.max_charging_power = self.components[1].return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self.components[5].beta

        # State 1: SOC very high, load < max power of the fuel cell
        if (self.max_power_fc > self.components[2].current_power()) \
                and ((self.components[1].soc() > self.bat_charge_below_soc and self.components[1].charging is False)
                     or (self.max_power_fc < self.components[2].current_power()+self.max_charging_power)):

            self.state.val = 1
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.max_power_fc < self.components[2].p_mission() and self.components[1].soc() > self.bat_min_soc:

            self.state.val = 2
            self.components[1].charging = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. bat_charge, 5. dcdc_batdis
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 1],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0),
                                        (self.components[3].i_out, 4, self.components[5].i_out)]
            self.initial_conditions = [(self.components[5].u_in,
                                        self.components[1].return_discharging_voltage(2))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.max_power_fc > self.components[2].p_mission()+self.max_charging_power and \
                self.components[1].soc > self.bat_min_soc:

            self.state.val = 3
            self.components[1].charging = True
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0]])
            self.boundary_conditions = [(self.components[2].u_in, self.load_voltage),
                                        (self.components[5].i_out, self.bat_charging_i),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(1))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}.')
            raise ValueError()

    def try_other_state(self):
        pass

    def define_circuit_states(self):
        self.circuit_states = [self.state]

    def define_circuit_operating_conditions(self):
        self.circuit_op_conditions = []


class CircuitRegulated9NG19(Circuit):
    """
    basic regulated circuit, constant power output of the fuel cell
    no conditioning possible FC-DC-Load-DC-Bat-DC-Load
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "dcdc_fc": "DCDC_converter_unreg_ng_2018.yaml",
                "diode_fc": "Diode_regulated_comparison.yaml",
                "battery": "NG19_Battery_Shepherd_regulated_3_6C.yaml",
                "dcdc_bat_di": "DCDC_converter_unreg_ng_2018.yaml",
                "diode_bat_di": "Diode_regulated_comparison.yaml",
                "dcdc_bat_ch": "DCDC_converter_unreg_ng_2018.yaml",
                "diode_bat_ch": "Diode_regulated_comparison.yaml",
                "power_sink": "Power_Sink.yaml",

            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.98,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "fuel_cell_current": 3.2,
                "load_voltage": 13.77,
                "bat_charging_i": 1,
                "bat_discharging_i": 2,
                "maximum_fuel_cell_power": 50
            }

        # Initialization of specific thresholds and parameters
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.fuel_cell_current = circuit_param["fuel_cell_current"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]
        self.set_max_fc_power = circuit_param["maximum_fuel_cell_power"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)
        self("battery").regulated = True
        self("battery").num_eq = 1
        self("battery").soc_start = 1

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= np.floor(self("battery").soc() * 3) / 3:
            self("battery").charging = False
            self("battery").idle = True

        self.max_power_fc = self("fc_stack").find_limit_values() * self("dcdc_fc").beta
        self.max_charging_power = self("battery").return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self("dcdc_bat_ch").beta

        if self("power_sink").p_mission() == 0:
            if np.floor(self("battery").soc() * 3) / 3 < self.bat_charge_below_soc:
                self.state.val = 4
                self("battery").charging = True
                self("battery").idle = False
                # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
                self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                              [0, 0, 0, 0, 0, 1],
                                              [0, 0, 0, 0, 0, 0],
                                              [1, 0, 0, 0, 0, 0],
                                              [0, 0, 0, 0, 0, 0],
                                              [0, 0, 0, 1, 0, 0]])
                # connectivity: 0. fc, 1. dcdc_fc, 2. diode_FC, 3. bat, 4. dcdc_dis, 5. diode_bat_dis, 6. dcdc_charge,
                #               7. diode_bat_char, 8. power_sink
                self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                              [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                              [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                              [0, 0, 0, 0, 0, 0, 1, 0, 0],
                                              [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                              [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                              [0, 0, 0, 0, 0, 0, 0, 1, 0],
                                              [0, 0, 1, 0, 0, 0, 0, 0, 0],
                                              [0, 0, 0, 0, 0, 0, 0, 0, 0]])
                self.boundary_conditions = [(self("dcdc_fc").i_out, self.fuel_cell_current),
                                            (self("dcdc_fc").u_out, self.load_voltage),
                                            (self("battery").i_out, 0),
                                            (self("battery").u_out, 0)]
                self.initial_conditions = [(self("dcdc_bat_ch").u_out,
                                            self("battery").return_charging_voltage(5))]

        # State 1: SOC very high, load < max power of the fuel cell
        elif (self("power_sink").p_mission() < self.set_max_fc_power) \
                and ((self("battery").soc() >= self.bat_charge_below_soc and self("battery").charging is False)
                     or (self.set_max_fc_power < self("power_sink").p_mission()+self.max_charging_power)):

            self.state.val = 1
            self("battery").charging = False
            self("battery").idle = True
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            # connectivity: 0. fc, 1. dcdc_fc, 2. diode_FC, 3. bat, 4. dcdc_dis, 5. diode_bat_dis, 6. dcdc_charge,
            #               7. diode_bat_char, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").u_out, self.load_voltage)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif self.set_max_fc_power <= self("power_sink").p_mission() and self("battery").soc() > self.bat_min_soc:

            self.state.val = 2
            self("battery").charging = False
            self("battery").idle = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            # connectivity: 0. fc, 1. dcdc_fc, 2. diode_FC, 3. bat, 4. dcdc_dis, 5. diode_bat_dis, 6. dcdc_charge,
            #               7. diode_bat_char, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 1, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").i_out, self.fuel_cell_current),
                                        (self("dcdc_fc").u_out, self.load_voltage),
                                        (self("battery").i_in, 0),
                                        (self("battery").u_in, 0)]
            self.initial_conditions = [(self("dcdc_bat_di").u_in,
                                        self("battery").return_discharging_voltage(5))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif self.set_max_fc_power > self("power_sink").p_mission()+self.max_charging_power and \
                self("battery").soc() > self.bat_min_soc:

            self.state.val = 3
            self("battery").charging = True
            self("battery").idle = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0]])
            # connectivity: 0. fc, 1. dcdc_fc, 2. diode_FC, 3. bat, 4. dcdc_dis, 5. diode_bat_dis, 6. dcdc_charge,
            #               7. diode_bat_char, 8. power_sink
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 1, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0, 0, 1, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0],
                                          [0, 0, 1, 0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self("dcdc_fc").i_out, self.fuel_cell_current),
                                        (self("dcdc_fc").u_out, self.load_voltage),
                                        (self("battery").i_out, 0),
                                        (self("battery").u_out, 0)]
            self.initial_conditions = [(self("dcdc_bat_ch").u_out,
                                        self("battery").return_charging_voltage(5))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}.')


class CircuitRegulatedTest(Circuit):
    """
    basic regulated circuit, constant power output of the fuel cell
    no conditioning possible FC-DC-Load-DC-Bat-DC-Load
    """
    def __init__(self, circuit_type: str, component_info=None, circuit_param=None):
        """
        :param circuit_type: name of the circuit (visible in plots and output files)
        :param component_info: dictionary containing name: filename.yaml pairs for every needed object of the circuit
        :param circuit_param: dictionary of the circuit parameters
        """
        # default parameters
        if component_info is None:
            component_info = {
                "fc_stack": "NG19_Fuel_Cell.yaml",
                "Battery": "Battery_Shepherd_unregulated.yaml",
                "Power_Sink": "Power_Sink.yaml",
                "DCDC_fc": "DCDC_converter.yaml",
                "DCDC_bat_di": "DCDC_converter.yaml",
                "DCDC_bat_ch": "DCDC_converter.yaml"
            }

        if circuit_param is None:
            circuit_param = {
                "bat_charge_below_soc": 0.9,
                "bat_max_soc": 0.99,
                "bat_min_soc": 0.05,
                "fuel_cell_current": 5,
                "load_voltage": 12,
                "bat_charging_i": 1,
                "bat_discharging_i": 2,
                "maximum_fuel_cell_power": 50
            }

        # Initialization of specific thresholds and parameters
        self.bat_charge_below_soc = circuit_param["bat_charge_below_soc"]
        self.bat_max_soc = circuit_param["bat_max_soc"]
        self.bat_min_soc = circuit_param["bat_min_soc"]
        self.load_voltage = circuit_param["load_voltage"]
        self.fuel_cell_current = circuit_param["fuel_cell_current"]
        self.bat_charging_i = circuit_param["bat_charging_i"]
        self.bat_discharging_i = circuit_param["bat_discharging_i"]
        self.set_max_fc_power = circuit_param["maximum_fuel_cell_power"]

        # Maximum Fuel Cell + DCDC fuel Cell Power
        self.max_power_fc = 0
        self.max_charging_power = 0

        super().__init__(component_info, circuit_type)

    def create_circuit_equations2(self):
        self.state.val = 0
        self.connectivity = np.array([[0, 0],
                                      [1, 0]])
        self.boundary_conditions = [(self.components[0].u_in, 0), (self.components[0].i_in, 0)]

    def create_circuit_equations(self):
        # Make sure the battery is not charged to much
        if self.bat_max_soc <= np.floor(self.components[1].soc() * 3) / 3:
            self.components[1].charging = False
            self.components[1].idle = True

        self.max_power_fc = self.components[0].find_limit_values() * self.components[3].beta
        self.max_charging_power = self.components[1].return_charging_voltage(self.bat_charging_i) \
                                  * self.bat_charging_i / self.components[5].beta

        if self.components[2].p_mission() == 0 \
                and np.floor(self.components[1].soc() * 3) / 3 < self.bat_charge_below_soc:
            self.state.val = 4
            self.components[1].charging = True
            self.components[1].idle = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1],
                                          [0, 0, 0, 0, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0]])
            self.boundary_conditions = [(self.components[3].i_out, self.fuel_cell_current),
                                        (self.components[3].u_out, self.load_voltage),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(5))]
        elif self.components[2].p_mission() == 0:
            pass
        # State 1: SOC very high, load < max power of the fuel cell
        elif (self.components[2].current_power() < self.set_max_fc_power) \
                and ((np.floor(self.components[1].soc() * 3) / 3 >= self.bat_charge_below_soc and self.components[1].charging is False)
                     or (self.max_power_fc < self.components[2].current_power()+self.max_charging_power)
                     or self.components[1].idle is True):

            self.state.val = 1
            self.components[1].charging = False
            self.components[1].idle = True
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[3].u_out, self.load_voltage)]

        # State 2 / 4: SOC greater than 0, load greater than max power of the fuel cell stack
        elif 50 < self.components[2].p_mission() and np.floor(self.components[1].soc() * 3) / 3 > self.bat_min_soc:

            self.state.val = 2
            self.components[1].charging = False
            self.components[1].idle = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 1, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 1, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0]])
            self.boundary_conditions = [(self.components[3].i_out, self.fuel_cell_current),
                                        (self.components[3].u_out, self.load_voltage),
                                        (self.components[1].i_in, 0),
                                        (self.components[1].u_in, 0)]
            self.initial_conditions = [(self.components[4].u_in,
                                        self.components[1].return_discharging_voltage(5))]

        # State 3 / 5: SOC near 0, load smaller than max power of the fuel cell stack
        elif 50 > self.components[2].p_mission()+self.max_charging_power and \
                np.floor(self.components[1].soc() * 3) / 3 > self.bat_min_soc:

            self.state.val = 3
            self.components[1].charging = True
            self.components[1].idle = False
            # note for the connectivity matrix: 0. FC, 1. Bat, 2. Load, 3. dcdc_fc, 4. dcdc_batdis, 5. bat_charge
            self.connectivity = np.array([[0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 1],
                                          [0, 0, 0, 1, 0, 0],
                                          [1, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 0, 0, 0],
                                          [0, 0, 0, 1, 0, 0]])
            self.boundary_conditions = [(self.components[3].i_out, self.fuel_cell_current),
                                        (self.components[3].u_out, self.load_voltage),
                                        (self.components[1].i_out, 0),
                                        (self.components[1].u_out, 0)]
            self.initial_conditions = [(self.components[5].u_out,
                                        self.components[1].return_charging_voltage(5))]

        else:
            logging.error(f'Power of Fuel Cell not sufficient and SOC too low at step: {self.t_place}')
            raise ValueError()

