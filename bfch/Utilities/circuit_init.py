"""
Created on 14.01.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 1.00

File Description:

"""
from ..Circuits.circuit_reg_test import *
from ..Circuits.circuit_unreg_test import *
from ..Circuits.circuit_reg_examples import *
from ..Circuits.circuit_unreg_examples import *
from ..Circuits.circuit_fc import *
from ..Circuits.circuit_fc_dcdc import *


class CreateCircuit:
    """
    callable class to make the creation of circuit objects easier
    """
    circuits_list = {
        # TESTS
        "regulated1": CircuitRegulated1,
        "regulated2": CircuitRegulated2,
        "regulated3": CircuitRegulated3,
        "regulated4": CircuitRegulated4,
        "regulated4_compressor": CircuitRegulated4Compressor,
        "regulated5": CircuitRegulated5,
        "regulated6": CircuitRegulated6,
        "area": CircuitRegulated7Area,
        "regulated8": CircuitRegulated8,
        "regulated9": CircuitRegulated9NG19,
        "unregulated1": CircuitUnregulated1,
        "unregulated2": CircuitUnregulated2,
        "unregulated3": CircuitUnregulated3,
        "unreg_diode": CircuitUnregulated_Diode_test,
        "fc": CircuitFuelCell,
        "fc_dc": CircuitFuelCellDcDc,
        "fc_dc_bleed_air": CircuitFuelCellDcDcBleedAirHydraulic,
        "regulated_test": CircuitRegulatedTest,
        "unregulated_diode": CircuitUnregulated_H3_Diode,
        "unregulated_diode_compressor": CircuitUnregulated_H3_Diode_Compressor,
        "unregulated_NG": CircuitUnregulated_NG,

        # EXAMPLES
        "CircuitRegulatedUncontrolledPowerSharing": CircuitRegulatedUncontrolledPowerSharing,
        "CircuitUnregulatedDiodeChargeResistor": CircuitUnregulatedDiodeChargeResistor
    }

    def __call__(self, circuit_param=None, circuit_type="regulated1", component_info=None, circuits_list=None):
        """
        :param circuit_param: default is None, contains a dictionary with all the necessary data for the circuit
        :param circuit_type: default is "regulated1", name of the circuit
        :param component_info: default is None, information about the components needed for the specific circuit, dict
        with name: filename.yaml pairs
        :param circuits_list: default is None, list of available circuits, make sure they are all imported in this file

        :return: an Object of the desired circuit
        """
        if circuits_list is None:
            pass
        else:
            self.circuits_list = circuits_list

        try:
            return self.circuits_list[circuit_type](circuit_type, component_info, circuit_param)
        except KeyError:
            raise KeyError(f'No Calculation possible in hy2sim! \n'
                           f'Please select one of the following circuit types: {self.circuits_list.keys()}')
