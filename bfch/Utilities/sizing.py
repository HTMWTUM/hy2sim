"""
Created on 01.04.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
import yaml
import pandas as pd
import numpy as np
import math
from matplotlib import pyplot as plt
from datetime import datetime
from bfch import FcModel
from bfch import Input

raw_fc_data = open("../../Data/Example_FuelCellStacks/FC_generic_singlecell.yaml")
fc_data = yaml.load(raw_fc_data, Loader=yaml.FullLoader)

fc = FcModel('FC', fc_data)

def get_iv_fc(fc):
    # Input
    input_file = Input()
    input_file.conditions = ["time s", "anode_h2_molfrac 1", "anode_p Pa", "cath_o2_molfrac 1", "cath_p Pa", "fc_temp K"]
    input_file.segment = [[0], [1], [80000], [0.21], [101325], [338]]
    fc.new_segment(input_file)
    fc.calculate_segment_pre_values()
    
    # test polarization curve
    i_range = np.linspace(0, 0.5, 1000)
    fc_model_vectorized = np.vectorize(fc.basic_act_var_t)
    eff_vectorized = np.vectorize(fc.calculate_efficiency)
    u_result = fc_model_vectorized(i_range, fc.alpha_A, fc.alpha_C, fc.i_0A, fc.i_0C, fc.ASR_ohm, fc.i_L,
                                   fc.i_leak, fc.C_bat_As)
    # eff_result=eff_vectorized(np.multiply(u_result, fc.series_cells))
    # p_result=np.multiply(np.multiply(u_result, 1),np.multiply(i_range, 1))
    p_result=np.multiply(np.multiply(u_result, fc.series_cells),np.multiply(i_range, fc.cell_area))
    
    df = pd.DataFrame()
    df['i'] = i_range*fc.cell_area
    df['i_dens'] = i_range
    df['u'] = u_result*fc.series_cells
    df['u_cell'] = u_result
    df['eff'] = [1]*len(df)#eff_result
    df['p'] = p_result
    return df

def plot_iv_fc(df, fc, absolute=True, i_rated=None):
    fig, ax1 = plt.subplots()
    ax1.plot(df.i_dens, df.u_cell, color='black', label='Cell Voltage')
    ax1.plot(df.i_dens, df.eff, color='black', linestyle='dashed', label='Efficiency')
    ax1.set_xlabel('I [A]')
    ax1.set_ylabel('U [V] / Eff. []')
    plt.title('normalized')
    ax1.legend()
    plt.show()
    
    if absolute:
        fig, ax1 = plt.subplots()
        ax1.plot(df.i, df.u, color='black', label='Voltage')
        ax1.set_xlabel('I [A]')
        ax1.set_ylabel('U [V]')
        ax2 = ax1.twinx()
        ax2.set_ylabel('P [W]')
        ax2.plot(df.i, df.p, color='black', linestyle='dashed', label='Power')
        plt.title('absolute')
        if i_rated:
            ax1.vlines(i_rated, min(df.u), max(df.u), linestyle='dotted')
        ax1.legend(loc='upper left')
        ax2.legend(loc='upper right')
        plt.show()
        
fc.power_rated=4800
fc.u_prated=70
frated=0.8

def size_fc(fc, power_rated=None, u_prated=None, frated=0.8):
    df=get_iv_fc(fc)
    
    if not fc.power_rated and power_rated:
        fc.power_rated=power_rated
    elif not fc.power_rated and not power_rated:
        raise ValueError("no rated power available")
        
    if not fc.u_prated and u_prated:
        fc.u_prated=u_prated
    elif not fc.u_prated and not u_prated:
        raise ValueError("no rated voltage available")
        
    index_rated=min(range(len(df.p)), key=lambda i: abs(df.p[i]-frated*max(df.p)))
    u_cell_rated=df.u_cell[index_rated]
    fc.series_cells=math.ceil(fc.u_prated/u_cell_rated)
    i_dens_rated=df.i_dens[index_rated]
    
    p_dens_rated=u_cell_rated*fc.series_cells*i_dens_rated
    fc.cell_area=fc.power_rated/p_dens_rated
    
size_fc(fc, frated=0.8)
print(fc.series_cells, fc.cell_area)
df_sized=get_iv_fc(fc)
plot_iv_fc(df_sized, fc, absolute=True)

# now = datetime.now()
# currentdate = now.strftime("%Y%m%d_%H%M%S")
# df.to_csv(f'../../plots/FC_{currentdate}.csv', sep=',', decimal='.')
