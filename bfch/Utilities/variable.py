"""
Created on 2/3/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:
    
"""
import numpy as np


class Variable:
    """
    container for values of unknowns, states, conditions
    """

    t_place = 0
    segment = 0
    reset = True

    def __init__(self, name: str, unit: str, val_max=np.inf, val_min=0.):
        """
        :param name: name of the variable
        :param unit: unit of the variable
        :param val_max: maximum allowed value
        :param val_min: minimum required value
        """
        self._val = []
        self.val_max = val_max
        self.val_min = val_min
        self.name = name.replace(" ", "")
        self.unit = unit.replace(" ", "")
        self.overshootmax = 0
        self.overshootmin = 0

    def __call__(self):
        return self.val

    def new_segment(self, added_values):
        """
        appends values of the new segment to the existing ones

        :param added_values: list of either zeros or given values
        """
        self._val = np.append(self._val, added_values)
        self.segment+=1

    def overwrite(self, values):
        """
        be careful, this function overwrites all the values of the variable

        :param values: values of the complete sequence
        """
        cutoff = len(values)
        self._val[-cutoff:] = values

    def overwrite_constant(self, value):
        """
        be careful, this function overwrite all the values of the variable

        :param value: constant value that will replace all the values
        """
        self._val[self.t_place:] = [value] * len(self._val[self.t_place:])

    def return_val(self, index=0):
        """
        returns all the value of the variable from index onwards

        :param index: place from which the values are returned

        :return: a list of values
        """
        return self._val[index:]

    def next_val(self, value):
        """
        sets the next value to value

        :param value: the next value
        """
        if isinstance(value, bool):
            self._val[self.t_place + 1] = value
        elif isinstance(value, (int, float)):
            self._val[self.t_place + 1] = min(max(value, self.val_min), self.val_max)

    def return_last_val(self):
        """
        :return: the last value of the currently stored values
        """
        return self._val[-1]

    @property
    def val(self):
        return self._val[self.t_place]

    @property
    def prev_val(self):
        if self.t_place == 0:
            return 0
        else:
            return self._val[self.t_place-1]

    @prev_val.setter
    def prev_val(self, value):
        if isinstance(value, bool):
            self._val[self.t_place-1] = value
        elif isinstance(value, (int, float)):
            self._val[self.t_place-1] = np.min([np.max([value, self.val_min]), self.val_max])

    @val.setter
    def val(self, value):
        if isinstance(value, bool):
            self._val[self.t_place] = value
        elif isinstance(value, (int, float)):
            if value > self.val_max:
                self.overshootmax += 1
                if self.overshootmax > 5 and self.reset:
                    value = np.min([self.val_max*0.5, 10000])
                else:
                    value = self.val_max
                self._val[self.t_place] = value
            elif value < self.val_min:
                self.overshootmin += 1
                if self.overshootmin > 5 and self.reset:
                    value = np.max([np.min([self.val_max*0.1, 10000]), self.val_min])
                else:
                    value = self.val_min
                self._val[self.t_place] = value
            else:
                self.overshootmax = 0
                self.overshootmin = 0
                self._val[self.t_place] = value

