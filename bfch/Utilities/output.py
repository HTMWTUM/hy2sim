"""
Created on 1/11/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Description:
    
"""
from ..Circuits.circuit import Circuit
import pandas as pd


class Output:
    """
    container for all the results and an interface to other code
    """
    output_circuit = []
    last_index = []

    def __init__(self):
        """
        This class is here to allow for an interface to other parts of code. right now it only stores the result of the
        current segment into a pandas dataframe
        """
        pass

    def fill_output(self, circuit: Circuit):
        """
        add the results of the current segment to the existing results in output_circuit

        :param circuit: the solved circuit
        """
        if len(self.output_circuit) == 0:
            self.last_index = 0
        else:
            self.last_index = self.last_index + self.output_circuit.index[-1] + 1

        # get results of the segment from the circuit
        self.output_circuit = circuit.pass_output(self.last_index)

        for component in circuit.components:
            self.output_circuit = pd.concat([self.output_circuit, component.pass_output(self.last_index)], axis=1)
