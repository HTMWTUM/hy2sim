"""
Created on 1/11/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Description:
    an object of the INPUT class contains the information about one time step for the calculation.
    that includes the states at input and output of a mission segment. Therefore the model can ether represent
    constant or linear segments.
"""
import numpy as np
import bfch.Utilities.data_handling as data
import pandas as pd
import logging


class Input:
    """
    container for all the input data of a mission segment
    has the necessary functions to make some changes to this data
    """

    def __init__(self):
        # Data that describes the start and den values of the current mission segment
        self.prev_segment_end = None
        self.conditions = None

        # Data that describes the boundary conditions of the circuit for every time step within one segment.
        self.conditioning = [False]

        self.segment = []

        # end of previous segment
        self.prev_segment_conditioning_end = False
        self.start_value_change = True

# -------------------------------------------------------------------------------------------------------------------- #
    def save_prev_segment_end(self):
        """
        saves the values of the conditions at the end of the previous segment. This is needed to insert the appropriate
        time steps in case conditioning is on
        """
        self.prev_segment_end = []
        for i in self.segment:
            self.prev_segment_end.append(i[-1])

    def input_to_boundary1(self):
        """
        prepare input in case the segment duration is smaller than the maximum allowed step size
        """
        if self.start_value_change:
            self.conditioning = [False, False]
        else:
            for i in self.segment:
                del i[0]
            self.conditioning = [False]
        self.segment.append(self.conditioning)
        self.conditions.append("conditioning 1")
        logging.info(f"Start value change: {self.start_value_change}"
              f"; Input to Boundary 1: DONE")
        #print(f"Start value change: {self.start_value_change}"
         #     f"; Input to Boundary 1: DONE\n")

    def input_to_boundary2(self, max_step_size, duration, min_num_steps):
        """
        prepare input in case the segment duration is greater than the maximum allowed step size

        :param max_step_size: maximum allowed step size, s
        :param duration: duration of the segment, s
        """
        # create time series corresponding to the input parameters
        if self.start_value_change:
            self.segment[0] = data.create_time_steps(1, [], max_step_size, duration, self.segment[0][0], min_num_steps)
            for i in range(1, len(self.segment)):
                self.segment[i] = data.convert_import_curve(self.segment[0], self.segment[0][1] - self.segment[0][0],
                                                            self.segment[i], np.array([self.segment[0][0],
                                                                                       self.segment[0][-1]]))
            self.conditioning = [False] * len(self.segment[0])
        else:
            self.segment[0] = data.create_time_steps(1, [], max_step_size, duration, self.segment[0][0],
                                                     min_num_steps)[1:]
            for i in range(1, len(self.segment)):
                self.segment[i] = data.convert_import_curve(self.segment[0], self.segment[0][1] - self.segment[0][0],
                                                            self.segment[i], np.array([self.segment[0][0],
                                                                                       self.segment[0][-1]]))
            self.conditioning = [False] * len(self.segment[0])
        self.segment.append(self.conditioning)
        self.conditions.append("conditioning 1")
        logging.info(f"Start value change: {self.start_value_change}"
              f"; Input to Boundary 2: DONE")
        #print(f"Start value change: {self.start_value_change}"
            #  f"; Input to Boundary 2: DONE\n")

    def insert_conditioning_cycles(self, period, duration):
        """
        inserts conditioning cycles to the dataframe
        has to be the last function to alter the input parameters of the segment before being send to the circuit

        :param period: time between to start of conditioning/conditioning cycles
        :param duration: duration of the conditioning
        """
        i = 1
        if len(self.prev_segment_end) > 0:
            if self.segment[0][0] != self.prev_segment_end[0]:
                for j in range(len(self.segment)-1):
                    self.segment[j] = np.insert(self.segment[j], 0, self.prev_segment_end[j])
                self.conditioning.insert(0, self.prev_segment_end[-1])

            i = 1

            if self.prev_segment_end[-1]:
                delta_t_alt = self.segment[0][1] - self.segment[0][0]
                self.segment[0] = np.insert(self.segment[0], 1, self.segment[0][0])
                delta_t_neu = self.segment[0][1] - self.segment[0][0]
                for j in range(1, len(self.segment)-1):
                    self.segment[j] = self.insert_conditioning_variable_start(self.segment[j], delta_t_neu, delta_t_alt)

                self.conditioning.insert(1, False)
                i = 2

        current_conditioning_cycle = period + np.floor(self.segment[0][0] / period) * period

        while current_conditioning_cycle <= self.segment[0][-1]:
            if current_conditioning_cycle == self.segment[0][i]:
                if current_conditioning_cycle == self.segment[0][-1]:
                    delta_t_alt_1 = self.segment[0][i] - self.segment[0][i - 1]
                    self.segment[0] = np.insert(self.segment[0], i, current_conditioning_cycle - duration)
                    delta_t_neu_1 = self.segment[0][i] - self.segment[0][i - 1]
                    self.segment[0] = np.insert(self.segment[0], i, current_conditioning_cycle - duration)
                    delta_t_neu_2 = self.segment[0][i] - self.segment[0][i - 1]

                    for j in range(1, len(self.segment)-1):
                        self.segment[j] = self.insert_conditioning_variable_end(self.segment[j], i, delta_t_neu_1,
                                                                                delta_t_neu_2, delta_t_alt_1)

                    self.conditioning[i] = True
                    self.conditioning.insert(i, True)
                    self.conditioning.insert(i, False)

                    break

                else:
                    delta_t_alt_0 = self.segment[0][i + 1] - self.segment[0][i]
                    delta_t_alt_1 = self.segment[0][i] - self.segment[0][i - 1]
                    self.segment[0] = np.insert(self.segment[0], i + 1, current_conditioning_cycle)
                    delta_t_neu_0 = self.segment[0][i + 1] - self.segment[0][i]
                    self.segment[0] = np.insert(self.segment[0], i, current_conditioning_cycle - duration)
                    delta_t_neu_1 = self.segment[0][i] - self.segment[0][i - 1]
                    self.segment[0] = np.insert(self.segment[0], i, current_conditioning_cycle - duration)
                    delta_t_neu_2 = self.segment[0][i] - self.segment[0][i - 1]

                    for j in range(1, len(self.segment)-1):
                        self.segment[j] = self.insert_conditioning_variable_1(self.segment[j], i, delta_t_neu_0,
                                                                              delta_t_neu_1, delta_t_neu_2, delta_t_alt_0,
                                                                              delta_t_alt_1)

                    self.conditioning.insert(i + 1, False)
                    self.conditioning[i] = True
                    self.conditioning.insert(i, True)
                    self.conditioning.insert(i, False)

                    current_conditioning_cycle += period
                    i += 4

            elif current_conditioning_cycle < self.segment[0][i]:
                delta_t_alt = self.segment[0][i] - self.segment[0][i - 1]
                self.segment[0] = np.insert(self.segment[0], i, current_conditioning_cycle)
                delta_t_neu_0 = self.segment[0][i] - self.segment[0][i - 1]
                self.segment[0] = np.insert(self.segment[0], i, current_conditioning_cycle)
                delta_t_neu_1 = self.segment[0][i] - self.segment[0][i - 1]
                self.segment[0] = np.insert(self.segment[0], i, current_conditioning_cycle - duration)
                delta_t_neu_2 = self.segment[0][i] - self.segment[0][i - 1]
                self.segment[0] = np.insert(self.segment[0], i, current_conditioning_cycle - duration)
                delta_t_neu_3 = self.segment[0][i] - self.segment[0][i - 1]

                for j in range(1, len(self.segment)-1):
                    self.segment[j] = self.insert_conditioning_variable_2(self.segment[j], i, delta_t_neu_0, delta_t_neu_1,
                                                                          delta_t_neu_2, delta_t_neu_3, delta_t_alt)

                self.conditioning.insert(i, False)
                self.conditioning.insert(i, True)
                self.conditioning.insert(i, True)
                self.conditioning.insert(i, False)

                current_conditioning_cycle += period
                i += 4
            elif current_conditioning_cycle > self.segment[0][i]:
                i += 1
        # if self.segment[0][0] != 0:
        #     for j in range(len(self.segment)):
        #         self.segment[j] = np.delete(self.segment[j], 0, 0)
        #     del self.conditioning[0]
        self.segment[-1] = self.conditioning
        logging.info("Insertion of conditioning cycles done!")

    def insert_conditioning_variable_start(self, var, delta_t_neu, delta_t_alt):
        """
        insert conditioning 'end' in case the previous segment ended on a conditioning cycle

        :param var: variable to be adjusted

        :return: adjusted variable
        """
        delta_var = var[1] - var[0]
        var = np.insert(var, 1, var[0] + (delta_var * delta_t_neu) / delta_t_alt)
        return var

    def insert_conditioning_variable_end(self, var, i, delta_t_neu_1, delta_t_neu_2, delta_t_alt_1):
        """
        insert a conditioning cycle at the end

        :param var: variable to be adjusted
        :param i: current place of time step

        :return: adjusted variable
        """
        delta_var_1 = var[i] - var[i - 1]
        var = np.insert(var, i, var[i - 1] + (delta_var_1 * delta_t_neu_1) / delta_t_alt_1)
        var = np.insert(var, i, var[i - 1] + (delta_var_1 * delta_t_neu_2) / delta_t_alt_1)
        return var

    def insert_conditioning_variable_1(self, var, i, delta_t_neu_0, delta_t_neu_1, delta_t_neu_2, delta_t_alt_0,
                                       delta_t_alt_1):
        """
        insert a conditioning cycle when the conditioning cycle coincides with a time step

        :param var: variable to be adjusted
        :param i: current place of time step

        :return: adjusted variable
        """
        delta_var_0 = var[i + 1] - var[i]
        delta_var_1 = var[i] - var[i - 1]
        var = np.insert(var, i + 1, var[i] + (delta_var_0 * delta_t_neu_0) / delta_t_alt_0)
        var = np.insert(var, i, var[i - 1] + (delta_var_1 * delta_t_neu_1) / delta_t_alt_1)
        var = np.insert(var, i, var[i - 1] + (delta_var_1 * delta_t_neu_2) / delta_t_alt_1)
        return var

    def insert_conditioning_variable_2(self, var, i, delta_t_neu_0, delta_t_neu_1, delta_t_neu_2, delta_t_neu_3,
                                       delta_t_alt):
        """
        insert a conditioning cycle when the conditioning cycle occurs between two time steps

        :param var: variable to be adjusted
        :param i: current place of time step

        :return: adjusted variable
        """
        delta_var = var[i] - var[i - 1]
        var = np.insert(var, i, var[i - 1] + (delta_var * delta_t_neu_0) / delta_t_alt)
        var = np.insert(var, i, var[i - 1] + (delta_var * delta_t_neu_1) / delta_t_alt)
        var = np.insert(var, i, var[i - 1] + (delta_var * delta_t_neu_2) / delta_t_alt)
        var = np.insert(var, i, var[i - 1] + (delta_var * delta_t_neu_3) / delta_t_alt)
        return var

    def check_start_value_change(self):
        """
        check whether the end of the previous segment corresponds exactly to the start of the new segment. If yes, the
        calculation of the first time step of the new segment will not be done to save time. This comes in handy when
        the input file contains a high resolution of time steps.
        """
        self.start_value_change = False
        if self.segment[0][0] == 0:
            self.start_value_change = True
        else:
            for i in range(len(self.prev_segment_end)-1):
                if self.prev_segment_end[i] != self.segment[i][0]:
                    self.start_value_change = True

# -------------------------------------------------------------------------------------------------------------------- #
    def update_from_mission_profile(self, i, mission_profile: pd.DataFrame):
        """
        extracts condition names and the start and end values of the current segment indicated by i.

        :param mission_profile: dataframe of the complete mission profile
        :param i: index indicating start of the current segment
        """
        self.save_prev_segment_end()
        self.conditions = mission_profile.columns.tolist()
        complete_profile = mission_profile.transpose().values.tolist()
        self.segment = []
        for condition in complete_profile:
            self.segment.append([condition[i], condition[i + 1]])
        self.check_start_value_change()

# -------------------------------------------------------------------------------------------------------------------- #
