"""
Created on 1/11/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Description:
    stores parameters concerning the calculation
    takes an input and a circuit and return an output
"""
import numpy as np
from ..Circuits.circuit import Circuit
from ..Utilities.input import Input
from ..Utilities.output import Output
import time
import bfch.Utilities.data_handling as data
import bfch.Utilities.tools_misc as tools
from ..Utilities.circuit_init import CreateCircuit
import copy
import logging


class CircuitSolver:
    """
    Objects of this class can a circuit, input and output and solve every time step of the segment described by input
    """
    create_circuit = CreateCircuit()
    tools.initialize_logging()

    def __init__(self, max_step_size=60, allowed_dev=1e-2, max_solver_steps=1000, convergence_crit=1e-4,
                 solver_step_factor=0.1, step_factor_reduction_speed=100, min_num_steps_per_segment=1):
        """
        :param max_step_size: maximum duration between to calculations inside a segment, s
        :param allowed_dev: allowed deviation between a set boundary condition and the result, 1
        :param max_solver_steps: maximum number of steps, before the solver stops, 1
        :param convergence_crit: value of the euclidian norm of the function at which the solver stops, 1
        :param solver_step_factor: length scaling of update steps, 1
        """
        self.max_step_size = max_step_size
        self.min_num_steps_per_segment = min_num_steps_per_segment
        self.allowed_dev = allowed_dev
        self.max_solver_steps = max_solver_steps
        self.convergence_crit = convergence_crit
        self.solver_step_factor = solver_step_factor
        self.step_factor_reduction_speed = step_factor_reduction_speed

    def solve(self, circuit: Circuit, input_data: Input, output_data: Output, verbose=True):
        """
        solves the circuit for every time step inside the segment and fills the results into the output

        :param circuit: the circuit to be solved. needs to already contain valid components
        :param input_data: the information about the current segment
        :param output_data: the result of the current segment

        :return: output_data
        """
        duration_segment = input_data.segment[0][-1] - input_data.segment[0][0]

        # check whether the time step is small enough
        # if yes: use the given t1 and t2 as boundaries
        # if no: calculate needed time steps
        if duration_segment <= self.max_step_size and self.min_num_steps_per_segment == 1:
            input_data.input_to_boundary1()
        else:
            input_data.input_to_boundary2(self.max_step_size, duration_segment, self.min_num_steps_per_segment)

        if circuit.conditioning_setting:
            input_data.insert_conditioning_cycles(circuit.conditioning_period, circuit.conditioning_duration)

        # propagate information about the mission segment
        circuit.new_segment(input_data)

        # from here on, the components have the necessary data to perform the calculation -> compute!
        for i in range(circuit.t_place, circuit.t_place + np.size(input_data.segment[0])):
            circuit.solve_circuit(i, self.allowed_dev, self.max_solver_steps, self.convergence_crit,
                                  self.solver_step_factor, self.step_factor_reduction_speed, verbose=verbose)
            if circuit.solved is False or circuit.connectivity is None:
                pass
                # break
            circuit.t_place += 1

        # fill the output
        output_data.fill_output(circuit)
        return output_data

    def calculate_mission(self, mission_file_path, circuit, data2plot=None, result_path='Results/', verbose=True):
        if isinstance(circuit, str):
            circuit = self.create_circuit(circuit_type=circuit)
        elif not isinstance(circuit, Circuit):
            raise ValueError("The input parameter 'circuit' needs to either be the name of a circuit or an object of"
                             "<Circuit>")
        mission_name_start1 = mission_file_path.rfind('/')
        mission_name_start2 = mission_file_path.rfind('\\')
        mission_name_end = mission_file_path.rfind('.')
        mission = mission_file_path[np.max([mission_name_start1, mission_name_start2])+1:mission_name_end]

        mission_profile = data.get_mission_data(mission_file_path)
        model_input = Input()
        output_data = Output()
        list_of_outputs = []
        start = time.perf_counter()
        current_mission_step = 0  # current time step in the mission profile
        t_elapsed = 0  # elapsed time since the start of the calculation
        for i in range(0, len(mission_profile.index) - 1, 2):
            # get the information about the current segment into the input class
            model_input.update_from_mission_profile(i, mission_profile)

            # run the calculation for the current segment and store it in the list of outputs
            list_of_outputs.append(copy.deepcopy((self.solve(circuit, model_input, output_data, verbose=verbose))))

            # show calculation progress
            state0 = list_of_outputs[-1].output_circuit[f'{circuit.circuit_type} state 1'].iloc[0]
            state1 = list_of_outputs[-1].output_circuit[f'{circuit.circuit_type} state 1'].iloc[-1]
            logging.info(f'Segment: {i / 2 + 1} GO!| t = {current_mission_step}| at: t={t_elapsed}| State: {state0}')
            current_mission_step = np.around(mission_profile["time s"][i + 1], 3)
            t_elapsed = np.around(time.perf_counter() - start, 6)
            logging.info(f'Segment: {i / 2 + 1} FIN| t = {current_mission_step}| at: t={t_elapsed}| State: {state1}')

        data.save_output_to_csv(list_of_outputs, mission, circuit, path=result_path)
        if data2plot is not None:
            fig = data.plot_results_dyn(list_of_outputs, data2plot=data2plot[circuit.circuit_type])
            data.save_plots(circuit.circuit_type, fig, mission)
