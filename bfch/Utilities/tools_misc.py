import logging
import os
from datetime import datetime


def initialize_logging():
    now = datetime.now()
    current_date = now.strftime("%Y%m%d_%H%M%S")
    dir_path = os.path.dirname(os.path.realpath(__file__))
    logging_path = dir_path[:dir_path.find("hy2sim") + len("hy2sim")] + "\\Logs"
    if not os.path.isdir(logging_path):
        os.makedirs(logging_path)
    logging.basicConfig(filename=f"{logging_path}\\{current_date}_{os.getlogin()}_logfile.log", level=logging.DEBUG,
                        format='%(asctime)s | %(levelname)-8s | %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info("Start Solving Procedure", stacklevel=logging.INFO)
