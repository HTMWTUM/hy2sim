"""
Created on 1/10/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Description:
functions useful for loading, checking, preparing and saving data
    
"""
import yaml
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
from matplotlib.ticker import FormatStrFormatter
import os
import logging


def get_component_data(component_data):
    """
    :param component_data: dictionary with component: yaml file pairs

    :return: data, components as lists
    """
    components = []
    data = []
    raw_data = None
    dir_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    for component in component_data.keys():
        components.append(component)
        try:
            raw_data = open(f"{dir_path}/Data/Example_Batteries/{component_data[component]}")
        except FileNotFoundError: pass
        try:
            raw_data = open(f"{dir_path}/Data/Example_FuelCellStacks/{component_data[component]}")
        except FileNotFoundError: pass
        try:
            raw_data = open(f"{dir_path}/Data/Example_PowerElectronics/{component_data[component]}")
        except FileNotFoundError: pass
        try:
            raw_data = open(component_data[component])
        except FileNotFoundError: pass
        if raw_data is None:
            raise FileNotFoundError(f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n'
                  f'File {component_data[component]} not found in folder {dir_path}\Data. Please Check\n'
                  f'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n')
        data.append(yaml.load(raw_data, Loader=yaml.FullLoader))
        raw_data.close()
    return data, components


def get_mission_data(mission: str, conditions_defaults=None, conversions=None, set_duration=0, duration=100):
    """
    reads the mission data, converts the units and adds default values if needed

    :param conversions: dictionary containing possible conversions
    :param conditions_defaults: dictionary containing the data that is necessary to solve the circuit
    :param set_duration: whether to take the duration from the csv (0) or set it separately (1)
    :param duration: duration in case set_duration is 1
    :param mission: name of the csv containing the mission information

    :return: dataframe containing all data needed to build and calculate the segments
    """
    if conditions_defaults is None:
        conditions_defaults = {
            "time": [0, "s"],
            "power": [0, "W"],
            "rot_speed": [0, "Hz"],
            "rot_torque": [0, "Nm"],
            "ambient_temp": [290, "K"],
            "fc_temp": [350, "K"],
            "anode_p": [101325, "Pa"],
            "anode_h2_molfrac": [1, "1"],
            "cath_p": [3 * 101325, "Pa"],
            "cath_o2_molfrac": [0.21, "1"],
            "spec_heat_cap": [1004.5, "J/(kg*K)"],
            "isentropic_exp": [1.4, "1"],
            "ambient_pressure": [101325, "Pa"],
            "cath_massflow": [0.001, "kg/s"],
            "ambient_air_density": [1.13, "kg/m^3"],
            "bleed_air": [0, "kg/s"],
            "altitude": [0, "m"],
            "conditioning": [0, "1"]
        }
    if conversions is None:
        conversions = {
            "kW": ["mult", 1000],
            "MW": ["mult", 1e6],
            "rpm": ["mult", 0.016666667],
            "1/min": ["mult", 0.016666667],
            "rad/s": ["mult", 0.15915494309191],
            "C": ["add", 273.15],
            "bar": ["mult", 100000],
            "atm": ["mult", 9.86923267e-6],
            "min": ["mult", 60],
            "h": ["mult", 3600],
            "g/cm^3": ["mult", 1000]
        }
    # read csv
    try:
        mission_input = pd.read_csv(f'Data/Example_Missions/{mission}.csv')
    except:
        try:
            mission_input = pd.read_csv(mission)
        except:
            raise ValueError(f"Mission {mission}.csv: File  not found in Data/Example_Missions/!")
    num_steps = len(mission_input.index)
    input_names_unit = []
    input_names = []
    for name in mission_input.columns:
        input_names_unit.append(name.split())
        input_names.append(name.split()[0])
    try:
        input_names.index("time")
    except KeyError:
        logging.error(f'no time data set, please add timestamps to the mission data')
        raise ValueError
    data = []
    columns = []

    for key in conditions_defaults:
        try:
            index = input_names.index(key)
            if conditions_defaults[key][1] == input_names_unit[index][1]:
                data.append(mission_input.iloc[:, index].tolist())
            else:
                try:
                    if conversions[input_names_unit[index][1]][0] == "mult":
                        data.append(np.multiply(mission_input.iloc[:, index].tolist(),
                                                conversions[input_names_unit[index][1]][1]))
                    elif conversions[input_names_unit[index][1]][0] == "add":
                        data.append(np.add(mission_input.iloc[:, index].tolist(),
                                           conversions[input_names_unit[index][1]][1]))
                except KeyError:
                    logging.error(f'The unit {input_names_unit[index][1]} does not match any unit in the conversion table.')
                    raise KeyError
        except ValueError:
            logging.info(f'No {key} found, setting default value: {[conditions_defaults[key][0]]} {[conditions_defaults[key][1]]}')
            data.append([conditions_defaults[key][0]] * num_steps)
        columns.append(f'{key} {conditions_defaults[key][1]}')

    try:
        input_names.index("air_density")
        t = data[columns.index("ambient_temp K")]
        rho = data[columns.index("air_density kg/m^3")]
        rs = 287.1  # specific Gas constant, J/(kgK)
        p = np.multiply(np.multiply(rho, t), rs)
        data[columns.index("ambient_pressure Pa")] = p
    except ValueError:
        pass

    # adjust time data
    if set_duration == 0:
        pass
    else:
        data[0] = np.multiply(np.divide(data[0], data[0][-1]), duration)

    # add segment information
    columns.append("segment_id 1")
    data.append([int(i/2 + 1) for i in range(0, len(data[0]))])

    output = pd.DataFrame(data, columns)
    return output.transpose()


def create_time_steps(set_duration, data_t, timestep, duration, t0, min_num_steps=1):
    """
    creates additional time steps in a defined interval

    :param set_duration: whether the user wants to override the duration of the mission
    :param data_t: list of time steps
    :param timestep: size of the new time steps
    :param duration: duration of the mission
    :param t0: time at the start

    :return: the new list of time steps
    """
    if set_duration == 1:
        pass
    else:
        duration = data_t[-1]  # in h

    t = np.linspace(t0, t0+duration, np.max([min_num_steps, np.int_(np.ceil(duration / timestep))+1]))
    return t


def save_output_to_csv(list_of_outputs, mission, circuit, decimal='.', sep=',',
                       path='Results'):
    """
    saves the output to a certain path, filename contains the current date, time, circuit type, mission

    :param path: relative or absolute path where the csv needs to be saved
    :param list_of_outputs: list of outputs generated by circuit_solver.solve
    :param circuit_type: name of the circuit
    :param mission: name of the mission
    :param decimal: separator between units and decimals, standard: ','
    """
    # If the result path does not exist, create it:
    if not os.path.isdir(path):
        os.makedirs(path)
    circuit_type = circuit.circuit_type
    circuit_param = circuit.circuit_param
    component_info = circuit.component_info
    data = []
    for output in list_of_outputs:
        data.append(output.output_circuit)
    data = pd.concat(data, ignore_index=True)
    now = datetime.now()
    currentdate = now.strftime("%Y%m%d_%H%M%S")
    data.to_csv(f'{path}/{currentdate}_{circuit_type}_{mission}_result.csv', sep=sep, decimal=decimal)
    with open(f'{path}/{currentdate}_{circuit_type}_{mission}_circuit_param.yaml', 'w') as file:
        documents = yaml.dump(circuit_param, file)
    with open(f'{path}/{currentdate}_{circuit_type}_{mission}_component_info.yaml', 'w') as file2:
        documents = yaml.dump(component_info, file2)


def plot_results_dyn(list_of_outputs, mode='complete', data2plot='all', units=None):
    """
    plots results automatically depending on the mode and data2plot

    :param list_of_outputs: list of outputs generated by circuit_solver.solve
    :param mode: if 'complete' all segments will be displayed at once
    :param data2plot: dictionary of the form:

    .. code-block:: python

        "Title 1 Unit": {
            "componentname1": ["unknown_name1"],
            "componentname2": ["state_name1"]
        },
        "Title2": {
                "componentname2": ["state_name1", "unknown_name1"]
            }

    :param units: used units for correct display

    :return: a figure with all the plots
    """
    if units is None:
        units = {
            "power": "W",
            "current": "A",
            "soc": "1",
            "voltage": "V",
            "percentage": "%",
            "empty": "",
            "efficiency": "%"
        }

    data = []
    # create list of all output dataframes
    for output in list_of_outputs:
        data.append(output.output_circuit)
    if mode == "complete":
        # concatenate the dataframes to one
        data = [pd.concat(data, ignore_index=True)]
    else:
        pass

    # loop through all the segments (if complete -> only one segment left)
    for segment in data:
        time = segment['time']
        segment = segment.drop('time', axis=1)
        # plot all available data
        if data2plot == 'all':
            number_of_plots = len(segment.columns)
            width_of_plots = 10
            width_of_diagram = 8
            fig = plt.figure(figsize=[11, 5 * number_of_plots])
            i = 0
            for column in segment:
                # loop through all columns of the segment
                name_segments = column.split(" ")
                ax = plt.subplot2grid((number_of_plots, width_of_plots), (i, 0), colspan=width_of_diagram)
                fig.subplots_adjust(hspace=0.3)
                i += 1
                ax.set_ylabel(f'{name_segments[-2]} [{name_segments[-1]}]')
                ax.set_xlabel(f'time in [s]')
                ax.set_title(f'{name_segments[0]} {name_segments[1]}')
                #ax.yaxis.get_major_locator().set_params(integer=True)
                ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
                ax.plot(time, segment[column], color='black')
                ax.grid()
            return fig
        else:
            # only plot the desired data
            plot_titles = data2plot.keys()
            number_of_plots = len(plot_titles)
            width_of_plots = 10
            width_of_diagram = 8
            fig = plt.figure(figsize=[11, 5 * number_of_plots])
            i = 0
            for plot_title in data2plot.keys():
                # loop through all the plot titles
                # look for a unit in the title
                title_words = plot_title.lower().split()
                for key in units.keys():
                    title_words.append(key)
                seen = set()
                dupl = []
                for x in title_words:
                    if x in seen:
                        dupl.append(x)
                    else:
                        seen.add(x)
                if len(dupl) == 0:
                    dupl.append("empty")
                # create the plot
                ax = plt.subplot2grid((number_of_plots, width_of_plots), (i, 0), colspan=width_of_diagram)
                fig.subplots_adjust(hspace=0.3)
                i += 1
                ax.set_ylabel(f'{dupl[-1]} [{units[dupl[-1]]}]')
                ax.set_xlabel(f'time in [s]')
                ax.set_title(plot_title)
                # ax.yaxis.get_major_locator().set_params(integer=True)
                ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
                colors = ['black', 'red', 'blue', 'yellow', 'green']
                j = 0
                for component in data2plot[plot_title]:
                    # loop through the components whose data will be displayed
                    for measure in data2plot[plot_title][component]:
                        # loop through the measures that will be displayed
                        for k in segment.keys():
                            # loop through data frame to find the data and plot it
                            if f"{component} {measure}" in k:
                                y = segment[k]
                                ax.plot(time, y, color=colors[j], label=k)
                                annot_max_noarrow(time, y, i=j, var=f'{k} maximum')
                                annot_min_noarrow(time, y, i=j, var=f'{k} minimum')
                                j += 1
                                break
                ax.legend(loc="center right", bbox_to_anchor=(1.4, 1.0))
                ax.grid()
            return fig


def plot_results(circuit, circuit_type):
    """
    old function to plot data, not very handy

    :param circuit: circuit which was solved
    :param circuit_type: name of the circuit

    :return: figure of the plotted data
    """
    t = circuit.components[1].t
    # create Figure Grid
    number_of_plots = 13
    width_of_plots = 10
    width_of_diagram = 8
    fig = plt.figure(figsize=[11, 5 * number_of_plots])
    ax7 = plt.subplot2grid((number_of_plots, width_of_plots), (0, 0), colspan=width_of_diagram)
    ax0 = plt.subplot2grid((number_of_plots, width_of_plots), (1, 0), colspan=width_of_diagram)
    ax6 = plt.subplot2grid((number_of_plots, width_of_plots), (2, 0), colspan=width_of_diagram)
    ax1 = plt.subplot2grid((number_of_plots, width_of_plots), (3, 0), colspan=width_of_diagram)
    ax12 = plt.subplot2grid((number_of_plots, width_of_plots), (4, 0), colspan=width_of_diagram)
    ax2 = plt.subplot2grid((number_of_plots, width_of_plots), (5, 0), colspan=width_of_diagram)
    ax3 = plt.subplot2grid((number_of_plots, width_of_plots), (6, 0), colspan=width_of_diagram)
    ax4 = plt.subplot2grid((number_of_plots, width_of_plots), (7, 0), colspan=width_of_diagram)
    ax5 = plt.subplot2grid((number_of_plots, width_of_plots), (8, 0), colspan=width_of_diagram)
    ax8 = plt.subplot2grid((number_of_plots, width_of_plots), (9, 0), colspan=width_of_diagram)
    ax9 = plt.subplot2grid((number_of_plots, width_of_plots), (10, 0), colspan=width_of_diagram)
    ax10 = plt.subplot2grid((number_of_plots, width_of_plots), (11, 0), colspan=width_of_diagram)
    ax11 = plt.subplot2grid((number_of_plots, width_of_plots), (12, 0), colspan=width_of_diagram)
    fig.subplots_adjust(hspace=0.3)

    # State
    ax7.set_ylabel('State')
    ax7.set_title('State of the Hybrid System')
    ax7.yaxis.get_major_locator().set_params(integer=True)
    ax7.plot(t, np.around(circuit.plot_state()), color='black')
    ax7.set_ylim([0, 3])
    ax7.grid()

    # LOADS
    ax0.set_ylabel('Power [W]')
    ax0.set_title('Power Directly Connected to the Load')
    y = np.add(circuit.components[2].p_mission.return_val(), circuit.components[7].p_it.return_val())
    ax0.plot(t, y, color='black', label='Load Power')
    ax0.plot(t, circuit.components[3].plot_p_out(), color='blue', label='DCDC Fuel Cell Power')
    if circuit_type == 1:
        ax0.plot(t, circuit.components[5].plot_p_in(), color='yellow', label='DCDC Batcharging Power')
        ax0.plot(t, circuit.components[4].plot_p_out(), color='red', label='DCDC Batdischarging Power')
    else:
        ax0.plot(t, circuit.components[1].plot_p_out(), color='red', label='Battery Power')
    ax0.legend(loc="center right", bbox_to_anchor=(1.4, 0.8))
    ax0.set_ylim(bottom=0)
    ax0.grid()

    ax6.set_ylabel('Power [W]')
    if circuit_type == "regulated":
        ax6.set_title('Power in the Battery and Fuel Cell Stack Branches')
        ax6.plot(t, circuit.components[1].plot_p_out(), color='red', label='Battery Power')
        ax6.plot(t, (-circuit.components[5].plot_p_in()), color='yellow', label='DCDC Batcharging Power')
        ax6.plot(t, circuit.components[4].plot_p_out(), color='brown', label='DCDC Batdischarging Power')
    else:
        ax6.set_title('Power in the Fuel Cell Stack Branches')
    ax6.plot(t, circuit.components[0].plot_p_out(), color='blue', label='Fuel Cell Power')
    ax6.plot(t, circuit.components[3].plot_p_out(), color='green', label='DCDC Fuel Cell Power')
    ax6.legend(loc="center right", bbox_to_anchor=(1.4, 0.8))
    ax6.set_ylim(bottom=0)
    ax6.grid()

    # BATTERY
    # SOC
    ax1.set_ylabel('SOC')
    ax1.set_title('Battery')
    y = circuit.components[1].soc.return_val()
    ax1.plot(t, y, color='black')
    ax1.set_ylim([0, 1])
    annot_max(t, y, ax1)
    annot_min(t, y, ax1)
    ax1.grid()

    # Battery Temperature
    ax12.set_ylabel('T [K]')
    ax12.set_title('Battery')
    y = circuit.components[1].temp.return_val()
    ax12.plot(t, y, color='black')
    annot_max(t, y, ax12, 'right')
    annot_min(t, y, ax12, 'left')
    ax12.grid()

    # Battery Voltage
    ax2.set_ylabel('Battery Voltage [V]')
    y = np.add(circuit.components[1].u_out.return_val(), circuit.components[1].u_in.return_val())
    ax2.plot(t, y, color='black')
    annot_max(t, y, ax2)
    annot_min(t, y, ax2)
    ax2.grid()

    # Battery Current
    ax3.set_ylabel('Battery Current [A]')
    ax3.plot(t, np.around(np.add(circuit.components[1].i_out.return_val(), circuit.components[1].i_in.return_val()), 3), color='black')
    ax3.ticklabel_format(useOffset=False)
    ax3.grid()

    # FUEL CELL
    # Fuel Cell Voltage
    ax4.set_ylabel('Fuel Cell Voltage [V]')
    ax4.set_title('Fuel Cell')
    y = np.around(circuit.components[0].u_out.return_val(), 3)
    ax4.plot(t, y, color='black')
    annot_max(t, y, ax4)
    annot_min(t, y, ax4)
    ax4.ticklabel_format(useOffset=False)
    ax4.grid()

    # Fuel Cell Current
    ax5.set_ylabel('Fuel Cell Current [A]')
    y = np.around(circuit.components[0].i_out.return_val(), 3)
    ax5.plot(t, y, color='black')
    annot_max(t, y, ax5, 'right')
    annot_min(t, y, ax5, 'left')
    ax5.ticklabel_format(useOffset=False)
    ax5.grid()

    # Fuel Cell Partial Pressures
    ax8.set_ylabel('Partial Pressure [Pa]')
    ax8.plot(t, circuit.components[0].pressure_h2.return_val(), color='blue', label='hydrogen')
    ax8.plot(t, circuit.components[0].pressure_o2.return_val(), color='red', label='oxygen')
    ax8.ticklabel_format(useOffset=False)
    ax8.set_ylim(bottom=0)
    ax8.grid()

    # Fuel Cell Temperature
    ax9.set_ylabel('Temperature [K]')
    ax9.plot(t, circuit.components[0].fc_temperature.return_val(), color='blue')
    ax9.ticklabel_format(useOffset=False)
    ax9.grid()

    # Fuel Cell H2 Consumption
    ax10.set_ylabel('Mass [g]')
    y = circuit.components[0].h2_fuel_consumption.return_val()
    indices = [i for i, e in enumerate(y) if e == 0]
    y = np.delete(y, indices)
    temp_t = t.copy()
    temp_t = np.delete(temp_t, indices)
    ax10.bar(temp_t, y, color='blue', width=100/len(temp_t))
    ax10.ticklabel_format(useOffset=False)
    ax10.grid()

    # Fuel Cell H2 cumulated consumption
    ax11.set_ylabel('Mass [g]')
    y = circuit.components[0].h2_cumulated_consumption.return_val()
    ax11.plot(t, y, color='blue')
    annot_max(t, y, ax11)
    ax11.ticklabel_format(useOffset=False)
    ax11.grid()

    return fig


def annot_max(x, y, ax=None, pos='left'):
    """
    creates a box with arrow to show the maximum value

    :param x: time series
    :param y: values
    :param ax: plot
    :param pos: position of the box
    """
    xmax = x[np.argmax(y)]
    ymax = y.max()
    text = "x={:.3f}, y={:.3f}".format(xmax, ymax)
    if not ax:
        ax = plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops = dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data', textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    if pos == 'left':
        ax.annotate(text, xy=(xmax, ymax), xytext=(0.2, 1.1), **kw)
    else:
        ax.annotate(text, xy=(xmax, ymax), xytext=(0.94, 1.1), **kw)


def annot_max_noarrow(x, y, ax=None, i=0, var=""):
    """
    creates a box to show the maximum value

    :param var: name of variable
    :param i: number of annotation to separate the boxes, when plotting multiple lines at once
    :param x: time series
    :param y: values
    :param ax: plot
    """
    xmax = x[np.argmax(y)]
    ymax = y.max()
    text = "x={:.3f}, y={:.3f}".format(xmax, ymax)
    text_complete = f"{var}\n{text}"
    if not ax:
        ax = plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    kw = dict(xycoords='data', textcoords="axes fraction", bbox=bbox_props, ha="right", va="top")
    ax.annotate(text_complete, xy=(xmax, ymax), xytext=(1.4, 0.8-i*0.12), **kw)


def annot_min_noarrow(x, y, ax=None, i=0, var=""):
    """
    creates a box to show the minimum value

    :param var: name of variable
    :param i: number of annotation to separate the boxes, when plotting multiple lines at once
    :param x: time series
    :param y: values
    :param ax: plot
    """
    xmax = x[np.argmin(y)]
    ymax = y.min()
    text = "x={:.3f}, y={:.3f}".format(xmax, ymax)
    text_complete = f"{var}\n{text}"
    if not ax:
        ax = plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    kw = dict(xycoords='data', textcoords="axes fraction", bbox=bbox_props, ha="right", va="top")
    ax.annotate(text_complete, xy=(xmax, ymax), xytext=(1.4, 0.4-i*0.12), **kw)


def annot_min(x,y, ax=None, pos='right'):
    """
    creates a box with arrow to show the minimum value

    :param x: time series
    :param y: values
    :param ax: plot
    :param pos: position of the box
    """
    xmin = x[np.argmin(y)]
    ymin = y.min()
    text = "x={:.3f}, y={:.3f}".format(xmin, ymin)
    if not ax:
        ax = plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops = dict(arrowstyle="->", connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data', textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    if pos == 'right':
        ax.annotate(text, xy=(xmin, ymin), xytext=(0.94,1.1), **kw)
    else:
        ax.annotate(text, xy=(xmin, ymin), xytext=(0.2, 1.1), **kw)


def save_plots(circuit_type, fig, mission, path='Results/'):
    """
    saves the figure to a pdf to a certain path, filename contains the current date, time, circuit type, mission

    :param path: absolute or relative path where the figure should be saved
    :param circuit_type: name of the circuit
    :param fig: figure containing the plots
    :param mission: name of the mission
    """
    now = datetime.now()
    currentdate = now.strftime("%Y%m%d_%H%M%S")
    fig.savefig(f'{path}{currentdate}_{circuit_type}_{mission}_result.pdf', format='pdf', bbox_inches='tight')


def convert_import_curve(t, timestep, data, t_data):
    """
    interpolates linearly to fill missing time steps compared to a maybe updated time list.

    :param t_data: time stamps
    :param t: time series with specific step size
    :param timestep: step size of t
    :param data: 1d array containing the data

    :return: 1d array with only the data at each time step
    """
    # normalize time to 1
    normfac = t[-1]-t[0]
    t = t/normfac
    timestep = timestep/normfac

    normfac = t_data[-1]-t_data[0]
    t_data = t_data/normfac

    # calculate properties for every timestep
    output = [0]
    for i in range(np.size(t_data) - 1):
        delta_t = t_data[i + 1] - t_data[i]  # in s

        if delta_t == 0:
            add = np.array([data[i]])
            if np.size(output) == 1:
                output = add
            else:
                output = np.concatenate([output, add], 0)
        else:
            t_add = np.linspace(0, delta_t, np.int_(np.ceil(delta_t / timestep))+1)
            gradient = (data[i + 1] - data[i]) / delta_t
            add = gradient * t_add + data[i]
            if np.size(output) == 1:
                output = add
            else:
                output = np.concatenate([output, add], 0)

    if np.size(t) != np.size(output):
        output = output[:(np.size(t))]

    return output
