"""
Created on 31.01.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
from abc import abstractmethod
import numpy as np
from ..Utilities.variable import Variable
from ..Utilities.input import Input
import pandas as pd
from typing import final
import logging


class Component:
    """
    Parent class for all components that might be integrated to the circuit
    Creates a unified framework for basic interaction, e.g. currents and voltages
    And for any additional calculation that a specific component might need to do and pass to the output
    Boundary conditions and connections between the components are also modelled as <Component>
    """
    i_out: Variable
    i_in: Variable
    u_out: Variable
    u_in: Variable
    t_place = 0
    segment = 0
    order = 0
    num_eq = 0
    unknowns = []
    t = []
    logging_variables = []
    operating_conditions = []
    system_output = False
    newton_function_factor = 1
    comp_type = ""
    core = False

    # basic component characterization
    power_rated = 0.  # in W
    power_max = 0.  # in W
    mass = 0.   # in kg
    volume = 0.   # in l

    def __init__(self, order, num_eq, name, newton_z_step_factor=1., max_step_size=np.inf, newton_function_factor=1,
                 unknowns=None, logging_variables=None, op_conditions=None, func_post_values=None,
                 func_pre_values=None, comp_type=""):
        """
        :param order: How many unknowns does the component have
        :param num_eq: How many equations describe the component
        :param name: unique name for every component
        :param newton_z_step_factor: factor by which the step is multiplied, mainly to reduce the step size
        :param max_step_size: Maximum step size allowed when updating a value of this component
        :param newton_function_factor: factor by which the computed f is multiplied, mainly to reduce the step size
        :param unknowns: List<Variable> that are unknown and described by the equation of the circuit
        :param logging_variables: List<Variable> that need to be saved at the end of each time step
        :param op_conditions: List<Variable> that describe the operating conditions of the component
        :param func_post_values: List<functions> functions that are called after every time step. the functions need to
                                 be defined in the child class
        :param func_pre_values: List<functions> functions that are called before every time step. the functions need to
                                 be defined in the child class
        :param comp_type: type of the component
        """
        self.name = name
        self.comp_type = comp_type

        # Initialization of component solving parameters
        self.order = order
        self.num_eq = num_eq
        self.newton_scale = newton_z_step_factor
        self.max_step_size = max_step_size
        self.newton_function_factor = newton_function_factor

        # Define unknowns of the component
        if unknowns is not None:
            self.unknowns = unknowns
        if len(self.unknowns) != self.order:
            raise ValueError("The number of unknowns does not match the set order of the component")

        # Initialize which variables of the component will be logged
        self.efficiency = Variable("efficiency", "1")
        self.power_in = Variable("power_in", "W")
        self.power_out = Variable("power_out", "W")
        self.logging_variables = [self.efficiency, self.power_in, self.power_out]
        if logging_variables is not None:
            self.logging_variables.extend(logging_variables)

        # Initialize the operating conditions of the component
        if op_conditions is not None:
            self.operating_conditions = op_conditions

        # Initialize user functions for post calculations
        self.post_values_funcs = None
        if func_post_values is not None:
            self.post_values_funcs = func_post_values

        # Initialize user functions for post calculations
        self.pre_values_funcs = None
        if func_pre_values is not None:
            self.pre_values_funcs = func_pre_values

# -------------------------------------------------------------------------------------------------------------------- #
    # ABSTRACT METHODS
# -------------------------------------------------------------------------------------------------------------------- #
    @abstractmethod
    def calculate_efficiency(self):
        """
        has to be implemented in every component, especially if the efficiency is not one

        :return: the efficiency of the component in the current time step
        """
        pass

    @abstractmethod
    def calculate_mass(self):
        """
        has to be implemented in every component

        :return: the mass of the component
        """

    @abstractmethod
    def calculate_volume(self):
        """
        has to be implemented in every component

        :return: the volume of the component
        """

    @abstractmethod
    def calculate_rated_power(self):
        """
        has to be implemented in every component

        :return: the rated power of the component
        """

    @abstractmethod
    def scale_rated_power(self, scaling_factor):
        """
        has to be implemented in every component
        the number of series components is multiplied by the scaling_factor, if applicable
        otherwise, the mass is also multiplied by the scaling factor
        """

    @abstractmethod
    def f(self):
        """
        this returns the result of the equation(s) governing the component as part of the circuit

        .. warning::
            MUST be zero if fulfilled
            MUST return a list, even if only one equation is needed

        :return: List of results of all equations describing the component
        """
        pass

    @abstractmethod
    def df(self, d_unknown: Variable):
        """
        this is the calculation of the derivative of f wrt d_unknown

        .. warning::
            MUST return a list, even if f is only one equation.
            the length of the output has to match the length of the output of f

        use something like

        .. code-block:: python

            try:
                place = self.unknowns.index(d_unknown)
            except ValueError:
                return [0] * length of f
            if place == 0:
                return [self.dfdv()]
            elif place == 1:
                return [self.dfdi()] and continue for every possible unknown the component has

        :param d_unknown: Variable wrt which the derivative is calculated

        :return: List
        """
        pass

# -------------------------------------------------------------------------------------------------------------------- #
    # METHODS THAT MAY BE OVERRIDDEN
# -------------------------------------------------------------------------------------------------------------------- #
    def pass_initial_condition(self):
        """
        useful tool to pass initial conditions between connected components of the circuit. Is called at the circuit
        level
        """
        pass

    def set_initial_condition(self):
        """
        sets all unknown values to one, avoids possible divisions by zero
        """
        for unknown in self.unknowns:
            unknown.val = 1

    def calculate_segment_pre_values(self):
        """
        function that is called at the end of the definition of a new segment, to allow pre calculations, that do not
        require solving the circuit and that reduce the time need to perform the calculation
        """
        pass

# -------------------------------------------------------------------------------------------------------------------- #
    # PRE AND POST CALCULATIONS
# -------------------------------------------------------------------------------------------------------------------- #
    @final
    def calculate_pre_values(self):
        """
        function called before every time step to calculate or update certain values
        e.g.: soc, temperatures, fuel consumptions, etc.
        """
        # Calculate the pre values defined by the user
        if self.pre_values_funcs is not None:
            [f() for f in self.pre_values_funcs]

    @final
    def calculate_post_values(self):
        """
        function called after every time step to calculate or update certain values
        e.g.: soc, temperatures, fuel consumptions, etc.
        """
        if hasattr(self, 'i_in') and hasattr(self, 'u_in'):
            self.power_in.val = self.i_in() * self.u_in()

        if hasattr(self, 'i_out') and hasattr(self, 'u_out'):
            self.power_out.val = self.i_out() * self.u_out()

        # Calculate the post values defined by the user
        if self.post_values_funcs is not None:
            [f() for f in self.post_values_funcs]

        self.efficiency.val = self.calculate_efficiency()

# -------------------------------------------------------------------------------------------------------------------- #
    # FUNCTION RELATED TO SOLVING THE CIRCUIT
# -------------------------------------------------------------------------------------------------------------------- #
    @final
    def update_var(self, z_k_plus_1, z_cursor):
        """
        .. warning::
            DO NOT OVERRIDE
            updates the unknowns of the component after an iteration of the newton method

        :param z_k_plus_1: update step that need to be performed
        :param z_cursor: position in the list z_k_plus_1

        :return: the position in the list z_k_plus_1 after updating one component
        """
        for variable in self.unknowns:
            if abs(z_k_plus_1[z_cursor]) > self.max_step_size:
                variable.val += np.sign(z_k_plus_1[z_cursor]) * self.max_step_size * self.newton_scale
            else:
                variable.val += z_k_plus_1[z_cursor] * self.newton_scale
            z_cursor += 1
        return z_cursor

# -------------------------------------------------------------------------------------------------------------------- #
    # BEFORE AND AFTER A SEGMENT
# -------------------------------------------------------------------------------------------------------------------- #
    @final
    def new_segment(self, input_data: Input):
        """
        prepares the component for the new segment, fills unknowns and states with zeros and the operating conditions
        with the corresponding values from the input file. This all should work automatically, if not check the input
        files and the naming in the component
        runs calculate_segment_pre_values() at the end to prepare any additional values for the component

        :param input_data: contains a dataframe with all the information of the next segment
        """
        added_values = np.zeros((1, len(input_data.segment[0])))
        for variable in self.unknowns:
            variable.new_segment(added_values)
        for state in self.logging_variables:
            state.new_segment(added_values)
        for condition in self.operating_conditions:
            try:
                index = input_data.conditions.index(f'{condition.name} {condition.unit}')
            except ValueError:
                logging.error(f"Could not find: f'{condition.name} {condition.unit}' in the input file.")
                raise ValueError()
            if input_data.conditions[index].split()[-1] != condition.unit:
                logging.error(f"Unit of Condition: {condition.name} does not match the input unit.")
                raise ValueError()
            condition.new_segment(input_data.segment[index])
        self.calculate_segment_pre_values()
        self.segment+=1

    @final
    def pass_output(self, index=0):
        """
        collects all the calculated or given data of the component and creates a dataframe

        :param index: can be used to only retrieve a part of the data. In a mission containing several segments, this is
        used to retrieve segments separately

        :return: dataframe
        """
        data = []
        columns = []
        for unknown in self.unknowns:
            columns.append(f'{self.name} {unknown.name} {unknown.unit}')
            data.append(unknown.return_val(index))
        for state in self.logging_variables:
            columns.append(f'{self.name} {state.name} {state.unit}')
            data.append(state.return_val(index))
        for condition in self.operating_conditions:
            columns.append(f'{self.name} {condition.name} {condition.unit}')
            data.append(condition.return_val(index))
        output = pd.DataFrame(data, columns)
        return output.transpose()
