"""
Created on 17.12.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 1.00

File Description:
    
"""
import numpy as np
import scipy.optimize as sopt
import warnings
from .component import Component
from ..Utilities.variable import Variable
import logging


class FcModel(Component):
    """
    Model of the fc stack based on the polarization curve
    """
    def __init__(self, name, data):
        """
        the operating conditions are: anode_h2_molfrac, anode_p, cathode_o2_molfrac, cathode_p, fc_temperature
        The logged variables are: mincurrent, maxcurrent, h2_fuel_flow, power_reserve, h2_fuel_consumption,
                                  h2_cumulated_consumption, pressure_h2, pressure_o2, current_density,
                                  cell_voltage, maximum_power

        :param name: set a unique name for the FcModel Object
        :param data: dictionary containing all the needed information of the fc model, the available modeltypes are:
                     'basic', 'basic_act_var_t', 'basic_var_p_var_t', '1D_flux_simpleASR'
        """
        self.modeltype = data['modeltype']
        if 'dataset_type' in data:
            self.dataset_type = data['dataset_type']    # component: parameters are representing a complete part
                                                    # cell: parameters are representing a single cell
                                                    # technology: parameters are representing values for a technology
        else:
            self.dataset_type = "component"

        # Constants Parameters etc.
        self.R = 8.314  # Gas constant, J/(mol * K)
        self.F = 96485.309  # Faradays constant, C/mol
        self.k = 1.38064852e-23  # Boltzmann constant, J/K
        self.h = 6.62607015e-34  # Planck constant, J * s
        self.atm = 101325
        self.h2_molar_mass = 2.01588  # molar mass of H2, g/mol
        self.n_A = 2  # free electrons anode (H2: 2), dimensionless
        self.n_C = 4  # free electrons cathode (O2:4), dimensionless

        # General Data relating to the fuel cell stack
        if self.dataset_type == "component":
            if 'mass' in data: self.mass = data['mass']
            if 'volume' in data: self.volume = data['volume']
            if 'power_rated' in data:
                self.power_rated = data['power_rated']  # Rated Power of Fuel Cell Stack
            else:
                self.power_rated = self.calculate_rated_power()
        elif self.dataset_type == "cell":
            pass  # todo: assign general data for cell model
        elif self.dataset_type == "technology":
            pass  # todo: assign general data for technology model


        # todo: streamline the following lines of code and integrate them in the above
        # self.power_rated = 0.
        self.u_prated = 0.
        self.i_prated = 0.  # power DENSITY through the fuel cell stack at rated power

        # self.fmax_prated = 0.

        if 'u_prated' in data:
            self.u_prated = data['u_prated']  # Rated Power of Fuel Cell Stack
        # if 'fmax_prated' in data:
        #     self.u_prated = data['fmax_prated']  # Rated Power of Fuel Cell Stack

        self.series_cells = data['series_cells']  # number of fuel cell in series, dimensionless
        self.cell_area = data['cell_area']  # active area of a single fuel cell, cm^2
        self.stoichiometry = data['stoichiometry']  # dimensionless

        # Gas properties
        # todo: input yaml: state of product water and baseline temperature of the fuel cell. Calculate h, s and eff
        try:
            if data["h2o"] == "l":
                self.delta_h = 284396  # maximum total energy to be extracted from the chemical reaction at T=70C, J/mol
                self.delta_s_hat = -163.23  # entropy delta, J/(mol K), - 44.34 for H20(g)
            else:
                self.delta_h = 243000  # maximum total energy to be extracted from the chemical reaction at T=70C, J/mol
                self.delta_s_hat = -44.34  # entropy delta, J/(mol K), - 44.34 for H20(g)
        except KeyError:
            raise KeyError(f"Key <h2o> was not found in the yaml file of the fuel cell model, {self.modeltype}")

        try:
            self.eff_thermo = data['eff_thermo']  # dimensionless, 0.83 for H2O2 at standard T, 0.8037 at 80 C
        except KeyError:
            self.eff_thermo = 0.83
        self.hydrogen_hhv = self.delta_h / self.h2_molar_mass  # J/g higher heating value of hydrogen at T= 80 C

        # Model specific Data
        if self.modeltype == 'basic_act_var_t':
            self.alpha_A = data['alpha_A']  # Transfer coefficient anode, dimensionless
            self.alpha_C = data['alpha_C']  # Transfer coefficient cathode, dimensionless
            self.i_0A = data['i_0A']  # exchange current density anode, A/cm^2
            self.i_0C = data['i_0C']  # exchange current density cathode, A/cm^2
            self.ASR_ohm = data['ASR_ohm']  # area specific resistivity, Ohm*cm^2
            self.C = data['C']  # fitting constant, V
            self.i_L = data['i_L']  # limit current density, A/cm^2
            self.i_leak = data['i_leak']  # leakage current density, A/cm^2

        if self.modeltype == 'basic':
            self.a_A = 0  # fitting parameter a of anode for Tafel equation (eq 6.4 / eta_act_basic_complete), V
            self.b_A = 0  # fitting parameter b of anode for Tafel equation (eq 6.4 / eta_act_basic_complete), V
            self.a_C = 0  # fitting parameter a of cathode for Tafel equation (eq 6.4 / eta_act_basic_complete), V
            self.b_C = 0  # fitting parameter b of cathode for Tafel equation (eq 6.4 / eta_act_basic_complete), V

        if self.modeltype == 'basic_var_p_var_t':
            self.delta_g_1_A = 80e3  # activation barrier of the forward reaction at the anode (H2 -> 2H+ + 2e-), J/mol
            self.d_eff_A = 1  # effective diffusivity of the anode, m^2/s
            self.delta_A = 0  # thickness of the anode, m
            self.delta_g_1_C = 80e3  # activation barrier of the forward reaction at the cathode (O2 + 4e- -> 2O2-), J/mol
            self.d_eff_C = 0  # effective diffusivity of the cathode, m^2/s
            self.delta_C = 0  # thickness of the cathode, m
            self.delta_Cat_C = 0  # thickness of the catalyst layer at the cathode, m
            self.delta_Cat_A = 0  # thickness of the catalyst layer at the anode, m

        if self.modeltype == "1D_flux_simpleASR":
            self.ASR_ohm = data['ASR_ohm']  # area specific resistivity, Ohm/cm^2
            self.alpha_C = data['alpha_C']  # transfer coefficient cathode, dimensionless
            self.i_0C = data['i_0C']  # exchange current density cathode, A/cm^2
            self.d_eff_C = data['d_eff_C']  # that's not physically possible, effective diffusivity cathode, m^2/s
            self.delta_C = data['delta_C']  # thickness of the cathode, m

        if self.modeltype == "basic_var_p_var_t":
            self.d_eff_C = 0.0506  # effective diffusivity of the cathode, m^2/s
            self.delta_C = 500.0e-06  # thickness of the cathode, m
            self.delta_Cat_A = 0.1e-6  # thickness of the catalyst layer at the anode, m
            self.delta_Cat_C = 0.1e-6  # thickness of the catalyst layer at the cathode, m

        # Unknowns
        self.i_out = Variable("i_out", "A", val_min=1e-6)
        self.u_out = Variable("u_out", "V", val_min=1e-6, val_max=10000)

        # Logged Variables
        self.mincurrent = Variable("min_current", "A")  # minimum current possible through fuel cell stack, A
        self.maxcurrent = Variable("max_current", "A")  # maximum current possible through fuel cell stack, A
        self.h2_fuel_flow = Variable(val_min=0, name="h2_fuel_flow", unit="g/s")  # H2 flow towards anode, g/s
        # burned H2 between two steps, g
        self.h2_fuel_consumption = Variable(val_min=0, name="h2_fuel_consumption", unit="g")
        # burned H2 since start, g
        self.h2_cumulated_consumption = Variable(val_min=0, name="h2_cumulated_consumption", unit="g")
        self.pressure_h2 = Variable("pressure_h2", "Pa")  # pressure of the Hydrogen at the anode, Pa
        self.pressure_o2 = Variable("pressure_o2", "Pa")  # pressure of the Oxygen at the cathode, Pa
        self.power = Variable("power", "W")  # power output of the fuel cell stack, W
        # power input of the fuel cell stack for efficiency calc, W
        self.power_h2_equivalent = Variable("power_h2_equivalent", "W")
        self.current_density = Variable("current_density", "A/cm^2")
        self.cell_voltage = Variable("cell_voltage", "V")
        self.eff_real = Variable("efficiency_real", "1")
        self.power_reserve = Variable("power_reserve", "1")
        self.maximum_power = Variable("maximum_power", "W")

        # Data relating to the use of the fuel cell stack - without logging
        self.i_max_power = 0  # current through fuel cell stak at maximum power
        self.max_power = 0  # maximum power provided by the fuel cell
        self.u_pmax = 0.  # Voltage at max. power

        # Operating conditions of the fuel_cell
        self.anode_h2_molfrac = Variable("anode_h2_molfrac", "1")  # mole fraction of hydrogen, dimensionless
        self.pressure_anode = Variable("anode_p", "Pa")  # static pressure at the anode of the fuel cell stack, Pa
        self.cath_o2_molfrac = Variable("cath_o2_molfrac", "1")  # mole fraction of oxygen, dimensionless
        self.pressure_cathode = Variable("cath_p", "Pa")  # static pressure at the cathode of the fuel cell stack, Pa
        self.fc_temperature = Variable("fc_temp", "K")  # Temperature of the fuel cell stack, K

        super().__init__(2, 1, name, unknowns=[self.u_out, self.i_out],
                         op_conditions=[self.anode_h2_molfrac, self.pressure_anode, self.cath_o2_molfrac, self.pressure_cathode,
                                        self.fc_temperature],
                         logging_variables=[self.mincurrent, self.maxcurrent, self.h2_fuel_flow, self.power_reserve,
                                            self.h2_fuel_consumption, self.h2_cumulated_consumption, self.pressure_h2,
                                            self.pressure_o2, self.current_density, self.cell_voltage, self.maximum_power],
                         func_post_values=[self.calc_h2_consumption,
                                           self.calculate_add_post_values], func_pre_values=[self.find_limit_values])

# -------------------------------------------------------------------------------------------------------------------- #
    # POLARIZATION CURVES
# -------------------------------------------------------------------------------------------------------------------- #
    def fc_voltage(self, current):
        """
        * basic:

            * description: fuel cell thermodynamics and substracting ohmic, activation and concentration losses
            * use case: constant environment, which can be reproduced on a test bench to fit the parameters
            * parameters to fit: 8

        * basic_act_var_t:

            * description: basic + activation losses are temperature dependent - model used by Ng and Datta (2019)
            * use case: changing environment, testbench capable of reproducing some different temperatures
            * parameters to fit: 8

        * basic_var_p_var_t:

            * description: activation and concentration losses are now temperature and pressure dependent
            * use case: changing environment, testbench capable of reproducing some different T-p pairs
            * parameters to fit: 11

        * basic_var_p_var_t_simple:

            * description: basic_var_p_var_t but using a combination of the activation and concentration loss
            * use case: changing environment, less possibility to test
            * parameters to fit: 5

        * 1D_flux_simpleASR:

            * description: flux based model, (p206 ff)
            * use case:
            * parameters to fit: 5, but they could be approximated if needed

        :param current: current flowing through fuel cell stack

        :return: resulting voltage of the fuel cell stack using a polarization curve
        """
        i = current / self.cell_area
        v = 0
        if self.modeltype == "basic":
            v = self.basic(i)

        elif self.modeltype == "basic_act_var_t":
            v = self.basic_act_var_t(i)

        elif self.modeltype == "basic_var_p_var_t":
            v = self.basic_var_p_var_t(i)

        elif self.modeltype == "basic_var_p_var_t_simple":
            v = self.basic_var_p_var_t_simple(i)

        elif self.modeltype == "1D_flux_simpleASR":
            v = self.one_d_flux_simple_asr(i)

        return v * self.series_cells

    def basic(self, i):
        """
        description: fuel cell thermodynamics and subtracting ohmic, activation and concentration losses
        use case: constant environment, which can be reproduced on a test bench to fit the parameters

        :param i: current density through a single cell
        :param a_A: Anode factor a, V (from class, not input)
        :param b_A: Anode factor b, V (from class, not input)
        :param a_C: Cathode factor a, V (from class, not input)
        :param b_C: Cathode factor b, V (from class, not input)
        :param ASR_ohm: average surface resistivity, Ohm*cm^2 (from class, not input)
        :param i_l: limit current density, A/cm^2 (from class, not input)
        :param i_leak: leakage current density, A/cm^2 (from class, not input)
        :param c: fitting constant, V (from class, not input)

        :return: the voltage of the single cell
        """
        return self.e_therm() \
               - self.eta_act_basic_complete(i) \
               - self.eta_ohmic_asr(i) - self.eta_Conc_basic(i)

    def basic_act_var_t(self, i):
        """
        description: basic + activation losses are temperature dependent - model used by Ng and Datta (2019), [Ng2019]_.
        use case: changing environment, testbench capable of reproducing some different temperatures

        :param i: current density through the cell
        :param alpha_A: transfer coefficient anode, dimensionless (from class, not input)
        :param alpha_C: transfer coefficient cathode, dimensionless (from class, not input)
        :param i_0A: exchange current density anode, A/cm^2 (from class, not input)
        :param i_0C: exchange current density cathode, A/cm^2 (from class, not input)
        :param ASR_ohm: average surface resistivity, Ohm*cm^2 (from class, not input)
        :param i_l: limit current density, A/cm^2 (from class, not input)
        :param i_leak: leakage current density, A/cm^2 (from class, not input)
        :param c: fitting constant, V (from class, not input)

        :return: the voltage of the cell
        """
        return self.e_therm() \
               - self.eta_act_fp_complete_var_t(i) \
               - self.eta_ohmic_asr(i) - self.eta_Conc_basic(i)

    def basic_var_p_var_t(self, i):
        """
        description: activation and concentration losses are now temperature and pressure dependent
        use case: changing environment, testbench capable of reproducing some different T-p pairs

        :param i: current density through the cell
        :param delta_C: thickness cathode, m (from class, not input)
        :param delta_Cat_c: thickness of the cathode catalyst layer, m (from class, not input)
        :param d_eff_c: effective diffusion coefficient cathode, m^2/s (from class, not input)
        :param delta_g_1_c: delta activation energy cathode, J/mol (from class, not input)
        :param alpha_C: transfer coefficient cathode, dimensionless (from class, not input)
        :param delta_A: thickness anode, m (from class, not input)
        :param delta_Cat_a: thickness of the anode catalyst layer, m (from class, not input)
        :param d_eff_a: effective diffusion coefficient anode, m^2/s (from class, not input)
        :param delta_g_1_a: delta activation energy anode, J/mol (from class, not input)
        :param alpha_A: transfer coefficient anode, dimensionless (from class, not input)
        :param ASR_ohm: average surface resistivity, Ohm*cm^2 (from class, not input)
        :param i_leak: leakage current density, A/cm^2 (from class, not input)
        :param c: fitting constant, V (from class, not input)

        :return: the voltage of the cell
        """
        return self.e_therm() \
               - self.eta_act_fp_complete_var_p_var_t(i) \
               - self.eta_ohmic_asr(i) - self.eta_Conc_basic(i) # - self.eta_Conc_var_il(i)

    def basic_var_p_var_t_simple(self, i):
        """
        description: basic_var_p_var_t but using a combination of the activation and concentration loss
        use case: changing environment, less possibility to test

        :param i: current density through a cell
        :param ASR_ohm: average surface resistivity, Ohm*cm^2 (from class, not input)
        :param d_eff_c: effective diffusion coefficient cathode, m^2/s (from class, not input)
        :param delta_C: thickness cathode, m (from class, not input)
        :param i_leak: leakage current density, A/cm^2 (from class, not input)
        :param c: fitting constant, V (from class, not input)

        :return: the voltage of the cell
        """
        return self.e_therm() \
               - self.eta_ohmic_asr(i) \
               - self.eta_Conc_var_il(i)

    def one_d_flux_simple_asr(self, i):
        """
        flux based model, ([Hayre16]_ p206 ff)

        :param i: current density through a cell
        :param ASR_ohm: average surface resistivity, Ohm*cm^2 (from class, not input)
        :param alpha_C: transfer coefficient cathode, dimensionless (from class, not input)
        :param i_0C: exchange current density cathode, A/cm^2 (from class, not input)
        :param d_eff_c: effective diffusivity of the cathode, m^2/s (from class, not input)
        :param delta_C: thickness of the cathode, m (from class, not input)

        :return: the voltage of the cell
        """
        return self.e_therm() - self.eta_ohmic_asr(i) \
               - self.eta_Cath_basic(i)

# -------------------------------------------------------------------------------------------------------------------- #
    def e_therm(self):
        """
        :return: thermodynamic equilibrium potential, steady state value, independent of current
        """
        # return 1.229 - (self.fc_temperature() - 298.15) * 8.46e-04 * 4.309e-05 * (
        #         np.log(self.pressure_h2()) + 0.5 * np.log(self.pressure_o2())) #* self.fc_temperature()
        return (1.229 + self.delta_s_hat / (2 * self.F) * (self.fc_temperature() - 298.15)
                - ((self.R * self.fc_temperature()) / (2 * self.F))
                * np.log(1 / ((self.pressure_h2() / self.atm) * np.power((self.pressure_o2() / self.atm), 0.5))))

# -------------------------------------------------------------------------------------------------------------------- #
    # Activation losses
# -------------------------------------------------------------------------------------------------------------------- #

    def eta_act_basic_complete(self, i):
        """
        Deduced from Tafel Equation ([Hayre16]_ eq. 6.4)

        :param i_leak: leakage current density, A/cm^2
        :param a_A: Anode factor a, V (from class, not input)
        :param b_A: Anode factor b, V (from class, not input)
        :param a_C: Cathode factor a, V (from class, not input)
        :param b_C: Cathode factor b, V (from class, not input)
        :param i: current density, A/cm^2 (from class, not input)

        :return: activation loss (due to needed activation energy to split H2 and extract e-), V
        """
        return self.a_A + self.a_C + np.log(i + self.i_leak) * (self.b_A + self.b_C)  # Deduced from Tafel Equation (eq 6.4)

    def eta_act_fp_complete_var_t(self, i):
        """
        Tafel Equation for large i ([Hayre16]_ eq. 3.40)

        :param i_leak: leakage current density, A/cm^2 (from class, not input)
        :param i_0C:  exchange current density cathode, A/cm^2 (from class, not input)
        :param i_0A:  exchange current density anode, A/cm^2 (from class, not input)
        :param alpha_C:  transfer coefficient cathode, dimensionless (from class, not input)
        :param alpha_A:  transfer coefficient anode, dimensionless (from class, not input)
        :param i: current density, A/cm^2

        :return: activation loss (due to needed activation energy to split H2 and extract e-), V
        """
        RT = self.R * self.fc_temperature()  # J/mol
        F = self.F  # C/mol, [J/C] = [VAs/As] = [V]
        # Tafel Equation for large i (eq 3.40)
        return (RT / (self.alpha_A * self.n_A * F)) * (-np.log(self.i_0A) + np.log(i + self.i_leak)) \
               + (RT / (self.alpha_C * self.n_C * F)) * (-np.log(self.i_0C) + np.log(i + self.i_leak))

    def eta_act_fp_cathode_var_t(self, i):
        """
        Tafel equation for large i ([Hayre16]_ eq 3.40) reduced to cathode, due to sluggishness of cathode reaction

        :param i_leak:  leakage current density, A/cm^2
        :param i_0C:  exchange current density cathode, A/cm^2
        :param alpha_C:  transfer current density cathode, dimensionless
        :param i: current density, A/cm^2

        :return: activation loss at the cathode (due to needed activation energy to split H2 and extract e-), V
        """
        RT = self.R * self.fc_temperature()  # J/mol
        F = self.F  # C/mol C = As; J= VAs -> J/C = V
        # Tafel equation for large i (eq 3.40) reduced to cathode, due to sluggishness of cathode reaction
        return (RT / (self.alpha_C * self.n_C * F)) * (-np.log(self.i_0C) + np.log(i + self.i_leak))

    def eta_act_fp_complete_var_p_var_t(self, i):
        """
        * decay rate: [Hayre16]_ eq 3.17
        * Reactant concentration in flow channel from ideal gas law ([Hayre16]_ p. 182)
        * Reactant concentration at catalyst layer ([Hayre16]_ eq 5.9)
        * exchange current density ([Hayre16]_ eq 3.36)
        * Tafel Equation for large i ([Hayre16]_ eq 3.40)

        :param delta_Cat_a:  thickness of the anode catalyst layer, m
        :param delta_Cat_c:  thickness of the cathode catalyst layer, m
        :param i:  current density, A/cm^2
        :param delta_C:  thickness cathode, m
        :param d_eff_c:  effective diffusion coefficient cathode, m^2/s
        :param delta_g_1_c:  delta activation energy cathode, J/mol
        :param alpha_C:  transfer coefficient cathode, dimensionless
        :param delta_A:  thickness anode, m
        :param d_eff_a:  effective diffusion coefficient anode, m^2/s
        :param delta_g_1_a:  delta activation energy anode, J/mol
        :param alpha_A:  transfer coefficient anode, dimensionless
        :param i_leak:  leakage current density, A/cm^2

        :return: activation loss dependent on pressure and temperature based on first principles, V
        """
        F = self.F  # C/mol
        RT = self.R * self.fc_temperature()  # J/mol
        T = self.fc_temperature()  # K
        k = self.k  # Boltzmann constant, J/K
        h = self.h  # Planck constant, J*s
        f1 = (k * T) / h  # decay rate, 1/s (eq 3.17)

        # Cathode
        c_r_0_C = self.pressure_o2() / RT  # Reactant concentration in flow channel from ideal gas law (p 182), mol/m^3
        # Reactant concentration at catalyst layer (eq 5.9), mol/m^3
        c_r_C = c_r_0_C - (i * 1e4 * self.delta_C / (self.n_C * F * self.d_eff_C))
        # exchange current density (eq 3.36), A/cm^2
        i_0C = self.n_C * F * c_r_C * self.delta_Cat_C * f1 * np.exp(- self.delta_g_1_C / RT) * 1e-4

        # Anode
        c_r_0_A = self.pressure_h2() / RT  # Reactant concentration in flow channel from ideal gas law (p 182), mol/m^3
        # Reactant concentration at catalyst layer (eq 5.9), mol/m^3
        c_r_A = c_r_0_A - (i * 1e4 * self.delta_A / (self.n_A * F * self.d_eff_A))
        # exchange current density (eq 3.36), A/cm^2
        i_0A = self.n_A * F * c_r_A * self.delta_Cat_A * f1 * np.exp(- self.delta_g_1_A / RT) * 1e-4
        
        # if (0.2<i) and (i<0.21): print('ioc:',i_0C,'ioa:',i_0A)
        # Tafel Equation for large i (eq 3.40)
        return (RT / (self.alpha_A * self.n_A * F)) * (-np.log(i_0A) + np.log(i + self.i_leak)) \
               + (RT / (self.alpha_C * self.n_C * F)) * (-np.log(i_0C) + np.log(i + self.i_leak))

    def eta_act_fp_cath_var_p_var_t(self, i):
        """
        :param delta_Cat_c: thickness of the catalyst layer at the cathode, m
        :param i: current density, A/cm^2
        :param delta_C:  thickness of the cathode, m
        :param d_eff_c:  effective diffusivity cathode, m^2/s
        :param delta_g_1_c:  delta activation energy cathode, J/mol
        :param alpha_C:  transfer coefficient cathode, dimensionless
        :param i_leak:  leakage current density, A/cm^2

        :return: activation loss at the cathode dependent on pressure and temperature based on first principles, V
        """
        F = self.F  # C/mol
        RT = self.R * self.fc_temperature()  # J/mol
        T = self.fc_temperature()  # K
        k = self.k  # Boltzmann constant, J/K
        h = self.h  # Planck constant, Js
        f1 = (k * T) / h  # decay rate, 1/s

        # Cathode
        c_r_0_C = self.pressure_o2() / RT  # concentration at in flow path, mol/m^3
        # Reactant concentration at catalyst layer (eq 5.9), mol/m^3
        c_r_C = c_r_0_C - (i * 1e4 * self.delta_C / (self.n_C * F * self.d_eff_C))
        # exchange current density (eq 3.36), A/cm^2
        i_0C = self.n_C * F * c_r_C * self.delta_Cat_C * f1 * np.exp(- self.delta_g_1_C / RT) * 1e-4
        # Tafel equation for large i (eq 3.40) reduced to cathode, due to sluggishness of cathode reaction
        return (RT / (self.alpha_C * self.n_C * F)) * (-np.log(i_0C) + np.log(i + self.i_leak))

# -------------------------------------------------------------------------------------------------------------------- #
    # Ohmic Losses
# -------------------------------------------------------------------------------------------------------------------- #
    def eta_ohmic_asr(self, i):
        """
        :param ASR_ohm: average surface resistivity, Ohm*cm^2
        :param i: current density, A/cm^2

        :return: ohmic loss of the fuel cell, V
        """
        return i * self.ASR_ohm

    def eta_ohmic_asr_var_t(self, i):
        """
        only useful for SOFC

        :param delta_G_act: electrolyte activation energy, J/mol, e.g. 1e5
        :param t_m: electrolyte thickness, m, e.g. 20e-6
        :param A: electrolyte constant, K/(Ohm*m), e.g. 9e7
        :param R: gas constant, J/(mol*K), 8.314
        :param T: Temperature, e.g. 1053
        :param i: current density, A/cm^2,

        :return: ohmic loss of the fuel cell, V
        """
        return i * (self.t_m * self.T) / (self.A * np.exp(-self.delta_G_act / (self.R * self.T)))

# -------------------------------------------------------------------------------------------------------------------- #
    # Concentration Losses
# -------------------------------------------------------------------------------------------------------------------- #
    def eta_conc_nernst(self, i):
        return ((self.R * self.fc_temperature()) / (2 * self.F)) * np.log(self.i_L / (self.i_L - i))

    def eta_Conc_basic(self, i):
        """
        :param c: fittin constant, V
        :param i_leak: leakage current density, A/cm^2
        :param i_l: limit current density, A/cm^2
        :param i: current density, A/cm^2

        :return: concentration losses, V
        """
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return self.C * np.log(self.i_L / (self.i_L - (i + self.i_leak)))

    def eta_Conc_var_il(self, i):
        """
        This variant should be very similar to using eta_Conc_basic and eta_act_fp_cath_var_p_var_t
        i_L: limiting current density of the fuel cell, concentration falls to zero

        :param c: fitting constant, V
        :param i_leak: leakage current density, A/cm^2
        :param delta_C: thickness of the cathode, A/cm^2
        :param d_eff_c: effective diffusivity of oxygen in the cathode, m^2/s
        :param i: current density A/cm^2

        :return: concentration losses, V
        """
        F = self.F  # Faraday's constant, C/mol
        R = self.R  # J/(mol*K)
        T = self.fc_temperature()  # K
        p_r_0_C = self.pressure_o2()  # pressure, Pa
        c_r_0_C = p_r_0_C / (R * T)  # concentration in flow channel (eq 5.13), mol/m^3
        # limiting current density w/o leaking current (eq 5.10), A/cm^2
        i_l = self.n_C * F * self.d_eff_C * (c_r_0_C / self.delta_C) * 1e-6
        # i_l should be at 0.31 under standard conditions
        d_eff_c_tmp = 0.31 / (self.n_C * F * (c_r_0_C / self.delta_C) * 1e-6)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return self.C * np.log(i_l / (i_l - (i + self.i_leak)))

    def eta_Cath_basic(self, i):
        """
        contains concentration and activation losses, only on cathode side, since the reaction is a lot slower. From
        [Hayre16]_ eq. 6.27

        :param delta_C: thickness of the cathode, m
        :param d_eff_c: effective diffusivity of the cathode, m^2/s
        :param i_0C: exchange current density cathode, A/cm^2
        :param alpha_C: transfer coefficient cathode, dimensionless
        :param i: current density, A/cm^2

        :return: cathode over voltage, V
        """
        x_o2 = self.cath_o2_molfrac()  # oxygen inlet mole fraction
        a = (self.R * self.fc_temperature()) / (4 * self.alpha_C * self.F)  # pre factor, V
        b = (i * 1e4 * self.R * self.fc_temperature()) / (4 * self.F * self.pressure_o2() * self.d_eff_C)  # factor, 1/m
        # inside_log = i / (i_0C * (self.pressure_o2() / 101325) * (x_o2 - delta_C * b))
        inside_log = i / (self.i_0C * self.pressure_o2() * (x_o2 - self.delta_C * b))  # unit correction
        if inside_log < 0:
            eta_Cath_res = np.nan
        elif inside_log == 0:
            eta_Cath_res = 0
        else:
            eta_Cath_res = a * np.log(inside_log)  # eq 6.27
        return eta_Cath_res

# -------------------------------------------------------------------------------------------------------------------- #
    # ADDITIONAL FUNCTIONS
# -------------------------------------------------------------------------------------------------------------------- #
    def find_limit_values(self):
        """
        calculates maximum current and power of the fuel cell stack. Additionally the current and the voltage at max
        power are calculated
        """
        if self.check_ambient_change():
            i_max = sopt.fmin(self.calculate_power, ftol=1e-4, x0=1, disp=False)
            self.maxcurrent.val = i_max[0]
            self.maximum_power.val = -self.calculate_power(i_max[0])
            self.u_pmax = self.maximum_power() / i_max[0]
            self.i_max_power = i_max[0]

            if self.power_rated != 0:
                try:
                    self.i_prated = sopt.fmin(self.delta_rated, ftol=1e-4, x0=1, disp=False)[0]
                    self.u_prated = self.power_rated / self.i_prated
                except:
                    logging.error("Rated voltage calculation did not converge. Check rated power.")
                    self.u_prated = 0.
                    self.i_prated = 0.

        else:
            self.mincurrent.val = self.mincurrent.prev_val
            self.maxcurrent.val = self.maxcurrent.prev_val
            self.maximum_power.val = self.maximum_power.prev_val

        if True:
            i = np.linspace(0, self.cell_area * 100., 100000)
            i_v_tmp = np.zeros((2, len(i)))
            i_v_tmp[0][:] = i
            k = 0
            for j in range(len(i)):
                i_v_tmp[1][j] = self.fc_voltage(i[j])
                if np.isnan(i_v_tmp[1][j]) and j > 100:
                    break
                k += 1
            i_v = np.zeros((2, k))
            i_v[0][:] = i_v_tmp[0][:k]
            i_v[1][:] = i_v_tmp[1][:k]
            nan_array = np.isnan(i_v[1][:])
            not_nan_array = ~ nan_array

            try:
                index_zero = np.argwhere(np.array(i_v[1][not_nan_array])<0)[0]
            except IndexError:
                index_zero = [len(i)]
            i_cleaned_tmp = i_v[0][not_nan_array]
            if len(i_cleaned_tmp) > index_zero[0]:
                i_cleaned = i_cleaned_tmp[:index_zero[0]]
            else:
                i_cleaned = i_cleaned_tmp
            self.mincurrent.val = i_cleaned[0]
            self.maxcurrent.val = i_cleaned[-1]

        self.i_out.val_max = self.maxcurrent()

    def calculate_power(self, i_fc):
        """
        calculation of the negative power of the fuel cell stack based on the current through the stack

        :param i_fc: current through the fuel cell stack

        :return: the negative power of the fuel cell in W
        """
        p = i_fc * self.fc_voltage(i_fc)
        return -p
    
    def delta_rated(self, i_fc):
        """
        helper function to find rated voltage
        """
        delta=abs(self.power_rated-abs(self.calculate_power(i_fc)))
        return delta

    def check_ambient_change(self):
        """
        check whether there has been a change in the ambient conditions since the previous time step

        :return: a bool about the change, if something changed, return True
        """
        if self.t_place == 0:
            return True
        else:
            change_p_h2 = self.pressure_h2() == self.pressure_h2.prev_val
            change_p_o2 = self.pressure_o2() == self.pressure_o2.prev_val
            change_temperature = self.fc_temperature() == self.fc_temperature.prev_val
            max_current_calc = self.maxcurrent.prev_val != 0
            change = change_p_h2 and change_p_o2 and change_temperature and max_current_calc
            return not change
    
    # def calculate_efficiency(self, v=0.):
    #     """
    #     calculates the real efficiency from thermodynamic, voltage and stoichiometric efficiency
    #     :return: efficiency 
    #     """
    #     if not v:
    #         v = self.u_out()
        
    #     eff = self.eff_thermo*(v/self.series_cells/self.e_therm())/self.stoichiometry
    #     return eff

    def calculate_efficiency(self, v=0.):
        """
        calculates the real efficiency from H2 mass flow and output power
        """
        # todo: hydrogen_hhv should be dependent on the temperature
        if not v:
            v = self.u_out()
        energy_flow_h2 = self.h2_fuel_flow() * self.hydrogen_hhv  # in Ws/g
        return self.i_out() * v / energy_flow_h2

    def calculate_efficiency_voltage(self, v=0.):
        """
        calculates the efficiency from the voltage losses. valid only at baseline temperature (!= standard T)
        is used for testing of the calculate efficiency method.
        """
        if not v:
            v = self.u_out()
        epsilon_therm = self.eff_thermo
        epsilon_loss = v / (self.e_therm() * self.series_cells)
        epsilon_stoich = 1 / self.stoichiometry
        return epsilon_stoich * epsilon_loss * epsilon_therm

    def calculate_mass(self):
        if self.mass == 0:
            pass  # todo: calculate the expected mass
        else:
            return self.mass

    def calculate_volume(self):
        if self.volume == 0:
            pass  # todo: calculate the expected volume
        else:
            return self.volume

    def calculate_rated_power(self, recalculation=False):
        if recalculation:
            #if self.i_prated != 0:

            pass  # todo: insert calculation for the rated power
        else:
            return self.power_rated

    def scale_rated_power(self, scaling_factor):
        # todo: allow different scaling types (voltage, current, both)
        self.series_cells = np.ceil(self.series_cells * scaling_factor)  # add series cells to match the scaling factor
        self.power_rated *= scaling_factor  # todo: do a correct recalculation of the rated power

    def calc_h2_mass_flow(self):
        """
        ::warning:: Discontinued
        calculates the mass flow of hydrogen at the current time step

        :return: the mass flow in g/s
        """
        i = self.i_out() * self.series_cells  # current generated by the fuel cell stack, A
        n = 2  # electrons per hydrogen molecule
        f = self.F  # Faraday's constant, C/mol
        h2_mol_flow_useful = i / (n * f)  # in mol/s
        # eff = self.calculate_efficiency()
        # h2_mass_flow = h2_mol_flow_useful * self.h2_molar_mass * self.stoichiometry * 1/self.efficiency  # g/s
        h2_mass_flow = h2_mol_flow_useful * self.h2_molar_mass * self.stoichiometry  #/ eff
        self.h2_fuel_flow.val = h2_mass_flow
        return h2_mass_flow

    def calc_h2_consumption(self):
        """
        calculate the hydrogen flow rate and the consumed hydrogen in the past time step
        """
        # Calculate mass flow rate of hydrogen at the anode
        h2_mol_flow_useful = (self.i_out() * self.series_cells) / (self.n_A * self.F)  # in mol/s
        self.h2_fuel_flow.val = h2_mol_flow_useful * self.h2_molar_mass * self.stoichiometry  # in g/s
        # Calculate consumed hydrogen mass
        if self.t_place == 0:
            self.h2_fuel_consumption.val = 0
        else:
            t0 = self.t[(self.t_place - 1)]
            t1 = self.t[self.t_place]
            self.h2_fuel_consumption.val = (t1 - t0) * self.h2_fuel_flow()  # g

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def calculate_segment_pre_values(self):
        self.pressure_h2.overwrite(np.multiply(self.pressure_anode.return_val(), self.anode_h2_molfrac.return_val()))
        self.pressure_o2.overwrite(np.multiply(self.pressure_cathode.return_val(), self.cath_o2_molfrac.return_val()))

    def calculate_add_post_values(self):
        self.h2_cumulated_consumption.val = self.h2_cumulated_consumption.prev_val+self.h2_fuel_consumption()
        self.current_density.val = self.i_out() / self.cell_area
        self.cell_voltage.val = self.u_out() / self.series_cells
        self.power_reserve.val = (self.i_out()*self.u_out())/self.maximum_power()

    def f(self):
        return [self.fc_voltage(self.i_out()) - self.u_out()]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0]
        if place == 0:
            return [self.dfdu()]
        elif place == 1:
            return [self.dfdi()]

    def dfdi(self):
        h = 1e-10
        if self.i_out() <= 1e-10:
            return (self.fc_voltage(self.i_out() + h) - self.fc_voltage(self.i_out())) / h
        else:
            return (self.fc_voltage(self.i_out()) - self.fc_voltage(self.i_out() - h)) / h

    def dfdu(self):
        return -1

    def set_initial_condition(self):
        self.i_out.val = 0.5 * self.maximum_power()/self.u_pmax
        self.u_out.val = self.fc_voltage(self.i_out())
