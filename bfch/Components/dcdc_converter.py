"""
Created on 17.12.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 1.00

File Description:
    
"""
from .component import Component
from ..Utilities.variable import Variable


class DcDc(Component):

    def __init__(self, name, data):
        """
        :param data: only parameter needed is "beta", typical values range from 0.75 to 0.95.
        """

        # data relating to the specific dcdc converter model
        self.beta = data['beta']
        if 'mass' in data: self.mass = data['mass']
        if 'volume' in data: self.volume = data['volume']

        # Unknowns
        self.i_out = Variable("i_out", "A", val_max=10000, val_min=1e-6)
        self.i_in = Variable("i_in", "A", val_max=10000, val_min=1e-6)
        self.u_out = Variable("u_out", "V", val_min=1e-6, val_max=10000)
        self.u_in = Variable("u_in", "V", val_min=1e-6, val_max=10000)

        super().__init__(4, 1, name, unknowns=[self.u_in, self.i_in, self.u_out, self.i_out])

# -------------------------------------------------------------------------------------------------------------------- #
# Override Abstract Methods
    def calculate_efficiency(self):
        return self.beta

    def calculate_mass(self):
        return self.mass  # todo: Update!

    def calculate_volume(self):
        return self.volume  # todo: Update!

    def calculate_rated_power(self):
        return 0.  # todo: Update!

    def scale_rated_power(self, scaling_factor):
        self.mass *= scaling_factor

# functions needed for Newtons method
    def f(self):
        if self.i_in() == 0:
            return [self.i_out()]
        else:
            return [(self.u_out()*self.i_out())/(self.beta*self.i_in())-self.u_in()]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0]
        if place == 0:
            return self.df_duin()
        elif place == 1:
            return self.df_diin()
        elif place == 2:
            return self.df_duout()
        elif place == 3:
            return self.df_diout()

    def df_duout(self):
        if self.i_in() == 0:
            return [1e-20]
        else:
            return [self.i_out()/(self.beta*self.i_in())]

    def df_diout(self):
        if self.i_in() == 0:
            return [1]
        else:
            return [self.u_out()/(self.beta*self.i_in())]

    def df_duin(self):
        if self.i_in() == 0:
            return [1e-20]
        else:
            return [-1]

    def df_diin(self):
        if self.i_in() == 0:
            return [1e-20]
        else:
            return [-(self.u_out() * self.i_out()) / (self.beta*(self.i_in() ** 2))]


class DcDc_Mult(Component):

    def __init__(self, name, data):
        """
        :param data: only parameter needed is "beta", typical values range from 0.75 to 0.95.
        """

        # data relating to the specific dcdc converter model
        self.beta = data['beta']
        self.voltage_multiplicator = data['voltage_multiplicator']  # factor between input and output voltage

        # Unknowns
        self.i_out = Variable("i_out", "A", val_min=1e-6)
        self.i_in = Variable("i_in", "A", val_min=1e-6)
        self.u_out = Variable("u_out", "V", val_max=100000)
        self.u_in = Variable("u_in", "V", val_max=100000)

        super().__init__(4, 2, name, unknowns=[self.u_in, self.i_in, self.u_out, self.i_out])

# -------------------------------------------------------------------------------------------------------------------- #
# Override Abstract Methods
    def calculate_efficiency(self):
        return self.beta

# functions needed for Newtons method
    def f(self):
        if self.i_in() == 0:
            if self.u_out() == 0:
                return [(self.beta * self.i_in() * self.u_in()) / 1e-20 - self.i_out(),
                        self.u_out() - self.u_in()*self.voltage_multiplicator]
            return [(self.beta*self.i_in()*self.u_in())/self.u_out()-self.i_out(),
                    self.u_out() - self.u_in()*self.voltage_multiplicator]
        else:
            return [(self.u_out()*self.i_out())/(self.beta*self.i_in())-self.u_in(),
                    self.u_out() - self.u_in()*self.voltage_multiplicator]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0, 0]
        if place == 0:
            return self.df_duin()
        elif place == 1:
            return self.df_diin()
        elif place == 2:
            return self.df_duout()
        elif place == 3:
            return self.df_diout()

    def df_duout(self):
        if self.i_in() == 0:
            return [-1e-20, 1]
        else:
            return [self.i_out()/(self.beta*self.i_in()), 1]

    def df_diout(self):
        if self.i_in() == 0:
            return [-1, 0]
        else:
            return [self.u_out()/(self.beta*self.i_in()), 0]

    def df_duin(self):
        if self.i_in() == 0:
            return [1e-20, -self.voltage_multiplicator]
        else:
            return [-1, -self.voltage_multiplicator]

    def df_diin(self):
        if self.i_in() == 0:
            if self.u_out()==0:
                return [(self.beta * self.u_in()) / 1e-20, 0]
            return [(self.beta*self.u_in())/self.u_out(), 0]
        else:
            return [-(self.u_out() * self.i_out()) / (self.beta*(self.i_in() ** 2)), 0]
