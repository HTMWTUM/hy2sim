"""
Created on 04.04.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
from .component import Component
from ..Utilities.variable import Variable


class Resistor(Component):
    """
    Model of the resistor based on Ohms law
    """
    def __init__(self, name, data):
        """
        models the a resistor. The model has two unknowns and one equation

        :param name: set a unique name for the Resistor Object
        :param data: dictionary containing all the needed information of the resistor, "R"
        """

        # Data relating to the resistor
        self.r = data['R']
        self.r_store = data['R']

        # Unknowns
        self.i_out = Variable("i_out", "A")
        self.i_in = Variable("i_in", "A")
        self.u_out = Variable("u_out", "V")
        self.u_in = Variable("u_in", "V")

        # Data relating to the use of the resistor
        self.power_loss = Variable("power_loss", "W", val_min=0)

        super().__init__(4, 2, name, unknowns=[self.u_in, self.i_in, self.u_out, self.i_out],
                         newton_function_factor=0.5,
                         newton_z_step_factor=1,
                         logging_variables=[self.power_loss], func_post_values=[self.calculate_power_loss])

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def pass_initial_condition(self):
        self.u_in.val = self.u_out()

    def calculate_efficiency(self):
        if self.power_out() == 0 or self.power_in() == 0:
            return 1
        else:
            return self.power_out() / self.power_in()

    def calculate_power_loss(self):
        self.power_loss.val = self.i_in() ** 2 * self.r

    def f(self):
        if self.i_in() <= 1e-20 or self.i_out() <= 1e-20:
            return[self.i_in() - self.i_out(), self.i_in() - self.i_out()]
        else:
            return [(self.u_in() - self.u_out()) - (((self.i_in()+self.i_out())/2) * self.r), self.i_in() - self.i_out()]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0, 0]
        if place == 0:
            if self.i_in() <= 1e-20 or self.i_out() <= 1e-20:
                return [1e-40, 0]
            else:
                return [1, 0]
        elif place == 1:
            if self.i_in() <= 1e-20 or self.i_out() <= 1e-20:
                return [-self.r/2, 1]
            else:
                return [-self.r/2, 1]
        elif place == 2:
            if self.i_in() <= 1e-20 or self.i_out() <= 1e-20:
                return [-1e-40, 0]
            else:
                return [-1, 0]
        elif place == 3:
            if self.i_in() <= 1e-20 or self.i_out() <= 1e-20:
                return [-self.r/2, -1]
            else:
                return [-self.r/2, -1]


class Potentiometer(Component):
    """
    Model of a variable potentionmeter for regulating the charging current of the battery.
    """
    def __init__(self, name, data):
        """
        creates an object of the class potentionmeter. Regulates the current through it and calculates the needed
        resistance.

        :param name: set a unique name for the <Potentiometer> Object
        :param data: currently empty
        """

        # Data relating to the resistor
        self.i = 40

        # Unknowns
        self.i_out = Variable("i_out", "A")
        self.i_in = Variable("i_in", "A")
        self.u_out = Variable("u_out", "V")
        self.u_in = Variable("u_in", "V")

        # Data relating to the use of the resistor
        self.power_loss = Variable("power_loss", "W", val_min=0)
        self.resistance = Variable("resistance", "Ohm")

        super().__init__(4, 2, name, unknowns=[self.u_in, self.i_in, self.u_out, self.i_out],
                         newton_function_factor=1,
                         logging_variables=[self.power_loss, self.resistance],
                         func_post_values=[self.calculate_power_loss])

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def pass_initial_condition(self):
        self.u_in.val = self.u_out()

    def calculate_efficiency(self):
        if self.power_out() == 0 or self.power_in() == 0:
            return 1
        else:
            return self.power_out() / self.power_in()

    def calculate_power_loss(self):
        if self.i_in() == 0:
            self.resistance.val = 0
            self.power_loss.val = 0
        else:
            self.resistance.val = abs(self.u_out() - self.u_in()) / self.i_in()
            self.power_loss.val = self.i_in() ** 2 * self.resistance()

    def f(self):
        return [self.i_in() - self.i, self.i_in() - self.i_out()]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0, 0]
        if place == 0:
            return [0, 0]
        elif place == 1:
            return [1, 1]
        elif place == 2:
            return [0, 0]
        elif place == 3:
            return [0, -1]