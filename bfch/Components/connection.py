"""
Created on 31.01.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
from typing import Tuple
from .component import Component
from ..Utilities.variable import Variable


class Connection(Component):
    """
    Objects of this class contain the information about which components are connected to each other
    Like nodes in a circuit. It is component like, but needs to be variable regarding the number of inputs
    and outputs and therefore functions and derivatives. On the other side, no useful information that would need
    saving is generated here.
    The connections are created automatically by the circuit
    """

    interface_type = Tuple[Component, ...]
    input_objs = Tuple[Component, ...]
    output_objs = Tuple[Component, ...]

    def __init__(self, input_objs: interface_type, output_objs: interface_type):
        """
        .. warning::
            DOES NOT CALL super.__init__

        :param input_objs: tuple of Component objects, the input objects
        :param output_objs: tuple of Component objects, the output objects
        """
        self.input_objs = input_objs
        self.output_objs = output_objs
        self._u_in = []
        self._i_in = []
        self._u_out = []
        self._i_out = []
        self.order = 0
        self.calculate_number_eq()

    def calculate_efficiency(self):
        return 0

    def calculate_number_eq(self):
        """
        calculates the number of equations that the specific object provides, needed to check, whether
        a set of equation is solvable or not
        """
        self.num_eq = len(self.output_objs) + len(self.input_objs)

    def pass_initial_condition(self):
        """
        if a value of an input voltage or output voltage variable is not 1 => it needs to be passed on
        """
        v_pass = 0
        for input_obj in self.input_objs:
            if input_obj.u_out() != 1:
                v_pass = input_obj.u_out()
        if v_pass == 0:
            for output_obj in self.output_objs:
                if output_obj.u_in() != 1:
                    v_pass = output_obj.u_in()
        if v_pass != 0:
            for input_obj in self.input_objs:
                input_obj.u_out.val = v_pass
            for output_obj in self.output_objs:
                output_obj.u_in.val = v_pass

    def f(self):
        result_raw = [self.f_voltage(), self.f_current()]
        result = [item for sublist in result_raw for item in sublist]
        return result

    def df(self, d_unknown: Variable):
        result = [0] * self.num_eq
        # derivation f_voltage
        # look at input objects
        try:
            place = self.u_in.index(d_unknown)
            if place == 0:
                result[:-1] = [1] * (self.num_eq - 1)
            else:
                result[place - 1] = -1
        except ValueError:
            pass
        # look at output objects
        try:
            place = self.u_out.index(d_unknown)
            result[place + len(self.u_in) - 1] = -1
        except ValueError:
            pass
        # derivation f_current
        try:
            place = self.i_in.index(d_unknown)
            result[-1] = -1
        except ValueError:
            pass
        try:
            place = self.i_out.index(d_unknown)
            result[-1] = 1
        except ValueError:
            pass
        return result

    def f_current(self):
        """
        sum over the input currents must equal the sum of the output currents
        """
        self._i_in = []
        self._i_out = []
        for input_object in self.input_objs:
            self.i_in = input_object.i_out

        for output_object in self.output_objs:
            self.i_out = output_object.i_in

        sum_current_in = 0
        sum_current_out = 0
        for i in self.i_in:
            sum_current_in = sum_current_in + i()

        for i in self.i_out:
            sum_current_out = sum_current_out + i()

        return [sum_current_out - sum_current_in]

    def f_voltage(self):
        """
        input voltages must be the same as the output voltages
        """
        self._u_in = []
        self._u_out = []
        for input_object in self.input_objs:
            self.u_in = input_object.u_out

        for output_object in self.output_objs:
            self.u_out = output_object.u_in

        result = []

        # take first input object and compare it to all other input objects
        for j in range(1, len(self._u_in)):
            result.append(self._u_in[0]() - self._u_in[j]())

        # take first input object and compare it to all the output objects
        for j in range(len(self._u_out)):
            result.append(self._u_in[0]() - self._u_out[j]())

        return result

# -------------------------------------------------------------------------------------------------------------------- #
    # PROPERTIES AND SETTERS
# -------------------------------------------------------------------------------------------------------------------- #
    @property
    def i_out(self):
        return self._i_out[:]

    @i_out.setter
    def i_out(self, value):
        self._i_out.append(value)

    @property
    def i_in(self):
        return self._i_in[:]

    @i_in.setter
    def i_in(self, value):
        self._i_in.append(value)

    @property
    def u_out(self):
        return self._u_out[:]

    @u_out.setter
    def u_out(self, value):
        self._u_out.append(value)

    @property
    def u_in(self):
        return self._u_in[:]

    @u_in.setter
    def u_in(self, value):
        self._u_in.append(value)
