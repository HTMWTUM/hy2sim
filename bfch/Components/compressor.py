"""
Created on 16.04.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
from .component import Component
from ..Utilities.variable import Variable
import numpy as np


class Compressor1(Component):
    """
    constant output pressure compressor based on polytropic compression process
    """
    def __init__(self, name, data):
        """
        constant output pressure compressor based on polytropic compression process, has two unknowns and one equation

        :param name: set a unique name for the FcModel Object
        :param data: dictionary containing all the needed information of the compressor,
                     it must contain: "pol_efficiency" and "e_motor_efficiency"
        """
        # data of the compressor
        self.pol_efficiency = data["pol_efficiency"]
        self.e_motor_efficiency = data["e_motor_efficiency"]

        # Unknowns
        self.i_in = Variable("i_in", "A")
        self.u_in = Variable("u_in", "V")

        # state of the compressor
        self.p_electric_motor = Variable("p_electric_motor", "W")
        self.pressure_ratio = Variable("pressure_coeff", "1")

        # operating conditions
        self.ambient_pressure = Variable("ambient_pressure", "Pa")
        self.pressure_cathode = Variable("cath_p", "Pa")
        self.ambient_temperature = Variable("ambient_temp", "K")
        self.spec_heat_cap = Variable("spec_heat_cap", "J/(kg*K)")
        self.isentropic_exp = Variable("isentropic_exp", "1")
        self.massflow_cath = Variable("cath_massflow", "kg/s")

        super().__init__(2, 1, name, unknowns=[self.u_in, self.i_in], logging_variables=[self.pressure_ratio],
                         op_conditions=[self.ambient_pressure, self.pressure_cathode, self.spec_heat_cap,
                                        self.isentropic_exp, self.ambient_temperature, self.massflow_cath])

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def calculate_efficiency(self):
        return self.e_motor_efficiency

    def calculate_segment_pre_values(self):
        """
        calculation of the power needed by the compressor at every time step of the segment
        """
        # pressure ratio between exterior and cathode
        self.pressure_ratio.overwrite(np.divide(self.pressure_cathode.return_val(), self.ambient_pressure.return_val()))
        # total temperature at the cathode
        t_tot_cath = np.multiply(np.power(self.pressure_ratio.return_val(),
                                          np.divide(np.subtract(self.isentropic_exp.return_val(), 1),
                                                    np.multiply(self.isentropic_exp.return_val(), self.pol_efficiency))),
                                 self.ambient_temperature.return_val())
        # total temperature delta, exterior T = Total temperature, assumption: c = 0 m/s
        delta_t_tot = np.subtract(t_tot_cath, self.ambient_temperature.return_val())
        # multiply cp, delta_t_tot, efficiency, mass flow to get power
        self.p_electric_motor.overwrite(np.multiply(np.multiply(self.spec_heat_cap.return_val(), delta_t_tot),
                                                    np.multiply(self.massflow_cath.return_val(), self.e_motor_efficiency)))
        # todo: shouldnt the massflow be DIVIDED by the emotor efficiency?

    def f(self):
        return [(self.p_electric_motor() - (self.i_in()) * self.u_in())]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0]
        if place == 0:
            return [-self.i_in()]
        elif place == 1:
            return [-self.u_in()]
