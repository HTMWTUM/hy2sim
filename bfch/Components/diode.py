"""
Created on 13.12.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:
    
"""
from .component import Component
from ..Utilities.variable import Variable
import numpy as np


class Diode(Component):
    """
    Model of the diode based on shockley model
    """
    def __init__(self, name, data):
        """
        :param name: set a unique name for the Diode Object
        :param data: dictionary containing all the needed information of the Diode model, "n", "i_s", "solving_param"
        """
        self.model = data['model']  # type of diode model

        # Constants etc.
        self.k = 1.38064852e-23  # Boltzmann constant, J/K
        self.q = 1.602176634e-19  # elementary charge, As

        # Data relating to the diode
        self.n = data['n']  # emission coefficient between 1 and 2, dimensionless
        self.i_s = data['i_s']  # reverse leakage current between 1e-12 and 1e-6, A
        self.solving_param = data['solving_param']

        # Unknowns
        self.i_out = Variable("i_out", "A", val_max=10000, val_min=self.solving_param)
        self.i_in = Variable("i_in", "A", val_max=10000, val_min=self.solving_param)
        self.u_out = Variable("u_out", "V", val_max=10000)
        self.u_in = Variable("u_in", "V", val_max=10000)

        # Operating conditions of the Diode
        self.ambient_temp = Variable('ambient_temp', "K")  # ambient temperature, K

        super().__init__(4, 2, name, newton_z_step_factor=1, newton_function_factor=1,
                         unknowns=[self.u_in, self.i_in, self.u_out, self.i_out],
                         op_conditions=[self.ambient_temp], comp_type="diode")

# -------------------------------------------------------------------------------------------------------------------- #
    # FUNCTIONS USED FOR THE DIODE MODEL
# -------------------------------------------------------------------------------------------------------------------- #
    def diode_current(self, u_in, u_out, i_out):
        """
        calculate the current through the diode. shockley new is the inverted form of the shockley equation. Through the
        use of the logarithm it is easier to solve

        :param i_out: current through the diode
        :param u_in: voltage level at the input
        :param u_out:  voltage level at the output

        :return: current, A
        """
        if self.model == 'shockley_exp':  # typical notation of the shockley equation
            return i_out - self.shockley(u_in-u_out, self.i_s, self.n)
        elif self.model == 'ideal':
            if u_in >= u_out:
                return u_in - u_out
            else:
                return i_out - 0
        elif self.model == 'shockley_log_approx':
            u_t = (self.k * self.ambient_temp()) / self.q  # thermal voltage
            if i_out <= 0:
                return self.solving_param  # 0 when using the unregulated circuit
            else:
                return u_in - u_out - self.n * u_t * (np.log(i_out / self.i_s))
        elif self.model == 'shockley_log':
            u_t = (self.k * self.ambient_temp()) / self.q  # thermal voltage
            if i_out <= -self.i_s:
                return self.solving_param  # 0 when using the unregulated circuit
            else:
                return u_in - u_out - self.n * u_t * (np.log((i_out / self.i_s) + 1))

    def shockley(self, u, i_s, n):
        """
        model of the diode

        :param u: voltage drop over the diode, V
        :param i_s:  reverse leakage current between 1e-12 and 1e-6, A
        :param n: emission coefficient

        :return: current through the diode, A
        """
        u_t = (self.k * self.ambient_temp()) / self.q
        return i_s * (np.exp(u / (n * u_t)) - 1)

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def pass_initial_condition(self):
        self.u_in.val = self.u_out()

    def calculate_efficiency(self):
        if self.power_in() <= 0 or self.power_out() == 0:
            return 1
        return self.power_out() / self.power_in()

    def f(self):
        return [self.diode_current(self.u_in(), self.u_out(), self.i_out()), (self.i_in() - self.i_out())]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0, 0]
        if place == 0:
            return self.df_duin()
        elif place == 1:
            return self.df_diin()
        elif place == 2:
            return self.df_duout()
        elif place == 3:
            return self.df_diout()

    # additional functions to keep derivatives clean
    def df_duin(self):
        if self.model == 'ideal':
            if self.u_in() >= self.u_out():
                return [1, 0]
            else:
                return [0, 0]
        elif self.model == 'shockley_exp':
            u_t = (self.k * self.ambient_temp()) / self.q
            return [-self.i_s / (self.n * u_t) * np.exp((self.u_out() - self.u_in()) / (self.n * u_t)), 0]
        elif self.model == 'shockley_log_approx':
            if self.i_out() <= 0:
                return [1e-40, 0]
            else:
                return [1, 0]
        elif self.model == 'shockley_log':
            if self.i_out() <= -self.i_s:
                return [1e-40, 0]
            else:
                return [1, 0]

    def df_diin(self):
        if self.model == 'ideal':
            if self.u_in() >= self.u_out():
                return [0, 1]
            else:
                return [1, 1]
        elif self.model == 'shockley_exp':
            return [0, 1]
        elif self.model == 'shockley_log_approx':
            return [0, 1]
        elif self.model == 'shockley_log':
            return [0, 1]

    def df_duout(self):
        if self.model == 'ideal':
            if self.u_in() >= self.u_out():
                return [-1, 0]
            else:
                return [0, 0]
        elif self.model == 'shockley_exp':
            u_t = (self.k * self.ambient_temp()) / self.q
            return [self.i_s / (self.n * u_t) * np.exp((self.u_out() - self.u_in()) / (self.n * u_t)), 0]
        elif self.model == 'shockley_log_approx':
            if self.i_out() <= 0:
                return [-1e-40, 0]
            else:
                return [-1, 0]
        elif self.model == 'shockley_log':
            if self.i_out() <= -self.i_s:
                return [-1e-40, 0]
            else:
                return [-1, 0]

    def df_diout(self):
        u_t = (self.k * self.ambient_temp()) / self.q
        if self.model == 'shockley_exp':
            return [0, -1]
        elif self.model == 'shockley_log_approx':
            if self.i_out() <= 0:
                dv = -1e-40
            else:
                dv = - self.n * u_t / self.i_out()
            return [dv, -1]
        elif self.model == 'shockley_log':
            if self.i_out() <= -self.i_s:
                dv = -1e-40
            else:
                dv = - (self.n * u_t * self.i_out() * self.i_s) / (self.i_out() + self.i_s)
            return [dv, -1]
