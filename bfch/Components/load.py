"""
Created on 14.11.2021
Author: Tancrède Oswald

File Version: 0.00

File Description:
    
"""
from .component import Component
from ..Utilities.variable import Variable
import numpy as np


class PowerSink(Component):
    """
    Model of a simple power sink. effectively transforming a power into a voltage and a current
    """
    def __init__(self, name, data=0):
        """
        creates an object that mimics a load. Draws the power "power". The model has two unknowns and one equation.

        :param name: set a unique name for the PowerSink Object
        :param data: currently, empty
        """
        # operating condition of the object
        self.p_mission = Variable("power", "W")
        self.system_output = True
        self.mass = 0.
        self.volume = 0.

        # unknowns
        self.i_in = Variable("i_in", "A", val_min=1e-6)
        self.u_in = Variable("u_in", "V")

        super().__init__(2, 1, name, newton_z_step_factor=1, unknowns=[self.u_in, self.i_in],
                         newton_function_factor=1,
                         op_conditions=[self.p_mission], func_post_values=[self.calculate_output_power])

# -------------------------------------------------------------------------------------------------------------------- #
    def current_power(self):
        return self.p_mission()

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def f(self):
        if self.i_in() == 0:
            self.i_in.val = 1
        return [(self.p_mission() / self.i_in()) - self.u_in()]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0]
        if place == 0:
            return [-1]
        elif place == 1:
            return [-self.p_mission() / (self.i_in() ** 2)]

    def calculate_efficiency(self):
        return 1

    def calculate_mass(self):
        return self.mass  # todo: Update!

    def calculate_volume(self):
        return self.volume  # todo: Update!

    def calculate_rated_power(self):
        return 0.  # todo: Update!

    def scale_rated_power(self, scaling_factor):
        pass

    def calculate_output_power(self):
        self.power_out.val = self.power_in()

    def set_initial_condition(self):
        self.u_in.val = np.sqrt(self.p_mission())
        self.i_in.val = self.u_in()


class BLDC(Component):
    """
    Model of a brushless direct current motor
    """
    def __init__(self, name, data):
        """
        model of a Brushless DC motor (BLDC). The behavior is very similar to a dc motor with permanent magnet for the
        field coil.

        :param name: set a unique name for the BLDC Object
        :param data: dictionary containing all the needed information of the bldc model: "km", "krlin", "J", "L", "R"
        """

        # Data relating to the BLDC motor
        self.km = data['km']  # Engine constant, Nm/A
        self.krlin = data['krlin']  # viscous friction coefficient, kgm^2/s
        self.J = data['J']  # moment of inertia, kgm^2
        self.L = data['L']  # inductivity, H
        self.R = data['R']  # resistivity, Ohm

        # Unknowns
        self.i_in = Variable("i_in", "A")
        self.u_in = Variable("u_in", "V")

        # Logged Variables
        self.p_curve = Variable("p_curve", "W")  # power of the motor, W
        self.power = Variable("power", "W")  # power of the motor, W
        self.p_it = Variable("p_it", "W")  # power of the motor during

        # Operating conditions
        self.rot_speed = Variable("rot_speed", "Hz", val_min=0)  # rotational frequency, 1/s
        self.rot_torque = Variable("rot_torque", "Nm", val_min=0)  # moment at the rotor, Nm

        super().__init__(2, 1, name, unknowns=[self.u_in, self.i_in],
                         logging_variables=[self.p_curve, self.p_it, self.power],
                         op_conditions=[self.rot_speed, self.rot_torque])

# -------------------------------------------------------------------------------------------------------------------- #
    # CALCULATION OF THE CURRENT, VOLTAGES AND THE POWER
# -------------------------------------------------------------------------------------------------------------------- #
    def current_power(self):
        """
        :return: the current power drawn by the BLDC
        """
        self.i_in.val = self.calc_current()
        return self.i_in() * self.calc_voltage(self.i_in())

    def update_p_it(self):
        """
        updates the value of the power for the current iteration
        """
        self.p_it.val = self.u_in() * self.i_in()

    def current_voltage(self):
        """
        :return: returns the voltage at the current time step
        """
        self.i_in.val = self.calc_current()
        return self.calc_voltage(self.i_in())

    def calc_current(self):
        """
        :return: the current of the BLDC based on a DC Model, A
        """
        return self.current_dc_model(self.rot_speed(), self.rot_speed.prev_val, self.krlin, self.J, self.rot_torque(), self.km,
                                     self.t[self.t_place], self.t[self.t_place - 1])

    def calc_voltage(self, i_in):
        """
        :param i_in: current through the BLDC, A

        :return: the voltage at the BLDC, V
        """
        return self.voltage_dc_model(self.rot_speed(), i_in, self.i_in.prev_val, self.t[self.t_place],
                                     self.t[self.t_place - 1], self.L, self.R, self.km)

    def voltage_dc_model(self, omega, i1, i0, t1, t0, L, R, km):
        """
        calculates the voltage of the dc motor model of the bldc, from 'Entwurf und Simulation der Regelung des neuen
        integrierten Aktuators' by 'Christian Prattes' p.13 et sqq.

        :param omega: rotational frequency of the rotor, Hz
        :param i1: current through the BLDC, A
        :param i0: previous current through the BLDC, A
        :param t1: time, s
        :param t0: time of the previous step, s
        :param L: inductivity, H
        :param R: resistivity, Ohm
        :param km: machine constant, Nm/A

        :return: voltage of the dc motor
        """
        delta_i = i1 - i0
        delta_t = t1 - t0
        if t1 == 0 or delta_t == 0:
            return R * i1 + km * omega
        else:
            return L * (delta_i / delta_t) + R * i1 + km * omega

    def current_dc_model(self, omega, omega_prev, krlin, J, m_l, km, t1, t0):
        """
        calculates the current of the dc motor model of the bldc, from 'Entwurf und Simulation der Regelung des neuen
        integrierten Aktuators' by 'Christian Prattes' p.13 et sqq.

        :param omega: rotational frequency of the rotor, Hz
        :param omega_prev: previous rotational frequency of the rotor, Hz
        :param krlin: viscous friction coefficient, kgm^2/s
        :param m_l: torque at the rotor, Nm
        :param km: machine constant, Nm/A
        :param t1: time, s
        :param t0: time of the previous step, s

        :return: current through the motor, A
        """
        delta_omega = omega - omega_prev
        delta_t = t1 - t0
        if t1 == 0 or delta_t == 0:
            return (krlin * omega + m_l) / km
        else:
            return (krlin * omega - J * (delta_omega / delta_t) + m_l) / km

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def f(self):
        self.update_p_it()
        return [self.calc_voltage(self.i_in()) - self.u_in()]

    def df(self, d_unknown):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0]
        if place == 0:
            return [-1]
        elif place == 1:
            h = 1e-5
            return [(self.calc_voltage(self.i_in() + h) - self.calc_voltage(self.i_in() - h)) / (2 * h)]

    def pass_initial_condition(self):
        self.u_in.val = self.current_voltage()

    def calculate_efficiency(self):
        return self.rot_torque() * self.rot_speed() / (self.i_in() * self.u_in())


class BleedAirSystem(Component):
    """
    Model of a compressor for cabin pressurization.
    """
    def __init__(self, name, data):
        """
        creates an object that models a compressor.
        The model has two unknowns and one equation.
        The electrical power needed for the compressor is calculated based on the altitude, the chosen cabin altitude/
        pressure and the airflow.

        :param name: set a unique name for the PowerSink Object
        :param data: currently, empty
        """
        # operating condition of the object
        self.bleed_air = Variable("bleed_air", "kg/s")
        self.altitude = Variable("altitude", "m")

        # logging variables of the object
        self.pressure_ratio = Variable("pressure_coeff", "1")
        self.ambient_pressure = Variable("ambient_pressure", "Pa")
        self.ambient_temperature = Variable("ambient_temperature", "K")
        self.isentropic_exp = Variable("isentropic_exponent", "1")
        self.p_electric_motor = Variable("p_electric_motor", "W")
        self.spec_heat_cap = Variable("spec_heat_cap", "J/(kg*K)")

        # Information about the component
        self.system_output = True
        self.mass = data['mass']  # in [kg]
        self.volume = data['volume']  # in [l]
        self.massflow_rated = data['massflow_rated']  # in [kg/s]
        self.massflow_max = data['massflow_max']  # in [kg/s]
        self.pol_efficiency = data["pol_efficiency"]  # in [dimensionless]
        self.e_motor_efficiency = data["e_motor_efficiency"]  # [dimensionless]
        self.constant_isentropic_exp = data["constant_isentropic_exp"]  # [dimensionless]
        self.constant_heat_cap = data["constant_heat_cap"]  # [J/(kg*K)]
        self.output_air_temp = data['output_air_temperature']  # [K] as a conservative estimate, take standard condition
        self.output_air_pressure = data['output_air_pressure']  # [Pa]

        # unknowns
        self.i_in = Variable("i_in", "A", val_min=1e-6)
        self.u_in = Variable("u_in", "V")

        super().__init__(2, 1, name, newton_z_step_factor=1,
                         unknowns=[self.u_in, self.i_in],
                         newton_function_factor=1,
                         op_conditions=[self.bleed_air, self.altitude],
                         func_post_values=[self.calculate_output_power],
                         logging_variables=[self.ambient_pressure, self.spec_heat_cap, self.pressure_ratio,
                                            self.p_electric_motor, self.ambient_temperature,
                                            self.isentropic_exp, ]
                         )
        self.comp_type = "bleedAirSystem"

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def f(self):
        if self.i_in() == 0:
            self.i_in.val = 1
        return [(self.p_electric_motor() / self.i_in()) - self.u_in()]

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0]
        if place == 0:
            return [-1]
        elif place == 1:
            return [-self.p_electric_motor() / (self.i_in() ** 2)]

    def calculate_efficiency(self):
        return 1

    def calculate_efficiency_design(self):
        """
        design point is currently set at 10000
        """
        ambient_pressure = 101325 * np.power(1 - (0.0225577 * 10000) / 1000, 5.25588)
        ambient_temperature = 288.15 - 6.5 * 10
        pressure_ratio = self.output_air_pressure / ambient_pressure
        t_total_output = np.power(pressure_ratio,
                                (self.constant_isentropic_exp - 1) /
                                (self.constant_isentropic_exp * self.pol_efficiency)) \
                        * ambient_temperature
        delta_t_total = t_total_output - ambient_temperature
        try:
            eff = self.e_motor_efficiency / (self.spec_heat_cap() * delta_t_total)
        except ZeroDivisionError:
            eff = 1
        return eff

    def calculate_mass(self):
        return self.mass  # todo: Update!

    def calculate_volume(self):
        return self.volume  # todo: Update!

    def calculate_rated_power(self):
        return 0.  # todo: Update!

    def scale_rated_power(self, scaling_factor):
        pass

    def calculate_output_power(self):
        self.power_out.val = self.power_in()

    def set_initial_condition(self):
        self.u_in.val = np.sqrt(self.p_electric_motor())
        self.i_in.val = self.u_in()

    def calculate_segment_pre_values(self):
        """
        calculation of the power needed by the compressor at every time step of the segment
        """
        # calculate ambient temperature and pressure with standard ISA atmosphere
        # todo: differentiate between <11km and >11km * 22632*exp((11000-h)/6341.62)
        self.ambient_pressure.overwrite(np.multiply(101325,
                                                    np.power(
                                                        np.subtract(1,
                                                                    np.divide(
                                                                        np.multiply(0.0225577, self.altitude.return_val()),
                                                                        1000)
                                                                    ),
                                                        5.25588)
                                                    )
                                        )

        self.ambient_temperature.overwrite(np.subtract(288.15,
                                                       np.multiply(6.5,
                                                                   np.divide(self.altitude.return_val(), 1000)
                                                                   )
                                                       )
                                           )

        # isentropic exponent constant 1.4  todo: maybe as a variable of the inlet conditions in the future
        self.isentropic_exp.overwrite_constant(self.constant_isentropic_exp)
        self.spec_heat_cap.overwrite_constant(self.constant_heat_cap)
        # pressure ratio between exterior and the compressor output
        self.pressure_ratio.overwrite(np.divide(self.output_air_pressure, self.ambient_pressure.return_val()))
        # total temperature at the output
        t_tot_output = np.multiply(np.power(self.pressure_ratio.return_val(),
                                            np.divide(np.subtract(self.isentropic_exp.return_val(), 1),
                                                      np.multiply(self.isentropic_exp.return_val(), self.pol_efficiency)
                                                      )
                                            ),
                                   self.ambient_temperature.return_val())
        # total temperature delta, exterior T = Total temperature, assumption: c = 0 m/s
        delta_t_tot = np.subtract(t_tot_output, self.ambient_temperature.return_val())
        # multiply cp, delta_t_tot, efficiency, mass flow to get power
        self.p_electric_motor.overwrite(np.multiply(np.multiply(self.spec_heat_cap.return_val(), delta_t_tot),
                                                    np.divide(self.bleed_air.return_val(), self.e_motor_efficiency)))


class HydraulicSystem(Component):
    """
    ::warning:: this class is currently completely useless
    """
    def __init__(self, name, data=0):
        """
        creates an object that mimics a load. Draws the power "power". The model has two unknowns and one equation.

        :param name: set a unique name for the PowerSink Object
        :param data: currently, empty
        """
        # operating condition of the object
        self.p_mission = Variable("power", "W")
        self.system_output = True
        self.mass = 0.
        self.volume = 0.

        # unknowns
        self.i_in = Variable("i_in", "A", val_min=1e-6)
        self.u_in = Variable("u_in", "V")

        super().__init__(2, 1, name, newton_z_step_factor=1, unknowns=[self.u_in, self.i_in],
                         newton_function_factor=1,
                         op_conditions=[self.p_mission], func_post_values=[self.calculate_output_power])

# -------------------------------------------------------------------------------------------------------------------- #
    def current_power(self):
        return self.p_mission()

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def f(self):
        pass

    def df(self, d_unknown: Variable):
        try:
            place = self.unknowns.index(d_unknown)
        except ValueError:
            return [0]
        if place == 0:
            return [-1]
        elif place == 1:
            return [-self.p_mission() / (self.i_in() ** 2)]

    def calculate_efficiency(self):
        return 1

    def calculate_mass(self):
        return self.mass  # todo: Update!

    def calculate_volume(self):
        return self.volume  # todo: Update!

    def calculate_rated_power(self):
        return 0.  # todo: Update!

    def scale_rated_power(self, scaling_factor):
        pass

    def calculate_output_power(self):
        self.power_out.val = self.power_in()

    def set_initial_condition(self):
        self.u_in.val = np.sqrt(self.p_mission())
        self.i_in.val = self.u_in()
