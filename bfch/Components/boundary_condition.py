"""
Created on 03.02.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:

"""
from .component import Component
from ..Utilities.variable import Variable


class BoundaryCondition(Component):
    """
    Component, that is not really a component, but since it fits very well into the solving method the
    Boundary conditions of the problem are modeled here. The set boundary condition is fulfilled, when f is zero
    """

    def __init__(self, variable: Variable, value: float, name="bc", second_variable=None, f_lam=None, df_lam=None):
        """
        if second_variable is set as a Variable, the quotient of the to values needs to equal the value of the boundary
        condition. This is used in case the power sharing has to be regulated to a fixed value

        :param variable: Variable Object that must fit a condition
        :param value: value of the condition
        :param name: name of this object
        """
        self.type = "bc"  # todo: type=name
        if second_variable is not None:
            self.type = "power_share_control"
            self.var1 = variable
            self.var2 = second_variable
            self.value = value
        if second_variable and f_lam and df_lam:
            self.type = "function"
            self.var1 = variable
            self.var2 = second_variable
            self.f_lam = f_lam
            self.df_lam = df_lam
            self.value = value
        else:
            self.fixed_unknown = variable
            variable.val = value
            self.value = value
        super().__init__(0, 1, name)

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def f(self):
        if self.type == "bc":
            return [self.fixed_unknown() - self.value]
        elif self.type == "power_share_control":
            if self.var2() == 0:
                return [1]
            else:
                return [(self.var1() / self.var2()) - self.value]
        elif self.type == "function":
                return [self.f_lam(self.var1(), self.var2()) - self.value]

    def df(self, d_unknown: Variable):
        if self.type == "bc":
            if d_unknown == self.fixed_unknown:
                return [1]
            else:
                return [0]
        elif self.type == "power_share_control":
            if d_unknown == self.var1:
                return [1 / self.var2()]
            elif d_unknown == self.var2:
                return [- 1 / (self.var2() ** 2)]
            else:
                return [0]
        elif self.type == "function":
            if d_unknown == self.var1:
                return [self.df_lam[0](self.var1(), self.var2())]
            elif d_unknown == self.var2:
                return [self.df_lam[1](self.var1(), self.var2())]
            else:
                return [0]

    def calculate_efficiency(self):
        return 0
