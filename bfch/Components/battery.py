"""
Created on 12.12.2021
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 1.00

File Description:
stores the data related to the li Ion battery
includes the different models that may be used
    
"""
import numpy as np
from .component import Component
from ..Utilities.variable import Variable


def shepherd(i, soc, es, k, a, b, r, q=0):
    """
    calculation of the cell voltage depending on SOC and I
    can be used to fit the parameters

    :param b: fitting parameter, 1/Ah
    :param r: resistance, Ohm
    :param a: reference over voltage, V
    :param k: fitting parameter, Ohm
    :param es: reference voltage, V
    :param i: current through a cell, A
    :param soc: state of charge of the battery (equivalent to cell state of charge), dimensionless

    :return: open circuit voltage, V
    """
    if soc <= 0 or np.isnan(soc):
        return 0
    return es - (k / soc) * i + a * np.exp(-b * (1 - soc)) - r * i - q * (1 - soc)


def shepherd_charging(i, soc, es, k, a, b, r, q=0):
    """
    calculation of the cell voltage depending on SOC and I
    can be used to fit the parameters

    :param r: resistance, Ohm
    :param b: fitting parameter, 1/Ah
    :param a: reference over voltage, V
    :param k: fitting parameter, Ohm
    :param es: reference voltage, V
    :param i: current through a cell, A
    :param soc: state of charge of the battery (equivalent to cell state of charge), dimensionless

    :return: open circuit voltage, V
    """
    if soc <= 0 or np.isnan(soc):
        return es - (k / 0.00001) * i + a * np.exp(-b * (1 - soc)) + r * i - q * (1 - soc)
    return es - (k / soc) * i + a * np.exp(-b * (1 - soc)) + r * i - q * (1 - soc)


def shepherd_i_density(i, soc, es, k, a, b, r, q=0):
    """
    calculation of the cell voltage depending on SOC and I
    can be used to fit the parameters

    :param r: ara resistance, Ohm * cm^2
    :param b: fitting parameter, 1/Ah
    :param a: reference over voltage, V
    :param k: fitting parameter, Ohm * cm^2
    :param es: reference voltage, V
    :param i: current density through a cell, A / cm^2
    :param soc: state of charge of the battery (equivalent to cell state of charge), dimensionless

    :return: open circuit voltage, V
    """
    return es - (k / soc) * i + a * np.exp(-b*(1 - soc)) - r * i - q * (1 - soc)


def shepherd_i_density_charging(i, soc, es, k, a, b, r):
    """
    calculation of the cell voltage depending on SOC and I
    can be used to fit the parameters

    :param r: ara resistance, Ohm * cm^2
    :param b: fitting parameter, 1/Ah
    :param a: reference over voltage, V
    :param k: fitting parameter, Ohm * cm^2
    :param es: reference voltage, V
    :param i: current density through a cell, A / cm^2
    :param soc: state of charge of the battery (equivalent to cell state of charge), dimensionless

    :return: open circuit voltage, V
    """
    return es - (k / soc) * i + a * np.exp(-b(1 - soc)) + r * i


class LiIon(Component):
    """
    Model of the Li Ion battery based on the shepherd model
    """
    def __init__(self, name, data, soc_start=1, temp_start=290):
        """
        initialization of an object of the class LiIon. If the battery is connected to a regulated charging circuit it
        has 2 unknowns and 1 equation. If it is not regulated, there are 4 unknowns and 2 equations. Logged variables
        are soc, temperature, efficiency, current, voltage and power.

        :param name: set a unique name for the LiIon Object
        :param data: dictionary containing all the needed information of the battery
        :param soc_start: SOC of the battery at the start of the mission
        :param temp_start: temperature of the battery at the start of the mission
        """
        # Data relating to the battery itself
        self.model = 'shepherd'
        self.C_cell_Ah = data['C_cell_Ah']  # nominal capacity of the cell, Ah
        self.eta_discharge = data['eta_discharge']  # efficiency of the discharging process, dimensionless
        self.eta_charge = data['eta_charge']  # efficiency of the charging process, dimensionless
        self.es = data['es']  # reference open circuit voltage, V
        self.k = data['k']  # fitting parameter, Ohm
        self.q = data['q']  # linear fitting parameter for (dis)charge curve [V/(C_delta/C_bat_As) = V/SoC]
        self.r = data['r']  # resistance, Ohm
        self.a = data['a']  # reference over voltage, V
        self.b = data['b']  # b: fitting parameter, 1/Ah
        self.rs = data['rs']  # internal series resistance, Ohm
        self.rct = data['rct']  # internal charge transfer resistance, Ohm
        self.series_cells = data['series_cells']  # number of cells in series, dimensionless
        self.parallel_cells = data['parallel_cells']  # number of parallel cells, dimensionless
        self.C_bat_As = self.C_cell_Ah * 3600 * self.parallel_cells  # nominal capacity of the battery, As
        self.I_ref = data['I_ref']  # reference current for the soc calculation, A
        self.T_ref = data['T_ref']  # reference temperature for the soc calculation, K
        self.max_charging_current = data['max_charging_c_rate_bat'] * self.C_bat_As / 3600  # maximum charging current, A
        self.max_discharging_current = data['max_discharging_c_rate_bat'] * self.C_bat_As / 3600  # maximum discharging current, A
        self.A = data['A']  # electrode area of a cell

        self.C_therm_discharge = data['C_therm_discharge']  # thermal capacity of the battery charging, J/K
        self.R_therm_discharge = data['R_therm_discharge']  # thermal resistance of the battery discharging, K/W
        self.C_therm_charge = data['C_therm_charge']  # thermal capacity of the battery charging, J/K
        self.R_therm_charge = data['R_therm_charge']  # thermal resistance of the battery discharging, K/W
        self.m = data['m']  # mass of the battery, kg

        # Unknowns
        self.i_out = Variable("i_out", "A", val_min=-1e-5, val_max=self.max_discharging_current)
        self.i_in = Variable("i_in", "A", val_min=-1e-5, val_max=self.max_charging_current)
        self.u_out = Variable("u_out", "V", val_min=0)
        self.u_in = Variable("u_in", "V", val_min=0)

        # Logged variables
        self.soc = Variable("soc", "1")  # state of charge, dimensionless
        self.soc_start = soc_start  # fill in the start value of the soc
        self.temp = Variable("temp", "K")  # temperature of the battery, K
        self.temp_start = temp_start  # fill in the start value of the battery temperature
        self.current = Variable("current", "A", val_min=-np.inf)
        self.voltage = Variable("voltage", "V",val_min=-np.inf)
        self.voltage_reversible_charging = Variable("voltage_reversible_charging", "V")
        self.voltage_reversible_discharging = Variable("voltage_reversible_discharging", "V")
        self.power_total = Variable("power_total", "W", val_min=-np.inf)


        # Operating Conditions
        self.temp_ambient = Variable("ambient_temp", "K")  # ambient temperature, K

        # State charging discharging
        self.charging = False  # False: discharging, True: charging
        self.idle = False
        self.regulated = False  # the default condition of the battery is an unregulated battery

        super().__init__(4, 2, name, unknowns=[self.u_in, self.i_in, self.u_out, self.i_out],
                         logging_variables=[self.soc, self.temp, self.current, self.voltage, self.power_total,
                                            self.voltage_reversible_charging, self.voltage_reversible_discharging],
                         op_conditions=[self.temp_ambient],
                         func_post_values=[self.update_soc_post, self.update_temp_post,
                                           self.calculate_add_post_values],
                         func_pre_values=[self.update_soc_pre, self.update_temp_pre],
                         newton_function_factor=1,
                         newton_z_step_factor=1)

# -------------------------------------------------------------------------------------------------------------------- #
    # MODEL OF THE BATTERY VOLTAGE
# -------------------------------------------------------------------------------------------------------------------- #
    def return_charging_voltage(self, i_out, model="shepherd", rs=None, rct=None):
        """
        calculation of the current charging voltage of the battery
        model based on the Shepherd Model

        :param rct: charge transfer resistance, Ohm
        :param rs: series resistance, Ohm
        :param model: type of mathematical model of the battery
        :param i_out: current through the battery

        :return: voltage of the battery
        """
        if rs is None:
            rs = self.rs
        if rct is None:
            rct = self.rct
        i = i_out / self.parallel_cells  # current through the series cells, A
        u_cell = 0

        if model == "shepherd":
            u_cell = shepherd_charging(i, self.soc(), self.es, self.k, self.a, self.b, self.r, self.q)
        elif model == "shepherd_i_density":
            i = i / self.A
            u_cell = shepherd_i_density(i, self.soc(), self.es, self.k, self.a, self.b, self.r, self.q)
        ocv_bat = u_cell * self.series_cells
        return ocv_bat - i_out * (rs + rct)

    def return_discharging_voltage(self, i_out, model="shepherd", rs=None, rct=None):
        """
        calculation of the current discharging voltage of the battery
        model based on the Shepherd Model

        :param rct: charge transfer resistance, Ohm
        :param rs: series resistance, Ohm
        :param model: type of mathematical model of the battery
        :param i_out: current through the battery

        :return: voltage of the battery
        """
        if rs is None:
            rs = self.rs
        if rct is None:
            rct = self.rct
        i = i_out/self.parallel_cells  # current through the series cells, A
        u_cell = 0
        if model == "shepherd":
            u_cell = shepherd(i, self.soc(), self.es, self.k, self.a, self.b, self.r, self.q)
        elif model == "shepherd_i_density":
            i = i / self.A
            u_cell = shepherd_i_density(i, self.soc(), self.es, self.k, self.a, self.b, self.r, self.q)
        ocv_bat = u_cell * self.series_cells
        return ocv_bat - i_out * (rs + rct)

# -------------------------------------------------------------------------------------------------------------------- #
    # CALCULATION OF SOC AND TEMPERATURE
# -------------------------------------------------------------------------------------------------------------------- #
    def update_soc_pre(self):
        """
        update the soc and carry the value to the next time step

        :return: nothing
        """
        if self.idle:
            self.soc.val = self.soc.prev_val
        else:
            #  estimation of the current soc
            i = self.i_out.prev_val + self.i_in.prev_val
            if i == 0 and self.t_place != 0:
                self.soc.val = self.soc.prev_val
            else:
                self.soc.val = self.new_soc(True)

    def update_soc_post(self):
        """
        function called after every time step to either copy the soc from the previous time step, if the battery was in
        idle, or compute the new value of the soc
        """
        if self.idle:
            self.soc.val = self.soc.prev_val
        else:
            self.soc.val = self.new_soc()

    def new_soc(self, forecast=False, current_dependent=False, temperature_dependence=False):
        """
        calculates the soc of the battery for the time step, depending on temperature and current
        the calculation is broken down to the cell level
        The underlying idea is that the soc should represent how much energy is still left inside the battery. For any
        given battery the available energy is highly dependent on the temperature of the battery and the current that is
        drawn from the battery. However, it is impossible to know how and under which conditions the battery will be
        drained in all future time steps. These circumstances led to a two part solution. First, given previous use and
        current external conditions, the soc is estimated for the current time step. This leads to more accurate
        calculations within a single time step, since the performance of the battery greatly depends on the soc. Second,
        at the end of the time step the 'real' soc is calculated given the effective use of the battery in the time
        step. The method for estimating and calculating the soc in the discharging case is the following: the current,
        increased by factors accounting for temperature and the intensity of the current and the efficiency, is
        integrated of the time step and divided by a reference capacity at normal conditions. This soc depletion value
        is subtracted from the soc level at the end of the previous time step. This calculation leads to a stronger
        depletion, when the conditions are worse than in the reference case. However, if the conditions are better, a
        smaller amount of soc is depleted than initially assumed.
        The efficiency of the battery is calculated by comparing the open circuit voltage at the current soc with the
        voltage under load at the current soc.

        :param temperature_dependence: True if the soc needs to be dependent on the temperature
        :param current_dependent: True if the soc needs to be dependent on the current
        :param forecast: whether to do a forecast calculation ("True") or the calculation after the current step
        ("False")

        :return: soc
        """
        i0 = (self.i_out.prev_val + self.i_in.prev_val)  # calculate the current at the start of the segment
        i1 = (self.i_out() + self.i_in())  # calculate the current at the end of the segment

        if self.i_in() <= 1e-8:    # if i_in is small, then the battery is discharging
            u_bat_0 = self.u_out.prev_val  # battery output voltage at the start of the segment
            u_bat_1 = self.u_out()  # battery output voltage at the end of the segment
        else:
            u_bat_0 = self.u_in.prev_val  # battery input voltage at the start of the segment
            u_bat_1 = self.u_in()  # battery input voltage at the end of the segment

        if forecast and self.t_place != 0:  # carries over the soc from the end of the previous segment
            self.soc.val = self.soc.prev_val

        # calculate the reversible discharging and charging voltage
        self.voltage_reversible_discharging.val = self.return_discharging_voltage(0, self.model)
        self.voltage_reversible_charging.val = self.return_charging_voltage(0, self.model)

        if forecast:
            # if a forecast for the soc during the calculated segment is made, the current and the voltage at the start
            # of the segment are doubled to accommodate the mean value calculation
            i0 *= 2
            i1 = 0
            u_bat_0 *= 2
            u_bat_1 = 0
        elif self.t_place == 0:
            # in the first segment, when the first soc value is calculated, no start value for the voltage exists, so
            # the end value is doubled.
            u_bat_0 = 0
            u_bat_1 *= 2

        if self.t_place == 0:
            t0 = 0
            soc_pre = self.soc_start  # in the first segment, the previous soc is the set start soc.
        else:
            t0 = self.t[(self.t_place - 1)]
            soc_pre = self.soc.prev_val
        t1 = self.t[self.t_place]

        # Current dependency of the capacity, when high currents are drawn from the battery, the effective capacity of
        # the battery reduces. CAUTION this is not yet performing correctly
        alpha0 = 1
        alpha1 = 1
        if current_dependent:
            alpha0 = 1 + 0.4 * (i0 / self.I_ref - 1) * (self.I_ref / self.C_bat_As)
            alpha1 = 1 + 0.4 * (i1 / self.I_ref - 1) * (self.I_ref / self.C_bat_As)

        # Temperature dependency of the capacity, typically, higher temperatures increase the capacity of the battery
        # CAUTION this is not yet performing correctly
        beta0 = 1
        beta1 = 1
        if temperature_dependence:
            beta0 = 1 - 0.02093 * (self.temp.prev_val - self.T_ref)
            beta1 = 1 - 0.02093 * (self.temp() - self.T_ref)

        u_bat_mean = (u_bat_0 + u_bat_1) / 2  # calculates the mean battery voltage over the segment

        if t1-t0 != 0:
            # discharging case
            if self.i_in.prev_val <= 1e-6:

                # get start and end values for the reversible discharging voltage
                if self.t_place == 0:
                    u_bat_mean_rev_discharging_0 = 0  # no previous value existing 0 => take value at the end of step
                    u_bat_mean_rev_discharging_1 = 2 * self.voltage_reversible_discharging()
                elif forecast:
                    u_bat_mean_rev_discharging_0 = 2 * self.voltage_reversible_discharging.prev_val
                    u_bat_mean_rev_discharging_1 = 0  # no end of step value available yet => take previous value
                else:
                    u_bat_mean_rev_discharging_0 = self.voltage_reversible_discharging.prev_val
                    u_bat_mean_rev_discharging_1 = self.voltage_reversible_discharging()

                # calculate mean over time step of reversible discharging voltage
                u_bat_mean_rev_discharging = (u_bat_mean_rev_discharging_0 + u_bat_mean_rev_discharging_1) / 2

                # check whether reversible voltages are plausible, if not, the soc is naught.
                if u_bat_mean_rev_discharging <= 0:
                    soc = 0
                else:
                    eta_bat_dis = u_bat_mean / u_bat_mean_rev_discharging  # calculation of the  voltage efficiency
                    # if self.t_place == 0 and forecast:
                        # eta_bat_dis = 1  # for the forecast of the very first step, the efficiency is set to 1

                    # numerical integration of the current, corrected by the efficiency, current and temperature factors
                    drawn_soc = 0.5 * (t1 - t0) * (alpha1 * beta1 * i1 + alpha0 * beta0 * i0) / (self.C_bat_As )#* eta_bat_dis)
                    soc = soc_pre - drawn_soc

            # charging case, analogous to the discharging case. Except the efficiency is in the numerator instead of the
            # denominator
            else:
                if self.t_place == 0:
                    u_bat_mean_rev_charging_0 = 0
                    u_bat_mean_rev_charging_1 = 2 * self.voltage_reversible_charging()
                elif forecast:
                    u_bat_mean_rev_charging_0 = 2 * self.voltage_reversible_charging.prev_val
                    u_bat_mean_rev_charging_1 = 0
                else:
                    u_bat_mean_rev_charging_0 = self.voltage_reversible_charging.prev_val
                    u_bat_mean_rev_charging_1 = self.voltage_reversible_charging()

                u_bat_mean_rev_charging = (u_bat_mean_rev_charging_0 + u_bat_mean_rev_charging_1) / 2

                if u_bat_mean == 0:
                    charged_soc = 0
                else:
                    # eta_bat_charging = u_bat_mean_rev_charging / u_bat_mean
                    charged_soc = (t1 - t0) * 0.5 * (i1 / (alpha1 * beta1) + i0 / (alpha0 * beta0)) * (1 / self.C_bat_As)# * eta_bat_charging
                soc = soc_pre + charged_soc
        else:
            soc = soc_pre
        return soc

    def update_temp_pre(self):
        """
        update the temperature and carry the value to the next time step

        :return: nothing
        """
        self.temp.val = self.new_temp(True)

    def update_temp_post(self, mode="post"):
        """
        update the temperature and carry the value to the next time step

        :param mode: whether to do an estimation of the new temp ("pre"), or the calculation of the actual temp ("post")

        :return: nothing
        """
        self.temp.val = self.new_temp()

    def new_temp(self, forecast=True):
        """
        calculates the temperature of the battery for the time step

        :param forecast: whether to do a forecast calculation ("True") or the calculation after the current step
        ("False")

        :return: the temperature
        """
        temp_pre_val = self.temp.prev_val

        p0_iout = (self.rs + self.rct) * (self.i_out.prev_val / self.parallel_cells) ** 2
        p0_iin = (self.rs + self.rct) * (self.i_in.prev_val / self.parallel_cells) ** 2
        p1_iout = (self.rs + self.rct) * (self.i_out() / self.parallel_cells) ** 2
        p1_iin = (self.rs + self.rct) * (self.i_in() / self.parallel_cells) ** 2

        if forecast:
            p0_iout *= 2
            p0_iin *= 2
            p1_iout = 0
            p1_iin = 0
            temp_pre_val = self.temp.prev_val

        if self.t_place == 0:
            t0 = 0
            temp_pre_val = self.temp_start
        else:
            t0 = self.t[(self.t_place - 1)]
        t1 = self.t[self.t_place]
        p_med_iout = 0.5 * (p1_iout + p0_iout)
        p_med_iin = 0.5 * (p1_iin + p0_iin)
        temp_iout = self.R_therm_discharge * p_med_iout + (t1 - t0) * p_med_iout / (self.C_therm_discharge * self.m)
        temp_iin = self.R_therm_charge * p_med_iin + (t1-t0) * p_med_iin / (self.C_therm_charge * self.m)
        temp = temp_pre_val + temp_iout + temp_iin
        return temp

    def calculate_add_post_values(self):
        """
        update the values of the soc and the temperature of the battery
        overrides method of parent class
        """
        self.current.val = self.i_out() - self.i_in()
        if self.current() < 0:
            self.voltage.val = self.u_in()
        else:
            self.voltage.val = self.u_out()
        self.power_total.val = self.current() * self.voltage()

# -------------------------------------------------------------------------------------------------------------------- #
    # OVERRIDING THE METHODS OF THE COMPONENT CLASS
# -------------------------------------------------------------------------------------------------------------------- #
    def calculate_segment_pre_values(self):
        """
        set initial values at t = 0
        """
        if self.t_place == 0:
            self.soc.overwrite_constant(self.soc_start)
            self.temp.overwrite_constant(self.temp_start)

    def calculate_efficiency(self):
        """
        overrides the method calculating the efficiency.
        The efficiency is calculated based on the ratio between the real drawn soc capacity and the drawn current
        capacity.
        """
        # calculate the duration of the time step and the mean current during this step
        if self.t_place == 0:
            delta_t = 0
            i_mean = 0  # no mean current needed, since no efficiency can be calculated for a duration of zero.
            nominal_cap = 0
            delta_soc = 0
        else:
            delta_t = self.t[self.t_place] - self.t[self.t_place - 1]
            i_mean = ((self.i_out.prev_val + self.i_in.prev_val) + (self.i_out() + self.i_in())) / 2
            nominal_cap = self.C_bat_As
            delta_soc = self.soc() - self.soc.prev_val

        if delta_t != 0 and delta_soc != 0 and i_mean != 0:
            eff = 1
            if delta_soc > 0:  # charging
                eff = (delta_soc * nominal_cap) / (delta_t * i_mean)
            elif i_mean >= 1e-4:  # discharging
                eff = -(delta_t * i_mean) / (delta_soc * nominal_cap)

            if self.efficiency.prev_val == 1:  # check whether the previous efficiency of the battery was 1
                self.efficiency.prev_val = eff  # if yes correct by saving eff
            return eff
        else:
            return 1

    def f(self):
        """
        function describing the behavior of the battery in the circuit
        charging and discharging have independent functions
        in case the circuit is unregulated, the charging and discharging equations have to be fulfilled simultaneously
        overrides method of parent class

        :return: difference between voltage and battery voltage dictated by the battery model and the current, goal: 0
        """
        if self.regulated and self.charging:
            # Charging
            return [self.return_charging_voltage(self.i_in()) - self.u_in()]
        elif self.regulated and not self.charging:
            # Discharging
            return [self.return_discharging_voltage(self.i_out()) - self.u_out()]
        else:  # unregulated
            return [self.return_discharging_voltage(self.i_out()) - self.u_out(),
                    self.return_charging_voltage(self.i_in()) - self.u_in()]

    def df(self, d_unknown: Variable):
        """
        derivative of the function f w.r.t. unknowns
        overrides method of parent class

        :param d_unknown: unknown w.r.t to which the derivative has to be calculated

        :return: the resulting derivative
        """
        try:
            # check whether the input unknown is used in the function f
            place = self.unknowns.index(d_unknown)
        except ValueError:
            if self.regulated:
                return [0]
            else:
                return [0, 0]
        if place == 0:
            return self.dfdu_in()
        elif place == 1:
            return self.dfdi_in()
        elif place == 2:
            return self.dfdu_out()
        elif place == 3:
            return self.dfdi_out()

    def dfdu_in(self):
        """
        derivative w.r.t the voltage, it's the same for u_in and u_out

        :return: -1
        """
        if self.regulated and self.charging:
            return [-1]
        elif self.regulated and not self.charging:
            return [0]
        else:
            return [0, -1]

    def dfdi_in(self):
        """
        derivate w.r.t. i_in
        """
        h = 1e-7
        if self.regulated and self.charging:
            return [(self.return_charging_voltage(self.i_in() + h) - self.return_charging_voltage(self.i_in() - h)) / (
                     2 * h)]
        elif self.regulated and not self.charging:
            return [0]
        else:
            return [0, (self.return_charging_voltage(self.i_in() + h) - self.return_charging_voltage(self.i_in() - h)) / (
                    2 * h)]

    def dfdu_out(self):
        if self.regulated and self.charging:
            return [0]
        elif self.regulated and not self.charging:
            return [-1]
        else:
            return [-1, 0]

    def dfdi_out(self):
        """
        derivative w.r.t. i_out
        """
        h = 1e-7
        if self.regulated and self.charging:
            return [0]
        elif self.regulated and not self.charging:
            return [(self.return_discharging_voltage(self.i_out() + h) - self.return_discharging_voltage(
                     self.i_out() - h)) / (2 * h)]
        else:
            return [(self.return_discharging_voltage(self.i_out() + h) - self.return_discharging_voltage(
                     self.i_out() - h)) / (2 * h), 0]
