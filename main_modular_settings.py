"""
running simulations with quick control over the parameters of the architecture
"""

import bfch

circuit_solver = bfch.CircuitSolver(max_step_size=100,
                                    allowed_dev=1e-2,
                                    max_solver_steps=500,
                                    convergence_crit=1e-4,
                                    solver_step_factor=0.1,
                                    step_factor_reduction_speed=100,
                                    min_num_steps_per_segment=2)

circuit_param = {
                "bat_charge_below_soc": 0.97,
                "bat_max_soc": 0.9999,
                "bat_min_soc": 0.05,
                "load_voltage": 40,
                "bat_charging_i": 1,
                "bat_discharging_i": 30,
                "conditioning_toggle": False,
                "conditioning_period": 30,
                "conditioning_duration": 0.1
            }

component_info = {
                "fc_stack": "NG19_Fuel_Cell_onedflux.yaml",  # 0
                "dcdc_fc": "DCDC_converter.yaml",  # 1
                "diode_fc": "Diode_regulated.yaml",  # 2
                "battery": "Battery_HyDDEn.yaml",  # 3
                "dcdc_bat_di": "DCDC_converter.yaml",  # 4
                "diode_bat_di": "Diode_regulated.yaml",  # 5
                "dcdc_bat_ch": "DCDC_converter.yaml",  # 6
                "diode_bat_ch": "Diode_regulated.yaml",  # 7
                "power_sink": "Power_Sink.yaml",  # 8

            }

data2plot = {
            "Power Distribution": {
                    "dcdc_fc": ["power_out"],
                    "power_sink": ["power_in"],
                    "diode_bat_di": ["power_out"],
                    "dcdc_bat_di": ["power_in"]
                },
            "Voltages": {
                    "fc_stack": ["u_out"],
                    "battery": ["u_out", "u_in"]
                },
            "SOC": {
                    "battery": ["soc"]
                }
            }

circuit = bfch.create_circuit(circuit_type="CircuitRegulatedUncontrolledPowerSharing",
                              circuit_param=circuit_param,
                              component_info=component_info)

circuit_solver.calculate_mission(mission_file_path=f"Data\Example_Missions\TEST_mission_profile_100s_test_state1.csv",
                                 circuit=circuit, result_path="Results",
                                 verbose=False, data2plot=data2plot)
