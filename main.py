"""
Created on 1/10/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Description:
minimal code example to run a simulation
    
"""
import bfch

circuit_solver = bfch.CircuitSolver(max_step_size=100,
                                    allowed_dev=1e-2,
                                    max_solver_steps=500,
                                    convergence_crit=1e-4,
                                    solver_step_factor=0.1,
                                    step_factor_reduction_speed=100,
                                    min_num_steps_per_segment=2)

circuit_solver.calculate_mission(mission_file_path=f"Data\Example_Missions\TEST_mission_profile_100s_test_state1.csv",
                                 circuit="CircuitRegulatedUncontrolledPowerSharing", result_path="Results",
                                 verbose=False)
