.. Hy2Sim documentation master file, created by
   sphinx-quickstart on Wed Dec 22 08:31:05 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

========================================
Welcome to Hy2Sim's documentation!
========================================

Hy2Sim is a tool for the analysis and design of fuel cell electric powertrain architectures with focus on 
aerial applications. Hy2Sim can be used within conceptual design or sizing applications. It is a standalone 
software, but could be integrated into a larger vehicle design or sizing environment. The tool is capable of 
calculating the states of the components within a given electrical architecture. The necessary values are 
computed based on a customizable mission profile and a network of components.  



.. _User_Guide:

`User Guide`_
=============


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   userguide
   bib

.. _Source Code Documentation:

`Source Code Documentation`_
============================


.. toctree::
   :maxdepth: 2

.. automodule:: bfch
   :imported-members:
   :members:
   :exclude-members: abstractmethod, final
   :special-members:

.. automodule:: bfch.data
   :imported-members:
   :members:
   :exclude-members: datetime, FormatStrFormatter


.. only:: html

	Indices and tables
	==================

	* :ref:`genindex`
	* :ref:`modindex`
	* :ref:`search`
