.. _Bibliography:


.. only:: html

   ============
   Bibliography
   ============


.. [Hayre16] Ryan P. O’Hayre, Suk-Won Cha, Whitney G. Colella, and F. B. Prinz.Fuel cell fundamentals.Third edition
    Ryan O’Hayre, Suk-Won Cha, Whitney Colella, Fritz B. Prinz. Hoboken, NewJersey: John Wiley & Sons, 2016.
    ISBN: 9781119114208
    
.. [Ng2021] Ng, Wanyi and Patil, Mrinalgouda and Datta, Anubhav. Hydrogen Fuel Cell and Battery Hybrid Architecture for Range Extension of Electric VTOL (eVTOL) Aircraft
    Journal of the American Helicopter Society, 2021.
    doi: 10.4050/JAHS.66.012009
    
.. [Ng2019] Ng, Wanyi and Datta, Anubhav. Hydrogen Fuel Cells and Batteries for Electric-Vertical Takeoff and Landing Aircraft
    Journal of Aircraft, 2019.
    doi: 10.2514/1.C035218


  
