.. _Introduction:

Introduction
++++++++++++

This is the user guide for Hy2Sim. This package is an analysis tool for fuel cell electric powertrains with 
focus on aerial applications. It computes the states of the powertrain for a single mission segment. The 
electric powertrain is modeled as a network of individual components. The electrical network 
(:obj:`~bfch.Circuit`) holds the information about the connections between the components. The components 
(:obj:`~bfch.Component`) model their physical behavior as a function and store their operating states. The 
analysis of the mission segment is a four step process. First, preprocessing is done where the mission segment
is divided into time steps and external operating conditions are passed on to the components. 
Second, the network is converted into a set of equations. Third, the set of
equations is solved using newtons method. Fourth, post-processing is done where additional components states
are evaluated. The whole package is based on object-oriented programming. New components or circuits can be
added quickly. 


.. _Installation_Guide:

Installation Guide
++++++++++++++++++++


.. _Installation:

Installation
^^^^^^^^^^^^
To install Hy2Sim, follow these steps::

	git clone https://gitlab.lrz.de/HTMWTUM/hy2sim.git
	cd hy2sim
	python setup.py install	


.. _Generate_Documentation:

Generate Documentation
^^^^^^^^^^^^^^^^^^^^^^
Hy2Sim documentation can be generated using the python package 
`Sphinx <https://www.sphinx-doc.org/en/master/usage/quickstart.html>`_
and the files in the ``docs``
directory of the Hy2Sim package by using the following commands::

	cd path_to_docs
	make html BUILDDIR=directory_path

if ``BUILDDIR`` argument is not provided, the HTML documents will be generated in
the ``docs/build/html`` directory.  Different document formats can be
built, just execute ``make`` without an argument to see which format targets are
available.


**LaTex PDF generation:**
If you have TeX and LaTeX available, run ``make latexpdf``, which runs
the LaTeX builder and invokes the pdfTeX toolchain for you. The PDF 
document will be generated in the ``docs/build/latex`` directory.


.. _Overview_of_Program_Structure:

Overview of Program Structure
+++++++++++++++++++++++++++++


.. _Top_Level:

Top Level
^^^^^^^^^^^^
Object oriented programming is used in this package. The interaction between the most important parts of the 
program are depicted in this :download:`diagram <includes/data_flow.pdf>`. The circuit architecture and the 
solving procedure are
implemented in the :obj:`~bfch.Circuit` class. One analysis necessitates only one circuit class. The 
components of the circuit are each
modelled as :obj:`~bfch.Component` classes and one instance is create for every single component of the
circuit. Therefore, one :obj:`~bfch.Circuit` object manages multiple 
:obj:`~bfch.Component` objects. The information about the investigated mission segment is stored within an
instance of the :obj:`~bfch.Input` class. This contains start and end values for the time, power, ambient
conditions and operating conditions. The results from the calculation are stored in an instance of the 
:obj:`~bfch.Output` class. This contains the currents and voltages of all components at every time step, as 
well as derived values like the hydrogen consumption or the state of charge of the battery. An instance of 
the :obj:`~bfch.CircuitSolver` class is used to coordinate the preprocessing of the segment information, the
actual calculation of all time steps in the :obj:`~bfch.Circuit` and the storing of information inside the
:obj:`~bfch.Output`. :download:`Here <includes/class_diagram_V1.pdf>`, you can find a class diagram for the 
package.


.. _Solving_Procedure:

Solving Procedure
^^^^^^^^^^^^^^^^^^^^
In this section, the procedure for solving the circuit is described. The general approach is dividing the 
mission or the segment into time steps and solving at each of them in a quasi - steady state fashion. The size 
of the time steps has to be chosen in such way that there are only small changes in the states of the 
components. This is crucial when modelling the battery, because its behaviour is highly dependent on the 
instantaneous SOC (State of Charge). Another critical application in this regard is the use of models based on 
differential equations. 

The solving operation for a single time step involves the classes :obj:`~bfch.Circuit`, 
:obj:`~bfch.Component` and :obj:`~bfch.Variable`. The following section explains the 
:download:`sequence diagram <includes/solving_procedure_V3.pdf>`.

The first step of the solving procedure is performing preliminary calculations in all the components. 
During this stage, the components can perform calculations regarding boundary conditions or states that 
influence the components behaviour in the current time step.

In the subsequent step of the calculation, the matrix containing the information about the connections 
between the components is used to create the first two parts of the system of equations. At first, the 
components having either power flowing in or power flowing out are stored in a list ("active_components"). 
Subsequently, the 
matrix is mapped to new objects of class :obj:`~bfch.Connection` of the super class :obj:`~bfch.Component`.
These objects 
therefore behave analogously to the other components of the circuit. They contain the equations enforcing 
that the sum of the incoming currents must equal the sum of exiting currents and that the relative voltage 
level is identical for all components connected to this node. All the created :obj:`~bfch.Connection` objects 
are appended to the list of the other components.

Next, the boundary conditions have to be included into the system of equations. Again, the input of the 
user is mapped to a child class of :obj:`~bfch.Component`. Here, it is the class 
:obj:`~bfch.BoundaryCondition`. Its function 
returns the difference between the set boundary condition and the current value of the constrained 
variable. Following that, initial conditions for the variables are set and the solvability of the system of
equations is checked. The set of equations is present in the form of a list of :obj:`~bfch.Component` child 
class objects, all containing the functions :func:`~bfch.Component.f` and :func:`~bfch.Component.df`, 
making the automation of the solving process easy.

At this point, Newton's method is applied to solve the non-linear set of equations. This iterative method 
computes the current values of the list of equations and uses the gradient of the functions with respect to all
the unknowns to calculate incremental steps for all unknowns. It terminates if one of the following occurs: the
convergence criterion is met, the maximum number of iterations is exceeded or a "NaN" value is computed.

After the set of equations is solved, it is checked whether the boundary conditions are met. At last, all 
components and the circuits perform their post calculations. That means calculating necessary or additional 
values, that are based on the solution of the circuit.


.. _Analysis_Example:

Analysis Example
+++++++++++++++++++++++++++++++

.. _Create_Circuit:

Create Circuit
^^^^^^^^^^^^^^^^
To investigate a new circuit architecture, a new child class of :obj:`~bfch.Circuit` is necessary. The best 
way to create one, is to copy an example, like :obj:`~bfch.CircuitRegulatedUncontrolledPowerSharing`. Then, 
adjust the
content of the constructor and the :func:`~bfch.Circuit.create_circuit_equations` function. 

The constructor must at least
call the constructor of :obj:`~bfch.Circuit` passing the information about the components of the circuit
("component_info") and the name of the circuit ("circuit_type"). "component_info" is a dictionary with one 
entry
([unique_component_name] : [component_parameter_file].yaml) for every component of the circuit. Additional
logging variables or operating conditions can be added. Therefore, new objects of :obj:`~bfch.Variable` have 
to be initialized and passed to the constructor of :obj:`~bfch.Circuit` as lists ("logging_variables" and 
"op_conditions"). If additional pre- or postprocessing calculations are necessary, the respective functions 
have to be passed to the constructor of :obj:`~bfch.Circuit` as lists ("func_post_values","func_pre_values").
Once the super class constructor has been called, you can access the components of the circuit through a 
callable: self.("[unique_component_name]").
 
:func:`~bfch.Circuit.create_circuit_equations` defines the architecture of the circuit. This information is stored in the
2D numpy array "self.connectivity". Filling the matrix follows the following rules:

* Every component defined in the constructor is assigned to one row and one column
* The order set for the components in the constructor is the same for the matrix.
* For every component (column), set a one in every row corresponding to the components into which power flows.
  All other entries are zeros.
* If a component is not connected, the corresponding column and row are filled with zeros.

If the architecture can change depending on the operating conditions, like an extra circuit for charging 
the battery, this can be achieved through the use of if-else conditions.
Additionally to the connectivity matrix, the boundary conditions can be defined ("self.boundary_conditions").
This is a list of tuples. The first entry beeing the the reference to the component variable and the second 
beeing the value that will be set for the calculation.


.. _Run_Analysis:

Run Analysis of multiple mission segments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Preparation steps to run an analysis:

* Create a folder "Data" for the mission profile and the component parameters
* Create a folder "Results" for the calculation results
* Create a script to run through the mission segment, like "main_H3"

The mission profile has to be defined in a csv similar to the ones in \Data\Example_Missions. The information
for the mission segments is organized in columns. The headers of the columns must follow the rule 
"[name] [unit]". The minimum 
information needed to start a calculation are the time and the power for every step of the mission. Every 
segment 
has to be characterized by two rows. One for the values at the start of the segment and one for the values at
the end of the segment. The "Data" folder must also contain the .yaml files for the component parameters.

First, the mission data needs to be imported using :func:`~bfch.data.get_mission_data`. Next, one instance of
each of the following classes needs to be created:

* the investigated circuit, (e.g. :obj:`~bfch.CircuitRegulatedUncontrolledPowerSharing`)
* the :obj:`~bfch.Input`
* the :obj:`~bfch.Output`
* the :obj:`~bfch.CircuitSolver`

Next, loop through the segments of the mission profile. First, the :obj:`~bfch.Input` has to be updated to the
current segment using :func:`~bfch.Input.update_from_mission_profile`. Second, 
:func:`~bfch.CircuitSolver.solve` can be excecuted. This function return the filled :obj:`~bfch.Output`
that can be added to a list of outputs, by deepcopy of the returned :obj:`~bfch.Output`.

Once the list of output has been generated, the following functions can be used to create plots and save the 
data:

* :func:`~bfch.data.plot_results_dyn`
* :func:`~bfch.data.save_output_to_csv`
* :func:`~bfch.data.save_plots`
   

.. _Model_Description:

Decription of the Component Models
+++++++++++++++++++++++++++++++++++++

.. _Fuel_Cell:

Fuel Cell Models
^^^^^^^^^^^^^^^^^^^^^^^^
Fuel cells have a characteristic voltage-current behavior. Not only does the voltage drop with increasing
current, but the output voltage is heavily influenced by the chemistry, the temperature and the pressure at the 
electrodes. The model of the fuel cell describes this behavior. The class :obj:`~bfch.FcModel`
is equipped with five models. The available data and the projected environment for the fuel cell should
determine which model is best suited. 

All models describe the behavior of a single fuel cell. To describe the characterictic independently of the
surface area of the cell, the current is replaced by the current density.

The thermodynamic equilibrium voltage of the fuel cell describes the highest possible potential between the 
anode and the cathode. This is dependent on the temperature of the fuel cell and the mole fractions of fuel 
at the electrodes. This is the baseline voltage and is used in all models. The following equation is derived 
from eq 2.96, [Hayre16]_.

:math:`E_{therm} = 1.229 V - (T - 298.15 K) * 8.46 * 10^{-4} \frac{V}{K} + 4.309 * 10^{-4}\frac{V}{K} (ln(x_{\ce{H2}}) + 0.5 ln(x_{\ce{H2}})) * T`

All fuel cells are affected by ohmic losses. These reduce the voltage by a linearly increasing voltage drop.
All models incorporate this type of loss. The following equation is Ohm's Law applied to current densities.

:math:`\eta_{ohmic} = i * ASR_{ohmic}`

Besides the thermodynamic and ohmic behavior of the fuel cell, the chemical reactions and the concentration of
reactants strongly influence the power output of the fuel cell. Every chemical reaction needs an activation
energy to start. This leeds to losses, which are mostly dependent on the temperature of the fuel cell. 
Additionally, the oxygen reaction at the cathode is much slower and induces higher loss than the hydrogen
reaction at the anode. The concentration of the reactants depends gas pressure at the electrode, the reaction
rate and the thickness of the electrode.


:func:`~bfch.FcModel.basic`
###########################
The basic model models the activation loss with :func:`bfch.FcModel.eta_act_basic_complete` and the 
concentration loss with :func:`bfch.FcModel.eta_conc_basic`. These are not influenced by the 
temperature nor the pressure at the electrodes. Therefore, this model is only useful if the operating 
conditions are constant and close to the conditions used for the fuel cell test.

:math:`V(i) = E_{therm} - \eta_{ohmic} - \eta_{act, \: basic} - \eta_{conc, \: basic}`


:func:`~bfch.FcModel.basic_act_var_t`
#####################################
"basic_act_var_t" models the activation loss with :func:`bfch.FcModel.eta_act_fp_complete_var_t` and the
concentration loss with :func:`bfch.FcModel.eta_conc_basic`. Now, the activation loss is dependent on the
temperature and the parameters are physically significant. This allows for modelling the fuel cell for 
operation under changing temperatures.

:math:`V(i) = E_{therm} - \eta_{ohmic} - \eta_{act, \: fixed \: P, \: var \: T} - \eta_{conc, \: basic}`


:func:`~bfch.FcModel.basic_var_p_var_t`
#######################################
"basic_var_p_var_t" models the activation loss with :func:`bfch.FcModel.eta_act_fp_complete_var_p_var_t` and
the concentration loss with :func:`bfch.FcModel.eta_conc_var_il`. Here, the exchange current density for the
activation loss and the limit current density for the concentration losses are calculated based on the pressure
on the electrodes. This model now describes the behaviour of the fuel cell under different pressures and
temperatures. The main inconvience being the high number of parameter which need fitting.

:math:`V(i) = E_{therm} - \eta_{ohmic} - \eta_{act, \: fixed \: P, \: var \: T} - \eta_{conc, \: var \: i_{leak}}`


:func:`~bfch.FcModel.one_d_flux_simple_asr`
###########################################
The 1D flux model takes a slightly different approach to calculating the activation and concentration losses
(:func:`bfch.FcModel.eta_cath_basic`).
It is described in [Hayre16]_ p.206 et sqq. Only the losses at the cathode are taken into account. The resulting
model is able to describe the behavior of the fuel cell under various operating conditions.

:math:`V(i) = E_{therm} - \eta_{ohmic} - \eta_{cath, \: basic}`



.. _Battery:

Battery Model
^^^^^^^^^^^^^^
The output voltage of a battery depends on the state of charge and the current through the battery. This is 
described by an adaptation of the shepherd model described in [Ng2019]_. The added linear dependency from the 
SOC
helps fitting to experimental data. 

:math:`V(I) = E_S - \frac{k}{SOC}I - Ae^{-B(1-SOC)} - RI - Q(1-SOC)`

:math:`E_S` represents the open circuit voltage of the battery at 100% SOC. For a given current this is reduced
by a constant ohmic resistance independent of the SOC, :math:`RI`. The exponential term describes the behavior
of the battery at high SOC. The qotient describes the battery behaviour at low SOC, where the voltage drops 
sharply. 


.. _Limitations:

Limitations
+++++++++++
Current limitations include:

* no transient behavior of the components modelled
* no model for the ageing of the fuel cell and the battery
* temperature and cooling models are not implemented and tested yet
* power flow is only possible in one direction for one specific connectivity matrix

.. _Validation:


Validation of Hy2Sim
++++++++++++++++++++++++
Experimental validation has not yet been performed. However, a comparison to the work presented in [Ng2021]_ 
,Figure 17, was performed. The results can be seen in 
:download:`this file <includes/20220608_171133_regulated9_ng_datta_regulated_power_result.pdf>`







