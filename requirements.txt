numpy~=1.20.3
pandas~=1.3.5
matplotlib~=3.5.0
yaml~=0.2.5
pyyaml~=6.0
scipy~=1.7.3
setuptools~=58.0.4