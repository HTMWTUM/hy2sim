"""
Created on 24.04.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""

import yaml
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
from bfch import *


raw_battery_data = open("../Data/Example_Batteries/Battery_generic_singlecell.yaml")
# raw_battery_data = open("../Data/Battery_Shepherd_ng_2019.yaml")
# raw_battery_data = open("../Data/NG19_LiPo_Cell_3_6C.yaml")
battery_data = yaml.load(raw_battery_data, Loader=yaml.FullLoader)

battery = LiIon("bat_test", battery_data)
# battery_high_press = LiIon("1D_flux_simpleASR", battery_data)
# battery_low_press = LiIon("1D_flux_simpleASR", battery_data)
# battery_ng = LiIon("Basic", battery_data)
# battery_ng_high_p = LiIon("Basic", battery_data)
# battery_ng_low_p = LiIon("Basic", battery_data)

input_file = Input()
c_rate = 30

battery.soc_start=1

charging = False
battery.idle = False
battery.regulated = True

if charging:
    nominal_cap = (1-battery.soc_start) * battery_data['C'] * battery_data['parallel_cells']
    current = c_rate * nominal_cap
else:
    nominal_cap = battery.soc_start * battery_data['C'] * battery_data['parallel_cells']
    current = c_rate * nominal_cap

predicted_time = 3600 * nominal_cap / current

t = np.linspace(0, predicted_time * 1., 100)

input_file.conditions = ["time s", "ambient_temp K"]
input_file.segment = [t, [290] * len(t)]
Component.t = t
battery.new_segment(input_file)
battery.i_out.overwrite_constant(current)
battery.soc.overwrite_constant(1)
battery.soc.val_min = 0
# battery.C_bat_As=4.
voltage = []

soc = [battery.soc_start]
es = []
term1 = []
term2 = []
term3 = []
ocv = []
term4 = []
eff = []

extraterms=False


for i in range(len(t)):
    Component.t_place = i
    Variable.t_place = i
    battery.idle = False
    battery.update_soc_pre()
    if charging:
        battery.charging = True
        voltage.append(battery.return_charging_voltage(current))
        battery.u_in.val = voltage[-1]
    else:
        voltage.append(battery.return_discharging_voltage(current))
        battery.u_out.val = voltage[-1]

    battery.update_soc_post()
    if i != 0:

        soc.append(battery.soc())
    eff.append(battery.calculate_efficiency())
    if extraterms:
        term1.append(battery.es-battery.k/battery.soc()*battery.i_out()/battery.parallel_cells)
        term2.append(battery.es+battery.a * np.exp(-battery.b * (1 - battery.soc())))
        term3.append(battery.es-battery.r*battery.i_out()/battery.parallel_cells)
        ocv.append(battery.es-battery.k/battery.soc()*battery.i_out()/battery.parallel_cells+battery.a * np.exp(-battery.b * (1 - battery.soc()))-battery.r*battery.i_out()/battery.parallel_cells)
        term4.append(battery.es-battery.i_out()/battery.parallel_cells * (battery.rs + battery.rct))
        es.append(battery.es)
    
    

voltage=[v/battery_data['series_cells'] for v in voltage]
t=[ti/60 for ti in t]

df=pd.DataFrame()
#df['t']=t
df['U']=voltage
df['SOC']=soc
df['eff']=eff

now = datetime.now()
currentdate = now.strftime("%Y%m%d_%H%M%S")
# df.to_csv(f'../../plots/{currentdate}_battery_charge.csv', sep=',', decimal='.')

# fig, ax = plt.subplots()
# plt.plot(soc, voltage, color='black')
# ax.set_xlim(1.,0.)
# ax.set_ylim(0.6*max(voltage),max(voltage))
# ax.set_ylabel('U [V]')
# ax.set_xlabel('SOC')

fig, ax = plt.subplots()
plt.plot(soc, voltage, color='black', label='U')
if extraterms:
    plt.plot(t, term1, color='red', label='es-T1')
    plt.plot(t, term2, color='green', label='es+T2')
    plt.plot(t, term3, color='yellow', label='es-T3')
    plt.plot(t, ocv, color='purple', label='ocv')
    plt.plot(t, term4, color='orange', label='es-T4')
    plt.plot(t, es, color='blue', label='es')
#ax.set_xlim(0.,max(t)+1)

# ax.set_ylim(0.6*max(voltage),max(voltage))
ax.set_ylim(2.5,4.5)
ax.set_ylabel('U [V]')
ax.set_xlabel('SoC')
plt.axis([1, 0, min(voltage), max(voltage)])
ax.legend()


# fig, ax = plt.subplots()
# plt.plot(t, battery.soc.return_val(), color='black')
# ax.set_ylabel('SOC')
# ax.set_xlabel('t [min]')
plt.show()

