"""
Created on 01.04.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
import yaml
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
from bfch import FcModel
from bfch import Input


def check_pol_curve(fc, color, style, ax, totalVoltage, label):
    fc.find_limit_values()
    i_range = np.linspace(0, fc.maxcurrent()/fc.cell_area, 1000)
    fc_model_vectorized = np.vectorize(fc.one_d_flux_simple_asr)
    eff_vectorized = np.vectorize(fc.calculate_efficiency)
    v_result = fc_model_vectorized(i_range)
    print(fc.e_therm())
    print(fc.eta_ohmic_asr(0.5))
    eff_result = eff_vectorized(np.multiply(v_result, fc.series_cells))
    p_result = np.multiply(np.multiply(v_result, 1), np.multiply(i_range, 1))
    # plt.plot(np.divide(i_range, fc.cell_area), np.divide(v_result, fc.series_cells), color='black')
    if totalVoltage:
        ax.plot(np.divide(i_range[1:], 1), np.multiply(v_result[1:], fc.series_cells), color=color, linestyle=style)
    else:
        ax.plot(np.divide(i_range[1:], 1), np.multiply(v_result[1:], 1), color=color, linestyle=style, label=f"{label}")
    ax.set_ylabel(r'Voltage $[V]$')
    ax.set_xlabel(r'Current Density $[\frac{A}{cm^2}]$')
    ax.set_xlim(-0.1, 2.)
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(15)
    # plt.plot(np.divide(i_range, 1), np.multiply(v_result,i_range), color='black')
    # plt.plot(np.divide(i_range, fc.cell_area), eff_result, color='black')
    df = pd.DataFrame()
    df['i'] = i_range
    df['v'] = v_result
    df['eff'] = eff_result
    df['p_res'] = p_result


raw_fc_data = open("../Data/Example_FuelCellStacks/UNICADO_FuelCell_oneDFlux.yaml")
raw_fc_data2 = open("../Data/Example_FuelCellStacks/UNICADO_FuelCell_oneDFlux2.yaml")
fc_data = yaml.load(raw_fc_data, Loader=yaml.FullLoader)
fc_data2 = yaml.load(raw_fc_data2, Loader=yaml.FullLoader)

fc1 = FcModel("1D_flux_simpleASR", fc_data)
fc2 = FcModel("1D_flux_simpleASR", fc_data2)
# fc_high_press = FcModel("1D_flux_simpleASR", fc_data)
# fc_low_press = FcModel("1D_flux_simpleASR", fc_data)
# fc_ng = FcModel("Basic", fc_data)
# fc_ng_high_p = FcModel("Basic", fc_data)
# fc_ng_low_p = FcModel("Basic", fc_data)

# Input
input_file1 = Input()
input_file1.conditions = ["time s", "anode_h2_molfrac 1", "anode_p Pa", "cath_o2_molfrac 1", "cath_p Pa", "fc_temp K"]
input_file1.segment = [[0], [1], [101325], [0.21], [101325], [343.15]]
fc1.new_segment(input_file1)

input_file2 = Input()
input_file2.conditions = ["time s", "anode_h2_molfrac 1", "anode_p Pa", "cath_o2_molfrac 1", "cath_p Pa", "fc_temp K"]
input_file2.segment = [[0], [1], [101325], [0.21], [101325], [343.15]]
fc2.new_segment(input_file2)

plt.style.use('publication_style')

#plt.style.use('tex')

plt.rcParams['text.latex.preamble']=r"\usepackage{fourier}"
#Options
params = {'text.usetex' : True,
          'font.size' : 15,
          'font.family' : 'fourier',
          }
plt.rcParams.update(params)
plt.rcParams.update({'font.size': 22})

fig, (ax1, ax2) = plt.subplots(1, 2)

check_pol_curve(fc1, 'black', 'dotted', ax1, True, "test")
check_pol_curve(fc2, 'red', 'solid', ax1, True, "test")

check_pol_curve(fc1, 'black', 'dotted', ax2, False, "$\delta_C = 0.0001$")
check_pol_curve(fc2, 'red', 'solid', ax2, False, "$\delta_C = 0.0002$")
ax2.legend(prop={'size': 15})


plt.show()

fig.savefig(f'polarisationLinesComparison.pdf', format='pdf', bbox_inches='tight')

# test activation overvoltage
# if False:
#     i_range = np.linspace(0, 0.31, 100)
#     fc_model_vectorized = np.vectorize(fc.eta_act_fp_complete_var_t)
#     v_result = fc_model_vectorized(i_range, fc.alpha_A, fc.alpha_C, fc.i_0A, fc.i_0C, fc.i_leak)
#     plt.plot(np.divide(i_range, 1), np.divide(v_result, 1), color='black')
#     df = pd.DataFrame()
#     df['i'] = i_range
#     df['eta_act'] = v_result
#     # plt.plot(np.divide(i_range, 1), np.divide(v_result,1), color='black')
#     # plt.plot(np.divide(i_range, 1), np.multiply(v_result,i_range), color='black')
#     # plt.plot(np.divide(i_range, fc.cell_area), eff_result, color='black')
#     plt.show()

# test concentration overvoltage

# if False:
#     i_range = np.linspace(0, 0.30499999, 1000)
#     fc_model_vectorized = np.vectorize(fc.eta_conc_basic)
#     v_result = fc_model_vectorized(i_range, fc.i_L, fc.i_leak, fc.C)
#     plt.plot(np.divide(i_range, 1), np.divide(v_result, 1), color='black')
#     df = pd.DataFrame()
#     df['i'] = i_range
#     df['eta_conc'] = v_result
#     # plt.plot(np.divide(i_range, 1), np.divide(v_result,1), color='black')
#     # plt.plot(np.divide(i_range, 1), np.multiply(v_result,i_range), color='black')
#     # plt.plot(np.divide(i_range, fc.cell_area), eff_result, color='black')
#     plt.show()

# now = datetime.now()
# currentdate = now.strftime("%Y%m%d_%H%M%S")
# df.to_csv(f'../../plots/{currentdate}_eta_conc.csv', sep=',', decimal='.')

# input_file_high_press = Input()
# input_file_high_press.time = np.array([0, 1])
# input_file_high_press.anode_pressure = np.array([101325, 101325])
# input_file_high_press.cath_pressure = np.array([401325, 401325])
# input_file_high_press.temperature = np.array([350, 350])
#
# input_file_low_press = Input()
# input_file_low_press.time = np.array([0, 1])
# input_file_low_press.anode_pressure = np.array([101325, 101325])
# input_file_low_press.cath_pressure = np.array([51325, 51325])
# input_file_low_press.temperature = np.array([350, 350])
#
#
# fc_high_press.new_segment(input_file_high_press)
# fc_low_press.new_segment(input_file_low_press)
# fc_ng.new_segment(input_file)
# fc_ng_high_p.new_segment(input_file_high_press)
# fc_ng_low_p.new_segment(input_file_low_press)
#
#
# fc_high_press.find_limit_values()
# fc_low_press.find_limit_values()
# fc_ng.find_limit_values()
# fc_ng_high_p.find_limit_values()
# fc_ng_low_p.find_limit_values()
#
#
# i_range_high_press = np.linspace(0, fc_high_press.maxcurrent(), 100)
# i_range_low_press = np.linspace(0, fc_low_press.maxcurrent(), 100)
# i_range_ng = np.linspace(0, fc_ng.maxcurrent(), 100)
# i_range_ng_high_p = np.linspace(0, fc_ng_high_p.maxcurrent(), 100)
# i_range_ng_low_p = np.linspace(0, fc_ng_low_p.maxcurrent(), 100)
#
#
# fc_model_vectorized_high_press = np.vectorize(fc_high_press.fc_voltage)
# fc_model_vectorized_low_press = np.vectorize(fc_low_press.fc_voltage)
# fc_model_vectorized_ng = np.vectorize(fc_ng.fc_voltage)
# fc_model_vectorized_ng_high_p = np.vectorize(fc_ng_high_p.fc_voltage)
# fc_model_vectorized_ng_low_p = np.vectorize(fc_ng_low_p.fc_voltage)
#
#
# v_result_high_press = fc_model_vectorized_high_press(i_range_high_press)
# v_result_low_press = fc_model_vectorized_low_press(i_range_low_press)
# v_result_ng = fc_model_vectorized_ng(i_range_ng)
# v_result_ng_high_p = fc_model_vectorized_ng_high_p(i_range_ng_high_p)
# v_result_ng_low_p = fc_model_vectorized_ng_low_p(i_range_ng_low_p)
#
#
# plt.plot(i_range_high_press, v_result_high_press, color='blue')
# plt.plot(i_range_low_press, v_result_low_press, color='orange')
#
# plt.plot(i_range_ng, v_result_ng, color='red')
# plt.plot(i_range_ng_high_p, v_result_ng_high_p, color='green')
# plt.plot(i_range_ng_low_p, v_result_ng_low_p, color='yellow')
#
# plt.show()
