"""
Created on 01.04.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""
from scipy.optimize import curve_fit, leastsq
import yaml
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
from bfch import *

raw_fc_data = open("../Data/Example_FuelCellStacks/H3_Fuel_Cell_4500LV.yaml")
fc_data = yaml.load(raw_fc_data, Loader=yaml.FullLoader)

measurement = pd.read_csv('E:/UNI/SemesterData/2020SS/Semesterarbeit/Ausarbeitung/plots/fuel_cell_total.csv')

fc = FcModel("basic_var_p_var_t_simple", fc_data)
input_file = Input()
input_file.conditions = ["time s", "anode_h2_molfrac 1", "anode_p Pa", "cath_o2_molfrac 1", "cath_p Pa", "fc_temp K"]
input_file.segment = [[0], [1], [101325], [0.21], [101325], [343.15]]
fc.new_segment(input_file)
i_range = measurement['i'][200:].tolist()
v_result = measurement['v'][200:].tolist()


def func2fit(i, asr_ohm, d_eff_c):
    res = []
    alpha_c = 0.5
    delta_c = 400e-6
    i_0c = 1e-4
    for x in i: res.append(fc.one_d_flux_simple_asr(x, asr_ohm, alpha_c, i_0c, d_eff_c, delta_c))
    return res

plt.plot(measurement['i'], measurement['v'], color='black')

popt, pcov = curve_fit(func2fit, i_range, v_result, ftol=0.000001, xtol=0.000001, maxfev=10000, method='trf', bounds=([1e-10, 1e-10], [1, 1]))#, verbose=2, method='dogbox')

v_fit = []
v_fit_hp = []
i_range = np.linspace(0, 0.35, 1000)
v_fit = func2fit(i_range, popt[0], popt[1])

fc.pressure_o2.val = 4000
v_fit_hp = func2fit(i_range, popt[0], popt[1])


plt.plot(i_range, v_fit, color='red')
plt.plot(i_range, v_fit_hp, color='blue')

plt.show()

print(popt)
print(pcov)


# raw_fc_data = open("../Data/Fuel_Cell_ng2019.yaml")
# fc_data = yaml.load(raw_fc_data, Loader=yaml.FullLoader)
# fc_ng = FcModel("Basic", fc_data)
# input_file = Input()
# input_file.time = np.array([0, 1])
# input_file.anode_pressure = np.array([101325, 101325])
# input_file.cath_pressure = np.array([101325, 101325])
# input_file.temperature = np.array([350, 350])
#
# fc_ng.new_segment(input_file)
# fc_ng.find_limit_values()
# i_range_ng = np.linspace(0, fc_ng.maxcurrent()/fc_ng.cell_area, 100)
# fc_model_vectorized_ng = np.vectorize(fc_ng.basic)
# v_result_ng = fc_model_vectorized_ng(i_range_ng, fc_ng.alpha_A, fc_ng.alpha_C, fc_ng.i_0A, fc_ng.i_0C,
#                            fc_ng.ASR_ohm, fc_ng.i_L, fc_ng.i_leak, fc_ng.C)
#
# popt, pcov = curve_fit(fc_ng.one_d_flux_simple_asr, i_range_ng, v_result_ng, [0.5, 0.0295/1000, 0.001, 1000 * 10 ** (-6), 0.2])
# print(popt)
# print(pcov)
