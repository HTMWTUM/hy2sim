import unittest
import bfch
import yaml


class TestFcModel_LTPEMFC(unittest.TestCase):
    fc_object = None

    def setUp(self):
        """
        Test Case: T = 70C = 343.15K und P = 1bar (H2 und O2), H20 Product in liquid state.
        """
        raw_fc_data = open("../Data/Example_FuelCellStacks/FCModel_unittest.yaml")
        fc_test_data = yaml.load(raw_fc_data, Loader=yaml.FullLoader)
        raw_fc_data.close()
        self.fc_object = bfch.FcModel("TestFuelCell", fc_test_data)
        self.fc_object.delta_s_hat = -163.23
        self.fc_object.delta_h = 284396
        self.fc_object.hydrogen_hhv = self.fc_object.delta_h / self.fc_object.h2_molar_mass
        input_file = bfch.Input()
        input_file.conditions = ["time s", "anode_h2_molfrac 1", "anode_p Pa", "cath_o2_molfrac 1", "cath_p Pa",
                                 "fc_temp K"]
        input_file.segment = [[0], [1], [100000], [1], [100000], [343.15]]  # Test Case: 70 C und 1bar
        self.fc_object.new_segment(input_file)

        self.fc_object.i_out.val = 100
        self.fc_object.u_out.val = self.fc_object.fc_voltage(self.fc_object.i_out())
        self.fc_object.calc_h2_consumption()

    def test_etherm(self):
        etherm = round(self.fc_object.e_therm(), 2)
        etherm_chart = 1.19
        self.assertEqual(etherm, etherm_chart)

    def test_efficiency(self):
        efficiency = round(self.fc_object.calculate_efficiency(), 3)
        efficiency_voltage = round(self.fc_object.calculate_efficiency_voltage(), 3)
        self.assertEqual(efficiency, efficiency_voltage)


if __name__ == '__main__':
    unittest.main()
