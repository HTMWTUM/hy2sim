import unittest
import bfch
import logging


class TestNewCircuit(unittest.TestCase):
    def test_initialization(self):
        with self.subTest("Check Initialization"):
            circuit_name = input("Please enter the name of the circuit: ")
            circuit = bfch.create_circuit(circuit_type=circuit_name)
            self.assertIsInstance(circuit, bfch.Circuit)  # add assertion here
            logging.critical(f"Check Initialization of {circuit_name} successful")

        with self.subTest("Check Components"):
            self.assertGreater(len(circuit.components), 0)
            logging.critical(f"Check Components of {circuit_name} successful")

        with self.subTest("Test New Segment"):
            mission_profile = bfch.data.get_mission_data("C:\SoftwareProjects\Hy2Sim\hy2sim\Data\Example_Missions\TEST_new_circuit.csv")
            input_file = bfch.Input()
            input_file.update_from_mission_profile(0, mission_profile)
            circuit.new_segment(input_file)
            for component in circuit.components:
                for unknown in component.unknowns:
                    self.assertEqual(2, len(unknown.return_val()))
            logging.critical(f"Check New Segment of {circuit_name} successful")

        with self.subTest("Test Calculate Pre Values"):
            circuit.update_tplace(0)
            circuit.calculate_pre_values()
            logging.critical(f"Check Calculation of Pre Values of {circuit_name} successful")

        # ------------ check all states:
        for i in range(0, circuit.number_of_states):
            with self.subTest(f"Test Connectivity State {i}"):
                circuit.state_caller(i)
                circuit.check_connectivity_matrix()
                logging.critical(f"Check Connectivity of {circuit_name}, State {i} successful")
            with self.subTest(f"Test System of Equations State {i}"):
                circuit.set_system_of_equations()
                circuit.check_solvability()
                logging.critical(f"Check System of Equations of {circuit_name}, State {i} successful")
            with self.subTest("Test Solve NLSE Newton"):
                circuit.solve_nlse_newton(max_solver_steps=100, convergence_crit=1e-4, solver_step_factor=0.1,
                                       step_factor_reduction_speed=100, verbose=False)
                self.assertEqual(circuit.solved(), 1)
                logging.critical(f"Check Solving of {circuit_name}, State {i} successful")

        with self.subTest("Test Calculate Post Values"):
            circuit.calculate_post_values()
            logging.critical(f"Check Calculation of Post Values of {circuit_name} successful")


if __name__ == '__main__':
    unittest.main()
