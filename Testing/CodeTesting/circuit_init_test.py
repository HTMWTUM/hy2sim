import unittest
import bfch


class CircuitInitTest(unittest.TestCase):

    def test_circuits(self):
        for circuit_name in bfch.CreateCircuit.circuits_list.keys():
            with self.subTest(circuit_name=circuit_name):
                circuit = bfch.create_circuit(circuit_type=circuit_name)
                self.assertIsInstance(circuit, bfch.Circuit)  # add assertion here


if __name__ == '__main__':
    unittest.main()
