import shutil
import unittest
import bfch
import os
import pandas as pd


class TestRegulatedUncontrolled(unittest.TestCase):
    circuit = None

    def test_standard_mission(self):
        circuit_solver = bfch.CircuitSolver(max_step_size=100,
                                            allowed_dev=1e-2,
                                            max_solver_steps=500,
                                            convergence_crit=1e-4,
                                            solver_step_factor=0.1,
                                            step_factor_reduction_speed=100,
                                            min_num_steps_per_segment=2)
        mission_list = ["TEST_mission_profile_100s_test_state1.csv", "TEST_mission_profile_100s_test_state2.csv",
                        "TEST_mission_profile_100s_test_state3.csv", "TEST_mission_profile_100s_test_conditioning.csv"]

        for mission in mission_list:

            circuit_solver.calculate_mission(mission_file_path=f"C:\SoftwareProjects\Hy2Sim\hy2sim\Data\Example_Missions\{mission}",
                                             circuit="CircuitRegulatedUncontrolledPowerSharing", result_path="../ResultTest/",
                                             verbose=False)
            check_csv = 0
            check_circuit_param = 0
            check_component_info = 0
            for root, dirs, files in os.walk(r'../ResultTest/'):
                # select file name
                for file in files:
                    # check the extension of files
                    if file.endswith('.csv'):
                        check_csv = check_csv + 1
                    elif file.endswith('info.yaml'):
                        check_component_info = check_component_info + 1
                    elif file.endswith('param.yaml'):
                        check_circuit_param = check_circuit_param + 1
            with self.subTest(msg=f"{mission} CSV Output"):
                self.assertEqual(1, check_csv)
            with self.subTest(msg=f"{mission} circuit parameter Output"):
                self.assertEqual(1, check_circuit_param)
            with self.subTest(msg=f"{mission} component info Output"):
                self.assertEqual(1, check_component_info)

            for root, dirs, files in os.walk(r'../ResultTest/'):
                # select file name
                for file in files:
                    # check the extension of files
                    if file.endswith('.csv'):
                        result_df = pd.read_csv(os.path.join(root, file))
                        with self.subTest(f"{mission} FC Efficiency Plausibility"):
                            self.assertGreater(0.83, result_df[['fc_stack efficiency 1']].iloc[0, 0])

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(r'../ResultTest/')


if __name__ == '__main__':
    unittest.main()
