import unittest
import bfch
import logging
import yaml
import os
import logging


def init_component():
    dir_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    raw_data = open(f"{dir_path}/Data/Example_Batteries/NG19_LiPo_Cell_0_07C.yaml")
    data = yaml.load(raw_data, Loader=yaml.FullLoader)
    return bfch.LiIon("li_test", data)


class TestNewComponent(unittest.TestCase):
    """
    ::warning:: not for Connections
    """
    def test_aa_initialization(self):
        component = init_component()
        logging.critical(f"Check Component {component.name} initialization")

    def test_ab_check_calculation_parameters(self):
        component = init_component()
        with self.subTest("Check Order"):
            self.assertGreaterEqual(component.order, 1)
            logging.info(f"Check Component {component.name} order")
        with self.subTest("Check Number of Unknowns"):
            self.assertEqual(component.order, len(component.unknowns))
            logging.info(f"Check Component {component.name} Number of Unknowns")
        with self.subTest("Check Number of Equations"):
            self.assertGreaterEqual(component.num_eq, 1)
            logging.info(f"Check Component {component.name} Number of Equations")
        with self.subTest("Check Name"):
            self.assertGreaterEqual(len(component.name), 1)
            logging.info(f"Check Component {component.name} Name")
        with self.subTest("Check Unknowns"):
            i = 0
            for unknown in component.unknowns:
                self.assertIsInstance(unknown, bfch.Variable)
                i = i + 1
            logging.info(f"Check Component {component.name} {i} Unknowns")
        with self.subTest("Check Logging Variables"):
            i = 0
            for logging_var in component.logging_variables:
                self.assertIsInstance(logging_var, bfch.Variable)
                i = i + 1
            logging.info(f"Check Component {component.name} {i} Logging Variables")
        with self.subTest("Check Operating Conditions"):
            i = 0
            for op_condition in component.operating_conditions:
                self.assertIsInstance(op_condition, bfch.Variable)
                i = i + 1
            logging.info(f"Check Component {component.name} {i} Operating Conditions")
        logging.critical(f"Check Component {component.name} calculation parameters")

    # test abstract methods --------------------------------------------------------
    def test_ba_check_f(self):
        pass

    def test_bb_check_df(self):
        pass

    def test_bc_check_calculate_efficiency(self):
        pass

    def test_bd_check_calculate_mass(self):
        pass

    def test_be_check_calculate_volume(self):
        pass

    def test_bf_check_calculate_rated_power(self):
        pass

    def test_bg_check_scale_rated_power(self):
        pass

    # test circuit integration ----------------------------------------------------
    def test_ca_initialization_in_circuit(self):
        pass

    # function test ----------------
    def test_da_single_component_circuit(self):
        pass


if __name__ == '__main__':
    unittest.main()
