import logging_utility
import logging

logging_utility.initialize_logging()

import bfch
import system_test
import new_circuit_test
import new_component_test
import unittest

test_name = input("Please enter the name type of test: ")

if "circuit" in test_name or "Circuit" in test_name or "curcuit" in test_name or "Curcuit" in test_name:
    suite = unittest.TestLoader().loadTestsFromTestCase(new_circuit_test.TestNewCircuit)
    unittest.TextTestRunner(verbosity=2).run(suite)
if "component" in test_name or "Component" in test_name or "comp" in test_name:
    suite = unittest.TestLoader().loadTestsFromTestCase(new_component_test.TestNewComponent)
    unittest.TextTestRunner(verbosity=2).run(suite)

