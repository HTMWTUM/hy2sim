"""
Created on 24.04.2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version:

File Description:
    
"""

import yaml
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from cycler import cycler
from datetime import datetime
from bfch import *
from copy import deepcopy

with open("../Data/Example_Batteries/Battery_generic_singlecell.yaml", 'r') as yamlfile:
    battery_data = yaml.load(yamlfile, Loader=yaml.FullLoader)

battery = LiIon("bat_test", battery_data)

input_file = Input()
c_rates_disch = [0.01]+list(np.linspace(1,30,21))
c_rates_disch = [0.01]+list(np.linspace(1,30,21))
c_rates_ch = [0.01]+list(np.linspace(0.5,5,10))

u_cutoff=2.5
battery.series_cells=1
battery.parallel_cells=1
battery.C_cell=4.*3600
battery.C_bat_As= battery.C_cell * battery.parallel_cells
battery.max_charging_current = 5 * battery.parallel_cells * battery.C_bat_As / 3600  # maximum charging current, A
battery.max_discharging_current = 30 * battery.parallel_cells * battery.C_bat_As / 3600
battery.i_in.val_max=battery.max_charging_current
battery.i_out.val_max=battery.max_discharging_current

charging = False


battery.idle = False
battery.regulated = True
battery.u_out.val_min = 1
battery.u_in.val_min = 1

c_rates=c_rates_disch
# c_rates=[c_rates_disch[3]]
if charging: c_rates=c_rates_ch

def bat_curves(battery, c_rates, charging=False):
    df=pd.DataFrame(columns=["I","C_rate","SoC","U", "C_delta", "Eff"])
    Component.t_place = 0
    Variable.t_place=0
    
    battery_i=deepcopy(battery)

    for c_rate in c_rates:
        if not charging:
            battery.soc_start=1
            nominal_cap = battery.soc_start * battery.C_bat_As * battery.parallel_cells
            current = c_rate * nominal_cap / 3600
        else:
            battery.soc_start=0.1
            nominal_cap = (1-battery.soc_start) * battery.C_bat_As * battery.parallel_cells
            current = c_rate * nominal_cap / 3600
            
        # battery_i.update_soc_pre()
        predicted_time = nominal_cap / current
        t = np.linspace(0, predicted_time * 1., 100)

        input_file.conditions = ["time s", "ambient_temp K"]
        input_file.segment = [t, [290] * len(t)]
        Component.t = t
        battery_i.new_segment(input_file)
        battery_i.soc.val_min = 0
        
        battery_i.i_out.overwrite_constant(current*(1.-float(charging)))
        battery_i.i_in.overwrite_constant(current*float(charging))
        
        len_df=len(df)
        C_delta=0
        for i,it in enumerate(t):
            Component.t_place = i
            Variable.t_place = i
            battery_i.idle = False
            if charging:
                battery_i.i_in.val=current
                battery_i.update_soc_pre()
                battery_i.charging = True
                voltage=battery_i.return_charging_voltage(current)
                battery_i.u_in.val = voltage
            else:
                battery_i.i_out.val=current
                battery_i.update_soc_pre()
                voltage=battery_i.return_discharging_voltage(current)
                battery_i.u_out.val = voltage
        
            eff=battery_i.calculate_efficiency()
            soc=battery_i.soc()
            battery_i.update_soc_post()
            if i>0: C_delta+=current*(t[i]-t[i-1])/3600
            df.loc[i+len_df]=[battery_i.i_out(),c_rate,soc,voltage, C_delta, eff]
                
    return df

def plot_curves(df, x_col='SoC'):
    
    fig, ax1 = plt.subplots()
    ax1.set_prop_cycle(cycler(color='bgrcmyk'))
    for c_rate in set(df.C_rate):
        df_tmp=df[df.C_rate==c_rate]
        x=df_tmp[x_col]
        ax1.plot(x, df_tmp.U, label=str(c_rate)+' C_bat_As')
       
    ax1.set_xlabel(x_col)
    ax1.set_ylabel('U [V]')
    ax1.legend()
    if x_col=='SoC': 
        plt.axis([1, 0, 2., max(df.U)+0.5])
    else:
        plt.axis([min(x), max(x), 2., max(df.U)+0.5])
        
    plt.show()
        
       
df = bat_curves(battery, c_rates, charging=charging)
plot_curves(df, x_col='SoC')
