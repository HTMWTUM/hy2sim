"""
Created on 5/24/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Version: 0.00

File Description:
    
"""
import yaml
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from datetime import datetime
from bfch import *


raw_diode_data = open("../Data/Diode.yaml")
diode_data = yaml.load(raw_diode_data, Loader=yaml.FullLoader)

diode = Diode("diode_test", diode_data)

input_file = Input()

u = np.linspace(-2, 1, 100)

input_file.conditions = ["time s", "ambient_temp K"]
input_file.segment = [[0], [290]]
Component.t = [0]
diode.new_segment(input_file)

diode_model_vectorized = np.vectorize(diode.shockley)
i_res = diode_model_vectorized(u,diode.i_s, diode.n)

plt.plot(u, i_res)
plt.show()
