"""
Created on 1/11/2022
Author: Tancrède Oswald
Units: m-kg-s (SI)

File Description:
    In this file the use of the core package is checked by emulating the surrounding calculation
    
"""
import copy

import bfch
import time
import numpy as np

# -------------------------------------------------------------------------------------------------------------------- #
# Model Control
# -------------------------------------------------------------------------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #
# CHECKLIST NEW MODEL / NEW CALCULATION
#   - check control parameters, spelling and values
#   - check circuit_param
#   - component_info_all filled out correctly at keyword circuit_type?
#   - data2plot ok? check the names of the variables, are they defined in the corresponding components?
#   - is the correct list entry chosen in plot_results_dyn (almost at the end of this file)

set_duration = 0    # 0: take duration from load data| 1: set custom duration
duration = 200     # in s
circuit_type = "unreg_H3_Diode"
soc = 0.1             # soc at the beginning of the calculation
mission = "mission_profile_state1_2000W"   # Info: character '°' is not allowed in here so degrees Celsius is only 'C_bat_As'
purging = False
max_step_size = 30
allowed_dev = 0.01
max_solver_steps = 10000
convergence_crit = 1e-4
solver_step_factor = 0.2

circuit_param = {
    "bat_chargingthreshold": 0.9,
    "bat_max_charging": 0.99,
    "bat_min_charge": 0.05,
    "conditioning": purging,
    "purging_period": 10,
    "purging_duration": 0.1,
    "load_voltage": 13.5,
    "bat_charging_i": 1,
    "bat_discharging_i": 2,
    "soc_min": 0.,
    "limit_fc_power_pc": 0.7,
    "fuel_cell_current": 3.5,
    "maximum_fuel_cell_power": 50
}
conditions_defaults = {
    "time": [0, "s"],
    "power": [0, "W"],
    "omega": [0, "Hz"],
    "m_l": [0, "Nm"],
    "ambient_temp": [290, "K"],
    "fc_temperature": [350, "K"],
    "anode_p": [101325, "Pa"],
    "xH2": [1, "1"],
    "cath_p": [3*101325, "Pa"],
    "xO2": [0.21, "1"],
    "cp": [1004.5, "J/(kg*K)"],
    "kappa": [1.4, "1"],
    "ambient_pressure": [101325, "Pa"],
    "massflow_cath": [0.001, "kg/s"],
    "air_density": [1.13, "kg/m^3"]
    }
conversions = {
    "kW": ["mult", 1000],
    "MW": ["mult", 1e6],
    "rpm": ["mult", 0.016666667],
    "1/min": ["mult", 0.016666667],
    "C_bat_As": ["add", 273.15],
    "bar": ["mult", 100000],
    "atm": ["mult", 9.86923267e-6],
    "min": ["mult", 60],
    "h": ["mult", 3600],
    "g/cm^3": ["mult", 1000]
    }

component_info_all = {
    "unreg_H3": {
        "fc_stack": "H3_Fuel_Cell_1000HV.yaml",
        "Battery": "AREA_FlyHy_Battery.yaml",
        "Power_Sink": "Power_Sink.yaml",
    }}


component_info_all = {
    "unreg_H3_Diode": {
        "fc_stack": "H3_Fuel_Cell_4500LV.yaml",
        "Diode": "Diode.yaml",
        "Battery": "Battery_HyDDEn.yaml",
        "Power_Sink": "Power_Sink.yaml",
        "Diode_bat": "Diode.yaml"
    }}


# Plotting
data2plot = [
    {   # Unregulated Fuel Cell + Battery
        "Power Split": {
                "fc_stack": ["power"],
                "Battery": ["power_out"],
                "Power_Sink": ["p_it"]
            },
        "Voltage": {
                "fc_stack": ["u_out"],
                "Battery": ["voltage"],
                "Power_Sink": ["u_in"]
            },
        "Current": {
                "fc_stack": ["i_out"],
                "Battery": ["i_out"],
                "Power_Sink": ["i_in"]
            },
        "Current Density": {
                "fc_stack": ["current_density"]
            },
        "Cell Voltage": {
                "fc_stack": ["cell_voltage"]
            },
        "Battery Current": {
                "Battery": ["i_out"]
            },
        "Battery Voltage": {
                "Battery": ["u_out"]
        },
        "H2 Consumption": {
                "fc_stack": ["h2_cumulated_consumption"]
        },
        "H2 Flow": {
                "fc_stack": ["h2_mass_flow"]
        }
    }
    ]


# -------------------------------------------------------------------------------------------------------------------- #
# Data Loading - the component data loading will be needed for the integrated form too
# -------------------------------------------------------------------------------------------------------------------- #
# load component data


# create the full mission profile
mission_profile = bfch.data.get_mission_data(conditions_defaults, conversions, mission, set_duration, duration)

# -------------------------------------------------------------------------------------------------------------------- #
# Initialization - this part will be needed in the integrated form of the code
# -------------------------------------------------------------------------------------------------------------------- #
# instantiate circuit
# curcuit = bfch.create_circuit(circuit_param=circuit_param, circuit_type=circuit_type, component_info=component_info_all[circuit_type])
curcuit = bfch.CircuitUnregulated_H3_Diode(circuit_param=circuit_param, circuit_type=circuit_type, component_info=component_info_all[circuit_type])

# instantiate the input and the output
model_input = bfch.Input()
output_data = bfch.Output()
list_of_outputs = []

# instantiate the solver
circuit_solver = bfch.CircuitSolver(max_step_size, allowed_dev, max_solver_steps, convergence_crit, solver_step_factor)

# -------------------------------------------------------------------------------------------------------------------- #
# Calculation - this part is again only an emulation except for the circuit_solver
# -------------------------------------------------------------------------------------------------------------------- #
start = time.perf_counter()
t_instant_p = 0
t_elap = 0
for i in range(0, len(mission_profile.index)-1, 2):
    # get the information about the current segment into the input class
    model_input.update_from_mission_profile(i, mission_profile)

    # run the calculation for the current segment and store it in the list of outputs
    list_of_outputs.append(copy.deepcopy((circuit_solver.solve(curcuit, model_input, output_data))))

    # show calculation progress
    state0 = list_of_outputs[-1].output_circuit[f'{circuit_type} state 1'].iloc[0]
    state1 = list_of_outputs[-1].output_circuit[f'{circuit_type} state 1'].iloc[-1]
    print(f'Segment: {i/2+1} GO!| t = {t_instant_p}| at: t={t_elap}| State: {state0}')
    t_instant_p = np.around(mission_profile["time s"][i+1], 3)
    t_elap = np.around(time.perf_counter() - start, 6)
    print(f'Segment: {i/2+1} FIN| t = {t_instant_p}| at: t={t_elap}| State: {state1}')

fig = bfch.data.plot_results_dyn(list_of_outputs, data2plot=data2plot[0])
# plt.show()
bfch.data.save_output_to_csv(list_of_outputs, circuit_type, mission, circuit_param, curcuit)
bfch.data.save_plots(circuit_type, fig, mission)
# -------------------------------------------------------------------------------------------------------------------- #
